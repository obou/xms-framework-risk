package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.common.domain.model.ValueObject;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@EmbeddedOnly
public class IpAddress extends ValueObject {

	// {{ IpProtocol (property)
	private IpProtocol ipProtocol;

	@MemberOrder(sequence = "1")
	public IpProtocol getIpProtocol() {
		return ipProtocol;
	}

	public void setIpProtocol(final IpProtocol ipProtocol) {
		this.ipProtocol = ipProtocol;
	}

	// }}

	// {{ IpAddress (property)
	private String ipAddress;

	/**
	 * @param string
	 * @param string2
	 */
	public IpAddress(String ipAddress, String ipMask) {
		this.setIpAddress(ipAddress);
		this.setIpMask(ipMask);
	}

	@MemberOrder(sequence = "1")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(final String ipAddress) {
		this.ipAddress = ipAddress;
	}

	// }}

	// {{ IpMask (property)
	private String ipMask;

	@MemberOrder(sequence = "1")
	public String getIpMask() {
		return ipMask;
	}

	public void setIpMask(final String ipMask) {
		this.ipMask = ipMask;
	}
	// }}

}
