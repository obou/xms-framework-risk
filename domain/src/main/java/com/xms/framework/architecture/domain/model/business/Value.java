package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Entity;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipRepository;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Value extends Entity {

	// {{ AssociatedWith (property)

	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssociatedWith() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.ASSOCIATION);
	}

	public void associatedWithProduct(Product product) {
		// Inverse relationship.
		product.associatedWithValue(this);
	}

	public void notAssociatedWithProduct(Product product) {
		// Inverse relationship.
		product.notAssociatedWithValue(this);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	public RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

}
