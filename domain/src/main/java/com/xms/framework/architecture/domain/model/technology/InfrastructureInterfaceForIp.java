package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.common.domain.model.ValueObject;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@EmbeddedOnly
public class InfrastructureInterfaceForIp extends ValueObject {

	// {{ NodeAddress (property)
	private NodeCommunicationPathAddress nodeAddress;

	@Optional
	@MemberOrder(sequence = "1")
	public NodeCommunicationPathAddress getNodeAddress() {
		return nodeAddress;
	}

	public void setNodeAddress(final NodeCommunicationPathAddress nodeAddress) {
		this.nodeAddress = nodeAddress;
	}

	// }}

	// {{ IpProtocol (property)
	private IpProtocol ipProtocol;

	/**
	 * @param nodeAddress
	 * @param ipProtocol2
	 * @param port
	 */
	public InfrastructureInterfaceForIp(
			NodeCommunicationPathAddress nodeAddress, IpProtocol ipProtocol,
			int port) {
		this.setNodeAddress(nodeAddress);
		this.setIpProtocol(ipProtocol);
		this.setIpPort(port);
	}

	/**
	 * 
	 */
	public InfrastructureInterfaceForIp() {
		// Do nothing.
	}

	@Optional
	@MemberOrder(sequence = "1")
	public IpProtocol getIpProtocol() {
		return ipProtocol;
	}

	public void setIpProtocol(final IpProtocol ipProtocol) {
		this.ipProtocol = ipProtocol;
	}

	// }}

	// {{ Port (property)
	private Integer ipPort;

	@MemberOrder(sequence = "1")
	public Integer getIpPort() {
		return ipPort;
	}

	public void setIpPort(final Integer port) {
		this.ipPort = port;
	}

	public String validateIpPort(final Integer ipPort) {
		if (ipPort == null)
			return null;
		if (ipPort < 0 || ipPort > 65535) {
			return "An Ip Port must be a number between 0 and 65.535";
		} else {
			return null;
		}
	}

	// }}

}
