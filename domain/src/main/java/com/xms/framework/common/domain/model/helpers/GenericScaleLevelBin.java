package com.xms.framework.common.domain.model.helpers;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.MappedSuperclass;

/**
 * Represents a Level on a Scale which can be represented by Bins between a
 * lower and upper limit.
 * <p>
 * This implementation does not support descriptions on multiple Locales.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <T>
 *            Scale this Level is related to.
 */
//@MappedSuperclass
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class GenericScaleLevelBin extends GenericScaleLevel {

	/**
	 * Lower value of the bin represented by this Scale Level.
	 */
	private Double lowerLimit;

	/**
	 * Upper value of the bin represented by this Scale Level.
	 */
	private Double upperLimit;

	public Double getLowerLimit() {
		return lowerLimit;
	}

	public Double getUpperLimit() {
		return upperLimit;
	}

	public void setLowerLimit(Double lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public void setUpperLimit(Double upperLimit) {
		this.upperLimit = upperLimit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.risk.criteria.api.domain.GenericScaleLevel#isValueIncluded
	 * (java.lang.Object)
	 */
	@Override
	public Boolean isValueIncluded(Double value) {
		return (value.doubleValue() >= this.lowerLimit.doubleValue())
				&& (value.doubleValue() < this.upperLimit.doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.risk.criteria.api.domain.GenericScaleLevel#equals(java
	 * .lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (other == this)
			return true;
		if (other == null)
			return false;
		if (getClass() != other.getClass())
			return false;
		GenericScaleLevelBin level = (GenericScaleLevelBin) other;
		return ((this.lowerLimit == level.getLowerLimit() || (lowerLimit != null && lowerLimit
				.equals(level.getLowerLimit()))) && (this.upperLimit
				.equals(level.getUpperLimit()) || (upperLimit != null && upperLimit
				.equals(level.getUpperLimit()))));
	}

	@Override
	public int hashCode() {
		return lowerLimit.hashCode() ^ upperLimit.hashCode();
	}
}
