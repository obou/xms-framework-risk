package com.xms.framework.common.domain.model;

import java.util.UUID;

import org.apache.isis.applib.AbstractFactoryAndRepository;
import org.apache.isis.applib.annotation.Hidden;

import com.xms.framework.domain.api.domain.DomainObject;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public abstract class AbstractDomainObjectRepositoryAndFactory extends
		AbstractFactoryAndRepository {

	// {{ newPersistenceInstance, newInstance
	// /**
	// * Returns a new instance of the specified class that will have been
	// * persisted.
	// *
	// * <p>
	// * This method isn't quite deprecated, but generally consider using
	// * {@link #newTransientInstance(Class)} instead.
	// */
	// protected T newPersistentDomainObject() {
	// // We need to initialize the Id.
	// // return super.newPersistentInstance(ofClass);
	// Class<T> ofClass = null;
	// T instance = this.newTransientInstance(ofClass);
	// persist(instance);
	// return instance;
	// }
	//

	// It cannot be implemented through generics, as the same repository must be
	// able to create different types of DomainObjects as needed (for example,
	// an Aggregate Root must create all aggregated Entities.
	protected <T extends DomainObject> T newTransientDomainObject(
			final Class<T> ofType, String id) {
		final T instance = newTransientInstance(ofType);
		if (id == null || id.isEmpty())
			instance.setId(nextIdentity());
		else
			instance.setId(id);
		return instance;

	}

	/**
	 * @return
	 */
	@Hidden
	public String nextIdentity() {
		return UUID.randomUUID().toString().toUpperCase();
	}

}
