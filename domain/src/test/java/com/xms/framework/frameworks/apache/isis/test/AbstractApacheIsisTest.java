package com.xms.framework.frameworks.apache.isis.test;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.progmodel.wrapper.applib.WrapperFactory;
import org.apache.isis.progmodel.wrapper.applib.WrapperObject;
import org.apache.isis.viewer.junit.IsisTestRunner;
import org.junit.After;
import org.junit.runner.RunWith;

@RunWith(IsisTestRunner.class)
public abstract class AbstractApacheIsisTest {

	private DomainObjectContainer domainObjectContainer;
	private WrapperFactory wrapperFactory;

	protected <T> T wrapped(final T obj) {
		return wrapperFactory.wrap(obj);
	}

	@SuppressWarnings("unchecked")
	protected <T> T unwrapped(final T obj) {
		if (obj instanceof WrapperObject) {
			final WrapperObject wrapperObject = (WrapperObject) obj;
			return (T) wrapperObject.wrapped();
		}
		return obj;
	}

	@After
	public void tearDown() {
	}

	protected WrapperFactory getWrapperFactory() {
		return wrapperFactory;
	}

	public void setWrapperFactory(final WrapperFactory wrapperFactory) {
		this.wrapperFactory = wrapperFactory;
	}

	protected DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

}
