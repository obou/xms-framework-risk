package com.xms.framework.party.domain.model.organization;

import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;
import com.xms.framework.party.domain.model.Organization;
import com.xms.framework.party.domain.model.Person;

/**
 * Job is the set of responsibilities to be done by a Person and Position is
 * holded by the Person.
 * <p>
 * Some Positions can be holded by Organizations, instead of Persons.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class Position extends AbstractMultiTenantEntity {

	@ManyToOne(optional = false)
	private Job job;

	@ManyToOne(optional = false)
	private Organization organization;

	@ManyToOne(optional = false)
	private Person holdedByPerson;

	/**
	 * Additional Roles assigned to this Position, additionally to the ones
	 * assigned to the Job description.
	 */
	@OneToMany(mappedBy = "position", cascade = CascadeType.REMOVE)
	private Set<PositionRole> extendedWithRoles;

	@SuppressWarnings("unused")
	//@Transient
	private Set<Role> effectiveRoles;

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Set<PositionRole> getExtendedWithRoles() {
		return extendedWithRoles;
	}

	public void setExtendedWithRoles(Set<PositionRole> extendedWithRoles) {
		this.extendedWithRoles = extendedWithRoles;
	}

	public Set<Role> getEffectiveRoles() {
		Set<Role> set = new HashSet<Role>();

		// Roles assigned to its Job description.
		if (this.getJob() != null) {
			for (JobRole current : this.getJob().getRolesAssigned()) {
				set.add(current.getRole());
			}

		}

		// Roles assigned directly to this Position.
		for (PositionRole current : this.getExtendedWithRoles()) {
			set.add(current.getRole());
		}

		return set;
	}

	public Person getHoldedByPerson() {
		return holdedByPerson;
	}

	public void setHoldedByPerson(Person holdedByPerson) {
		this.holdedByPerson = holdedByPerson;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
