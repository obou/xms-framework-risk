package com.xms.framework.architecture.domain.model.motivation;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.CoreElement;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Requirement extends MotivationalElement {

	// {{ RealisedBy (Collection)
	private Set<CoreElement> realisedBy = new LinkedHashSet<CoreElement>();

	@MemberOrder(sequence = "1")
	public Set<CoreElement> getRealisedBy() {
		return realisedBy;
	}

	public void setRealisedBy(final Set<CoreElement> realisedBy) {
		this.realisedBy = realisedBy;
	}
	// }}

}
