package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Entity;
import com.xms.framework.architecture.domain.model.PassiveStructureElement;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessObject extends PassiveStructureElement {

	// {{ AccessedBy

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "accessedBy", sequence = "10")
	public void accessedByBusinessFunction(BusinessFunction businessFunction) {
		super.addToAccessedByBehaviorElement(businessFunction);
	}

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "accessedBy", sequence = "20")
	public void notAccessedByBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromAccessedByBehaviorElement(businessFunction);
	}

	/**
	 * @param businessService
	 */
	@MemberOrder(name = "accessedBy", sequence = "30")
	public void accessedByBusinessService(BusinessService businessService) {
		super.addToAccessedByService(businessService);
	}

	/**
	 * @param businessService
	 */
	@MemberOrder(name = "accessedBy", sequence = "40")
	public void notAccessedByBusinessService(BusinessService businessService) {
		super.removeFromAccessedByService(businessService);
	}

	/**
	 * @param businessEvent
	 */
	@MemberOrder(name = "accessedBy", sequence = "30")
	public void accessedByBusinessEvent(BusinessEvent businessEvent) {
		super.addToAccessedByBehaviorElement(businessEvent);
	}

	/**
	 * @param businessEvent
	 */
	@MemberOrder(name = "accessedBy", sequence = "40")
	public void notAccessedByBusinessEvent(BusinessEvent businessEvent) {
		super.removeFromAccessedByBehaviorElement(businessEvent);
	}

	// }}

	// {{ AssociatedWith
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssociatedWith() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.ASSOCIATION);
	}

	/**
	 * @param businessRole
	 */
	// A Product can Aggregate both BusinessServices and Contracts.
	protected void addToAssociatedWith(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this, entity,
				RelationshipType.AGGREGATION);

	}

	/**
	 * @param businessRole
	 */
	protected void removeFromAssociatedWith(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "30")
	public void associatedWithEntity(Entity entity) {
		addToAssociatedWith(entity);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "40")
	public void notAssociatedWithEntity(Entity entity) {
		removeFromAssociatedWith(entity);

	}

	/**
	 * @param meaning
	 */
	public void associatedWithMeaning(Meaning meaning) {
		associatedWithEntity(meaning);
	}

	/**
	 * @param meaning
	 */
	public void notAssociatedWithMeaning(Meaning meaning) {
		associatedWithEntity(meaning);
	}

	// }}

	// {{ RealizedBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getRealizedBy() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.REALIZATION);
	}

	protected void realizedByRepresentation(final Representation representation) {

		// It's the inverse Relationship.
		representation.realizesBusinessObject(this);

	}

	protected void removeFromAccessedByBehaviorElement(
			final Representation representation) {

		// Is the inverse relationship.
		representation.realizesBusinessObject(this);

	}

	// }}

}
