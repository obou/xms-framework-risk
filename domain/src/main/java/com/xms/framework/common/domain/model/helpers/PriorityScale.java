package com.xms.framework.common.domain.model.helpers;

import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * Scale used for assessing the priority of a Business BusinessProcess.
 * <p>
 * <b>ISO 21827 - 2011 - 7.2.2 BP.02.01 - Prioritize Capabilities</b>: Identify,
 * analyse, and prioritize operational, business, or mission capabilities
 * leveraged by the system.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class PriorityScale extends GenericScale {

	@OneToMany(mappedBy = "priorityScale")
	private Set<PriorityScaleLevel> scaleLevels;

	@Override
	public Set<GenericScaleLevel> getLevels() {
		Set<GenericScaleLevel> set = new HashSet<GenericScaleLevel>();
		if (scaleLevels != null)
			set.addAll(scaleLevels);
		return set;
	}

	public Set<PriorityScaleLevel> getScaleLevels() {
		return scaleLevels;
	}

	public void setScaleLevels(Set<PriorityScaleLevel> scaleLevels) {
		this.scaleLevels = scaleLevels;
	}

}
