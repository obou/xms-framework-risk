package com.xms.framework.common.domain.model.helpers;

public enum Weekday {

	SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
