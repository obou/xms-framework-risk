package com.xms.framework.architecture.domain.model.technology;

import java.util.List;

import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.architecture.domain.model.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class InfrastructureInterface extends Entity {

	// {{ PartOfNode (property)
	private AbstractNode partOfNode;

	@MemberOrder(sequence = "1")
	public AbstractNode getPartOfNode() {
		return partOfNode;
	}

	protected void setPartOfNode(final AbstractNode partOfNode) {
		this.partOfNode = partOfNode;
	}

	public void modifyPartOfNode(final AbstractNode node) {
		AbstractNode currentPartOfNode = getPartOfNode();
		// check for no-op
		if (node == null || node.equals(currentPartOfNode)) {
			return;
		}
		// delegate to parent to associate
		node.addToComposedOfInfrastructureInterfaces(this);
		// additional business logic
		onModifyPartOfNode(currentPartOfNode, node);
	}

	/**
	 * @param currentPartOfNode
	 * @param node
	 */
	private void onModifyPartOfNode(AbstractNode currentPartOfNode,
			AbstractNode node) {
		// Do nothing.

	}

	public void clearPartOfNode() {
		AbstractNode currentPartOfNode = getPartOfNode();
		// check for no-op
		if (currentPartOfNode == null) {
			return;
		}
		// delegate to parent to dissociate
		currentPartOfNode.removeFromComposedOfInfrastructureInterfaces(this);
		// additional business logic
		onClearPartOfNode(currentPartOfNode);
	}

	// }}

	/**
	 * @param currentPartOfNode
	 */
	private void onClearPartOfNode(AbstractNode currentPartOfNode) {
		// Do nothing.

	}

	// {{ AssignedToInfrastructureService (property)
	private InfrastructureService assignedToInfrastructureService;

	@Optional
	@MemberOrder(sequence = "1")
	public InfrastructureService getAssignedToInfrastructureService() {
		return assignedToInfrastructureService;
	}

	public void setAssignedToInfrastructureService(
			final InfrastructureService assignedToInfrastructureService) {
		this.assignedToInfrastructureService = assignedToInfrastructureService;
	}

	public void modifyAssignedToInfrastructureService(
			final InfrastructureService infrastructureService) {
		InfrastructureService currentAssignedToInfrastructureService = getAssignedToInfrastructureService();
		// check for no-op
		if (infrastructureService == null
				|| infrastructureService
						.equals(currentAssignedToInfrastructureService)) {
			return;
		}
		// delegate to parent to associate
		infrastructureService.addToAssignedToInfrastructureInterfaces(this);
		// additional business logic
		onModifyAssignedToInfrastructureService(
				currentAssignedToInfrastructureService, infrastructureService);
	}

	/**
	 * @param currentAssignedToInfrastructureService
	 * @param infrastructureService2
	 */
	private void onModifyAssignedToInfrastructureService(
			InfrastructureService currentAssignedToInfrastructureService,
			InfrastructureService infrastructureService2) {
		// Do nothing.

	}

	public void clearAssignedToInfrastructureService() {
		InfrastructureService currentAssignedToInfrastructureService = getAssignedToInfrastructureService();
		// check for no-op
		if (currentAssignedToInfrastructureService == null) {
			return;
		}
		// delegate to parent to dissociate
		currentAssignedToInfrastructureService
				.removeFromAssignedToInfrastructureInterfaces(this);
		// additional business logic
		onClearAssignedToInfrastructureService(currentAssignedToInfrastructureService);
	}

	// }}

	/**
	 * @param currentAssignedToInfrastructureService
	 */
	private void onClearAssignedToInfrastructureService(
			InfrastructureService currentAssignedToInfrastructureService) {
		// Do nothing.

	}

	// }}

	// {{ IpInterface (property)
	@Persistent
	@Embedded
	private InfrastructureInterfaceForIp ipInterface = new InfrastructureInterfaceForIp();

	@Optional
	@MemberOrder(sequence = "1")
	public InfrastructureInterfaceForIp getIpInterface() {
		return ipInterface;
	}

	public void setIpInterface(final InfrastructureInterfaceForIp ipInterface) {
		this.ipInterface = ipInterface;
	}

	public void modifyIpInterface(final InfrastructureInterfaceForIp ipInterface) {
		InfrastructureInterfaceForIp currentIpInterface = getIpInterface();
		// check for no-op
		if (ipInterface == null || ipInterface.equals(currentIpInterface)) {
			return;
		}
		// associate new
		setIpInterface(ipInterface);
		// additional business logic
		onModifyIpInterface(currentIpInterface, ipInterface);
	}

	public void clearIpInterface() {
		InfrastructureInterfaceForIp currentIpInterface = getIpInterface();
		// check for no-op
		if (currentIpInterface == null) {
			return;
		}
		// dissociate existing
		setIpInterface(null);
		// additional business logic
		onClearIpInterface(currentIpInterface);
	}

	protected void onModifyIpInterface(
			final InfrastructureInterfaceForIp oldIpInterface,
			final InfrastructureInterfaceForIp newIpInterface) {

		// TODO: Send event for updating monitoring.

	}

	protected void onClearIpInterface(
			final InfrastructureInterfaceForIp oldIpInterface) {

		// TODO: Send event for updating monitoring.

	}

	/**
	 * @return
	 */
	public List<InfrastructureInterfaceUsedByInfrastructureService> getUsedByInfrastructureServices() {
		return nodeFactory
				.getInfrastructureServicesThatUsesInfrastructureInterface(this);
	}

	// }}

	// {{ injected: NodeFactory
	private NodeFactory nodeFactory;

	public void setNodeFactory(final NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	// }}

	// {{ usedByInfrastructureService (action)
	@MemberOrder(sequence = "1")
	public void usedByInfrastructureService(
			InfrastructureService infrastructureService) {

		infrastructureService.usesInfrastructureInterface(this);

	}

	// }}

	// {{ UsesInfrastructureInterface (action)
	@MemberOrder(sequence = "1")
	public void usesInfrastructureInterface(
			InfrastructureInterface infrastructureInterface) {

	}
	// }}
	
}
