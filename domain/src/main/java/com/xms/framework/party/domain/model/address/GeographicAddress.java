package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

@XMSEntityNames({
		@XMSEntityName(locale = "es", name = "Dirección Geográfica"),
		@XMSEntityName(locale = "en", name = "Geographic Address") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class GeographicAddress extends Address {

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Dirección Secundaria"),
			@XMSLocale(locale = "en", caption = "Address Line 2") })
	private String addressLine2;

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Dirección Terciaria"),
			@XMSLocale(locale = "en", caption = "Address Line 3") })
	private String addressLine3;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Ciudad"),
			@XMSLocale(locale = "en", caption = "City") })
	private String city;

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Código Postal"),
			@XMSLocale(locale = "en", caption = "Postal Code") })
	private String postalCode;

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Región o Estado"),
			@XMSLocale(locale = "en", caption = "Region or State") })
	private String regionOrState;

	@ManyToOne
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "País"),
			@XMSLocale(locale = "en", caption = "Country") })
	private Country country;

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRegionOrState() {
		return regionOrState;
	}

	public void setRegionOrState(String regionOrState) {
		this.regionOrState = regionOrState;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
