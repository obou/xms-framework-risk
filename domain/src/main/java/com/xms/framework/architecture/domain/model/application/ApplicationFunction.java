package com.xms.framework.architecture.domain.model.application;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.BehaviorElement;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class ApplicationFunction extends BehaviorElement {

	// {{ Realizes
	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "realizes", sequence = "10")
	public void realizesApplicationService(ApplicationService applicationService) {
		super.addToRealizes(applicationService);
	}

	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "realizes", sequence = "20")
	public void notRealizesApplicationService(
			ApplicationService applicationService) {
		super.removeFromRealizes(applicationService);
	}

	// }}

	// {{ Uses

	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "uses", sequence = "10")
	public void usesApplicationService(ApplicationService applicationService) {
		super.addToUses(applicationService);
	}

	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "uses", sequence = "20")
	public void notUsesApplicationService(ApplicationService applicationService) {
		super.removeFromUses(applicationService);
	}

	// }}

	// {{ AssignedFrom

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void assignedFromApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.addToAssignedFrom(applicationComponent);

	}

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "assignedFrom", sequence = "20")
	public void notAssignedFromApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.removeFromAssignedFrom(applicationComponent);
	}

	// public String validateAssignedFromApplicationComponent(
	// ApplicationComponent applicationComponent) {
	// if ((this instanceof ApplicationInteraction)
	// && !(applicationComponent instanceof ApplicationCollaboration)) {
	// return
	// "An Application Interaction can only be assigned to Application Collaborations, not to usual Application Components";
	// }
	// return null;
	// }

	// }}

	// {{ Triggers

	/**
	 * @param applicationFunction2
	 */
	@MemberOrder(name = "triggers", sequence = "10")
	public void triggersApplicationFunction(
			ApplicationFunction applicationFunction2) {
		super.addToTriggers(applicationFunction2);

	}

	/**
	 * @param applicationFunction2
	 */
	@MemberOrder(name = "triggers", sequence = "20")
	public void notTriggersApplicationFunction(
			ApplicationFunction applicationFunction2) {
		super.removeFromTriggers(applicationFunction2);

	}

	// }}

	// {{ TriggeredBy

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "triggeredBy", sequence = "10")
	public void triggeredByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addToTriggeredBy(applicationFunction);

	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "triggeredBy", sequence = "10")
	public void notTriggeredByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromTriggeredBy(applicationFunction);

	}

	// }}

	// {{ FlowTo

	/**
	 * @param applicationFunction2
	 */
	@MemberOrder(name = "flowTo", sequence = "10")
	public void flowToApplicationFunction(
			ApplicationFunction applicationFunction2) {
		super.addToFlowTo(applicationFunction2);

	}

	/**
	 * @param applicationFunction2
	 */
	@MemberOrder(name = "flowTo", sequence = "20")
	public void notFlowToApplicationFunction(
			ApplicationFunction applicationFunction2) {
		super.removeFromFlowTo(applicationFunction2);

	}

	// }}

	// {{ FlowFrom

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "flowFrom", sequence = "10")
	public void flowFromApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addToFlowFrom(applicationFunction);

	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "flowFrom", sequence = "20")
	public void notFlowFromApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromFlowFrom(applicationFunction);

	}

	// }}

	// {{ Accesses

	/**
	 * @param dataObject
	 */
	@MemberOrder(name = "accesses", sequence = "10")
	public void accessesDataObject(DataObject dataObject) {
		super.addToAccesses(dataObject);

	}

	/**
	 * @param dataObject
	 */
	@MemberOrder(name = "accesses", sequence = "20")
	public void notAccessesDataObject(DataObject dataObject) {
		super.removeFromAccesses(dataObject);

	}

	// }}

}
