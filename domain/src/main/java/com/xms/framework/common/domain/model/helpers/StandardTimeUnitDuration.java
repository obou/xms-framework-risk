package com.xms.framework.common.domain.model.helpers;

import javax.jdo.annotations.EmbeddedOnly;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@EmbeddedOnly
public class StandardTimeUnitDuration {
	private final Double duration;

	private final StandardTimeUnit unit;

	public StandardTimeUnitDuration(Double duration, StandardTimeUnit unit) {
		this.duration = duration;
		this.unit = unit;
	}

	public Double getDuration() {
		return duration;
	}

	public StandardTimeUnit getUnit() {
		return unit;
	}
}
