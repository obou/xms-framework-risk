package com.xms.framework.architecture.domain.model.application.tests;

import org.junit.Test;

import com.xms.framework.architecture.domain.model.application.ApplicationComponent;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class ApplicationComponentTest extends ApplicationLayerTest {

	@Test
	public void testCreateDefault() {

		ApplicationComponent applicationComponent = this
				.getApplicationComponentFactory().createApplicationComponent(
						"SAP HR", null);

		validateCommonAssertions(applicationComponent);

	}

}
