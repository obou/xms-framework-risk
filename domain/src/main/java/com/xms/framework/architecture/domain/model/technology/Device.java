package com.xms.framework.architecture.domain.model.technology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MaxLength;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Device extends AbstractNode {

	private NodeFactory nodeFactory;

	// {{ Node (property)
	private Node partOfNode;

	@MemberOrder(sequence = "1")
	public Node getPartOfNode() {
		return partOfNode;
	}

	protected void setPartOfNode(final Node node) {
		this.partOfNode = node;
	}

	// El modifyXXX deber�a llamar al setter. Mirar la documentaci�n.

	public void modifyPartOfNode(final Node node) {
		Node currentNode = getPartOfNode();
		// check for no-op
		if (node == null || node.equals(currentNode)) {
			return;
		}
		// XMS: Not needed.
		// if (this.getNode() != null) {
		// this.getNode().removeFromComposedOfDevices(this);
		// }

		// delegate to parent to associate
		node.addToComposedOfDevices(this);
		// additional business logic
		onModifyPartOfNode(currentNode, node);
	}

	/**
	 * @param currentNode
	 * @param node2
	 */
	private void onModifyPartOfNode(Node currentNode, Node node2) {
		if (currentNode != null)
			currentNode.removeFromComposedOfDevices(this);
	}

	public void clearPartOfNode() {
		Node currentNode = getPartOfNode();
		// check for no-op
		if (currentNode == null) {
			return;
		}
		// delegate to parent to dissociate
		currentNode.removeFromComposedOfDevices(this);
		// additional business logic
		onClearPartOfNode(currentNode);
	}

	/**
	 * @param currentNode
	 */
	private void onClearPartOfNode(Node currentNode) {
		// // Don't do nothing.

	}

	// }}

	// {{ ComposedOfSystemSoftware (Collection)
	@Element(mappedBy = "assignedToDevice", dependent = "true")
	private Set<SystemSoftware> assignedToSystemSoftware = new LinkedHashSet<SystemSoftware>();

	@MemberOrder(sequence = "1")
	public Set<SystemSoftware> getAssignedToSystemSoftware() {
		return assignedToSystemSoftware;
	}

	public void setAssignedToSystemSoftware(
			final Set<SystemSoftware> assignedSystemSoftware) {
		this.assignedToSystemSoftware = assignedSystemSoftware;
	}

	public void addToAssignedToSystemSoftware(
			final SystemSoftware systemSoftware) {
		// check for no-op
		if (systemSoftware == null
				|| getAssignedToSystemSoftware().contains(systemSoftware)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		systemSoftware.clearAssignedToDevice();
		// associate arg
		systemSoftware.setAssignedToDevice(this);
		getAssignedToSystemSoftware().add(systemSoftware);
		// additional business logic
		addToAssignedToSystemSoftware(systemSoftware);
	}

	public void removeFromAssignedToSystemSoftware(
			final SystemSoftware systemSoftware) {
		// check for no-op
		if (systemSoftware == null
				|| !getAssignedToSystemSoftware().contains(systemSoftware)) {
			return;
		}
		// dissociate arg
		systemSoftware.setAssignedToDevice(null);
		getAssignedToSystemSoftware().remove(systemSoftware);
		// additional business logic
		onRemoveFromAssignedToSystemSoftware(systemSoftware);
	}

	/**
	 * @param systemSoftware
	 */
	private void onRemoveFromAssignedToSystemSoftware(
			SystemSoftware systemSoftware) {
		// Don't do nothing.

	}

	/**
	 * @param string
	 * @param object
	 * @return
	 */
	public SystemSoftware assignToNewSystemSoftware(@Named("Name") String name,
			@Named("Description") @Optional @MaxLength(2048) String description) {

		return nodeFactory.createSystemSoftware(this, name, description);
	}

	// }}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	/**
	 * @param software
	 */
	public void assignToExistingSystemSoftware(SystemSoftware software) {
		software.modifyAssignedToDevice(this);

	}

	// }}

}
