package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

@XMSEntityNames({
		@XMSEntityName(locale = "es", name = "Dirección de Telecomunicación"),
		@XMSEntityName(locale = "en", name = "Telecom Address") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class TelecomAddress extends Address {

	@ManyToOne
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Categoría"),
			@XMSLocale(locale = "en", caption = "Category") })
	private TelecomAddressCategory category;

	public TelecomAddressCategory getCategory() {
		return category;
	}

	public void setCategory(TelecomAddressCategory category) {
		this.category = category;
	}

}
