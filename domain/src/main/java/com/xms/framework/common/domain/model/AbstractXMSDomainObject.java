package com.xms.framework.common.domain.model;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOHelper;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.DatastoreIdentity;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.VersionStrategy;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.annotation.Programmatic;
import org.apache.isis.applib.annotation.When;
import org.apache.isis.applib.annotation.Where;
import org.apache.isis.applib.clock.Clock;
import org.apache.isis.core.objectstore.jdo.applib.annotations.Auditable;

import com.google.common.collect.ComparisonChain;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.api.exception.XMSRuntimeException;
import com.xms.framework.domain.api.domain.DomainObject;

@Auditable
/*
 * It's mandated by Isis to have IdentityType.DATASTORE or
 * IdentityType.UNSPECIFIED
 */
@PersistenceCapable(identityType = IdentityType.DATASTORE)
@DatastoreIdentity(strategy = IdGeneratorStrategy.IDENTITY)
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE/*
														 * , customStrategy =
														 * "complete-table"
														 */)
@javax.jdo.annotations.Version(strategy = VersionStrategy.VERSION_NUMBER, column = "VERSION")
// //@MappedSuperclass
public abstract class AbstractXMSDomainObject extends
		org.apache.isis.applib.AbstractDomainObject implements DomainObject,
		Comparable<AbstractXMSDomainObject> {

	private DomainObjectContainer domainObjectContainer;

	/*
	 * @GeneratedValue(strategy=GenerationType.SEQUENCE)
	 * 
	 * @Column(length=40, insertable=false, updatable=false,
	 * columnDefinition="BigSerial not null")
	 */
	/*
	 * @GeneratedValue(generator = "xms_generator")
	 * 
	 * @GenericGenerator(name = "xms_generator", strategy = "assigned")
	 * 
	 * @Column(length=40, insertable=false, updatable=false,
	 * columnDefinition="BigSerial not null")
	 */
	// @PrimaryKey
	// @Persistent(valueStrategy =
	// javax.jdo.annotations.IdGeneratorStrategy.UUIDSTRING)
	// Needed to be explicit as this class is not @PersistenceCapable
	// @Persistent(valueStrategy = IdGeneratorStrategy.UUIDSTRING)
	/**
	 * This is the Domain Id.
	 * <p>
	 * It must be the same for a Domain Object through its full life-cycle on
	 * the XMS framework platform. It allows to identify objects universally
	 * (also between different XMS Framework platform instances on different
	 * databases).
	 * <p>
	 * It's different from the Database Id. It's mandatory to have it to avoid
	 * identity problems (two objects overriden when inserting on a Set - due to
	 * a one-to-many relationship - not recognized as different, for example, so
	 * they are overwritten.
	 * <p>
	 * All Database relationships and foreign keys will be created using the
	 * Database Id.
	 * <p>
	 * More info:
	 * <p>
	 * <a href=
	 * "http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma"
	 * >http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-
	 * dilemma</a>
	 * <p>
	 * <a href=
	 * "http://stackoverflow.com/questions/6033905/create-the-perfect-jpa-entity"
	 * >http://stackoverflow.com/questions/6033905/create-the-perfect-jpa-entity
	 * </a>
	 */
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Identificador"),
			@XMSLocale(locale = "en", caption = "Id") }, show = false)
	@Persistent
	private String id;

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Creado por"),
			@XMSLocale(locale = "en", caption = "Created by") }, show = false)
	private String createdByUser;

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(allowsNull = "false")
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Fecha de Creación"),
			@XMSLocale(locale = "en", caption = "Date Created") }, show = false)
	private Date dateCreated;

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Modificado por"),
			@XMSLocale(locale = "en", caption = "Updated by") }, show = false)
	private String updatedByUser;

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(allowsNull = "false")
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Fecha de Modificación"),
			@XMSLocale(locale = "en", caption = "Date Updated") }, show = false)
	private Date dateUpdated;

	@Override
	@Disabled
	@Persistent
	@MemberOrder(sequence = "20001")
	@Hidden(where = Where.ALL_TABLES)
	@Optional
	@Named("Domain Id.")
	public String getId() {
		return id;
	}

	// TODO: It should be private or protected if the Object-Store supports it.
	// It has no-sense for the Domain to be able to set it.
	@Override
	public void setId(String id) {
		if (this.id != null && this.id != id) {
			// The new id can be null when JDO replaces all fields that are not
			// the primary key...
			if (id != null) {
				throw new XMSRuntimeException(
						String.format(
								"The Id. cannot be changed once assigned! Current id: %s. New id: %s",
								this.id, id));
			}
		}
		this.id = id;
	}

	public String defaultId() {
		// This will not be assigned here to admit the option to pass an initial
		// Domain Id for the Domain Object. As it will not be able to change
		// after first assignment it cannot have a default value. It must be
		// assured that the setId() method is created just after calling
		// newTransientInstance() for avoiding referencing it in a hash-based
		// Collection with a null Id. See tests for this.

		// Assign the Domain Id. on object creation.
		// return UUID.randomUUID().toString();
		return null;
	}

	// {{ Version (derived property)
	@Hidden(where = Where.ALL_TABLES)
	@Disabled
	@MemberOrder(name = "Detail", sequence = "99")
	@Named("Version")
	public Long getVersionSequence() {
		if (!(this instanceof PersistenceCapable)
				|| !(!this.domainObjectContainer.isPersistent(this))) {
			return null;
		} else {
			PersistenceCapable persistenceCapable = (PersistenceCapable) this;
			final Long version = (Long) JDOHelper
					.getVersion(persistenceCapable);
			return version;
		}
	}

	public boolean hideVersionSequence() {
		return !(this instanceof PersistenceCapable)
				|| !(!this.domainObjectContainer.isPersistent(this));
	}

	// }}

	@Hidden(where = Where.ALL_TABLES)
	@Override
	@Optional
	@Disabled
	@MemberOrder(sequence = "10000")
	public String getCreatedByUser() {
		return createdByUser;
	}

	@Override
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}

	@Hidden(where = Where.ALL_TABLES)
	@Override
	@Optional
	@Disabled
	@MemberOrder(sequence = "10001")
	public Date getDateCreated() {
		return dateCreated;
	}

	@Override
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Hidden(where = Where.ALL_TABLES)
	@Override
	@Optional
	@Disabled
	@MemberOrder(sequence = "10002")
	public String getUpdatedByUser() {
		return updatedByUser;
	}

	@Override
	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	@Hidden(where = Where.ALL_TABLES)
	@Override
	@Optional
	@Disabled
	@MemberOrder(sequence = "10004")
	public Date getDateUpdated() {
		return dateUpdated;
	}

	@Override
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	// @PrePersist
	// For JDO it should be called jdoPreStore and implement a JDO interface.
	// Search for more info if needed.
	public void persisting() {

		// Assign default values.
		Date currentDateTime = Clock.getTimeAsDateTime().toDate();
		if (getDateCreated() == null)
			setDateCreated(currentDateTime);
		setDateUpdated(currentDateTime);

		Logger.getLogger("XMS").log(Level.SEVERE, "Creation values assigned");

		assert getContainer() != null : "The DomainObjectContainer should be injected!";
		assert getContainer().getUser() != null : "The User is not returned!";

		String currentUser = getContainer().getUser().getName();
		if (getCreatedByUser() == null)
			setCreatedByUser(currentUser);
		setUpdatedByUser(currentUser);

		// Verify the Domain Id. is assigned.
		if (getId() == null) {
			Logger.getLogger("XMS").log(
					Level.SEVERE,
					String.format(
							"The (Domain) Id. for a XMS Domain Object wasn't set on time. "
									+ "uuid: %s, className: %s", getId(), this
									.getClass().getName()));
			throw new XMSRuntimeException(String.format(
					"The (Domain) Id. for a XMS Domain Object wasn't set on time. "
							+ "uuid: %s, className: %s", getId(), this
							.getClass().getName()));
		}
	}

	public void updating() {
		// Assign values. Does it launches a StackOverflow? Perhaps due to
		// modifying Domain Object properties inside the persisted() lifecycle
		// method.
		setDateUpdated(Clock.getTimeAsDateTime().toDate());
		setUpdatedByUser(getContainer().getUser().getName());
	}

	@Optional
	@Hidden
	@Override
	public Integer getVersion() {
		// TODO: Change name to getVersionSequence() as in Apache Isis to avoid
		// conflicts. See implementation above.
		return null;
	}

	@Override
	public void setVersion(Integer version) {
		// TODO: Remove this setter. The version can only be read. It only
		// assigned by the ORM.
	}

	@Override
	@Programmatic
	@Hidden(when = When.ALWAYS, where = Where.ANYWHERE)
	public int compareTo(
			@Named("Other Object") AbstractXMSDomainObject otherObject) {
		return ComparisonChain.start()
				.compare(this.getId(), otherObject.getId()).result();
	}

	// http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
	// equals() and hashCode() rely on non-changing data only. Thus we
	// guarantee that no matter how field values are changed we won't
	// lose our entity in hash-based Sets.
	// @Override
	// public int hashCode() {
	// return getId().hashCode();
	// }

	// http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
	// Note that I don't use direct field access inside my entity classes and
	// call getters instead. That's because Persistence provider (PP) might
	// want to load entity data lazily. And I don't use
	// this.getClass() == other.getClass()
	// for the same reason. In order to support laziness PP might need to wrap
	// my entity object in some kind of proxy, i.e. subclassing it.
	// @Override
	// public boolean equals(final Object obj) {
	// if (this == obj)
	// return true;
	// if (!(obj instanceof AbstractXMSDomainObject))
	// return false;
	// return getId().equals(((AbstractXMSDomainObject) obj).getId());
	// }

	/**
	 * This is the Database Id.
	 * <p>
	 * It's the Id this Domain Object has on the database.
	 * <p>
	 * The XMS Framework will use instead the "id" (Domain Id) for identifying
	 * objects universally (also between different XMS Framework platform
	 * instances on different databases).
	 * <p>
	 * All Database relationships and foreign keys will be created using the
	 * Database Id.
	 * <p>
	 * More info: <a href=
	 * "http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma"
	 * >http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-
	 * dilemma</a>
	 */
	@Hidden(where = Where.ALL_TABLES)
	@Disabled
	public String getDatabaseId() {
		Object id = JDOHelper.getObjectId(this);
		if (id != null) {
			return id.toString();
		} else {
			return null;
		}
	}

	@Programmatic
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Programmatic
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractXMSDomainObject other = (AbstractXMSDomainObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
