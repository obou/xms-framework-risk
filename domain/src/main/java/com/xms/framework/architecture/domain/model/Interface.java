package com.xms.framework.architecture.domain.model;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class Interface extends Entity {

	// {{ Composes (Collection)
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getComposes() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.COMPOSITION);
	}

	public void addToComposes(
			final ActiveStructureElement activeStructureElement) {

		// It's the inverse Relationship.
		activeStructureElement.addToComposedOf(this);

	}

	public void removeFromComposes(
			final ActiveStructureElement activeStructureElement) {

		// Is the inverse relationship.
		activeStructureElement.removeFromComposedOf(this);

	}

	// }}

	// {{ AssignedTo
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getAssignedTo() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.ASSIGNMENT);
	}

	public void addToAssignedTo(final Service service) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, service,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (service == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, service,
				RelationshipType.ASSIGNMENT);
	}

	public void removeFromAssignedTo(final Service service) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, service,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (service == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ UsedBy (Collection)
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "30")
	public List<Relationship> getUsedBy() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.USED_BY);
	}

	public void addToUsedBy(final ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.USED_BY);
		// Check for no-op
		if (activeStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, activeStructureElement,
				RelationshipType.USED_BY);
	}

	public void removeFromUsedBy(
			final ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.USED_BY);
		// Check for no-op
		if (activeStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}
	// }}

}
