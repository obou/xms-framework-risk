package com.xms.framework.party.domain.model;

import java.util.Set;

import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;
import com.xms.framework.party.domain.model.address.AddressParty;
import com.xms.framework.party.domain.model.organization.ResponsibilityParty;

@XMSEntityNames({
		@XMSEntityName(locale = "es", name = "Organización / Persona"),
		@XMSEntityName(locale = "en", name = "Party") })
//@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "DTYPE", length = 64)
@PersistenceCapable
@javax.jdo.annotations.Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class Party extends AbstractMultiTenantEntity {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Es Externa"),
			@XMSLocale(locale = "en", caption = "Is External") })
	private Boolean isExternal;

	private String userName;

	@OneToMany(mappedBy = "party", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Direcciones"),
			@XMSLocale(locale = "en", caption = "Addresses") })
	private Set<AddressParty> addresses;

	@OneToMany(mappedBy = "party", cascade = CascadeType.REMOVE)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Responsabilidades"),
			@XMSLocale(locale = "en", caption = "Responsibilities") })
	private Set<ResponsibilityParty> responsibilities;

	public Boolean getIsExternal() {
		return isExternal;
	}

	public void setIsExternal(Boolean isExternal) {
		this.isExternal = isExternal;
	}

	public Set<AddressParty> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<AddressParty> addresses) {
		this.addresses = addresses;
	}

	public Set<ResponsibilityParty> getResponsibilities() {
		return responsibilities;
	}

	public void setResponsibilities(Set<ResponsibilityParty> responsibilities) {
		this.responsibilities = responsibilities;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
