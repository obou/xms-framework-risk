package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Unique;
import javax.jdo.annotations.Uniques;

import org.apache.isis.applib.annotation.Bounded;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.MaxLength;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.MultiLine;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.annotation.Title;
import org.apache.isis.applib.annotation.Where;
import org.apache.isis.core.objectstore.jdo.applib.annotations.Auditable;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.domain.api.domain.MultiTenantEntity;

/**
 * Represents any Entity who has information
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM
 *         <http://GESCONSULTOR.COM></a>. All rights reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@javax.jdo.annotations.PersistenceCapable
@javax.jdo.annotations.Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
@Uniques({ @Unique(name = "unique_name_per_tenant", members = { "tenant, name" }) })
@Auditable
@Bounded
// //@MappedSuperclass
public abstract class AbstractMultiTenantEntity extends
		AbstractMultiTenantObject implements MultiTenantEntity {

	/**
	 * Name of this Entity.
	 */

	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(allowsNull = "false")
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Nombre"),
			@XMSLocale(locale = "en", caption = "Name") }, required = true)
	private String name;

	/**
	 * Description of this Entity.
	 * <p>
	 * Use it to document anything relevant that should be accessible any time
	 * anybody views this Entity.
	 */
	@Column(length = 10000)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Descripci�n"),
			@XMSLocale(locale = "en", caption = "Description") })
	private String description;

	@Override
	@Title(prepend = "")
	@MemberOrder(sequence = "-10000")
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		if (name.equals(""))
			throw new IllegalArgumentException("Name cannot be an empty string");
		this.name = name;
	}

	@Override
	@Optional
	@MultiLine(numberOfLines = 5)
	@MaxLength(2048)
	@Hidden(where = Where.ALL_TABLES)
	@MemberOrder(sequence = "-9000")
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

}
