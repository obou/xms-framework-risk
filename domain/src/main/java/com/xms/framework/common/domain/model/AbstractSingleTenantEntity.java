package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import org.apache.isis.applib.annotation.Title;

import com.xms.framework.domain.api.domain.SingleTenantEntity;

@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
// //@MappedSuperclass
public abstract class AbstractSingleTenantEntity extends
		AbstractSingleTenantObject implements SingleTenantEntity {

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(allowsNull = "false")
	private String name;

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(length = 10000)
	private String description;

	@Override
	@Title
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
}
