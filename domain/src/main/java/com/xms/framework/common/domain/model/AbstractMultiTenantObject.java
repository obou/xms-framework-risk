package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Persistent;

import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.annotation.Where;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.domain.api.domain.MultiTenantDomainObject;

@javax.jdo.annotations.PersistenceCapable
@javax.jdo.annotations.Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
// //@MappedSuperclass
public abstract class AbstractMultiTenantObject extends AbstractXMSDomainObject
		implements MultiTenantDomainObject {

	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(name = "tenantId")
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Identificador del Cliente"),
			@XMSLocale(locale = "en", caption = "Tenant Id") }, show = false)
	private String tenantId;

	@Override
	@Optional
	@Disabled
	@Hidden(where = Where.ALL_TABLES)
	@MemberOrder(sequence = "20000")
	public String getTenantId() {
		return tenantId;
	}

	@Override
	public void setTenantId(String tenantId) {
		if (tenantId != this.getTenantId()) {
			if (tenantId.equals(""))
				throw new IllegalArgumentException(
						"The Tenant Id. cannot be an empty string");

			if (this.tenantId != null)
				throw new IllegalArgumentException(
						String.format(
								"The Tenant Id. cannot be changed once assigned. Current value: '%s'. New value to assign: '%s'",
								this.tenantId, tenantId));
			this.tenantId = tenantId;
		}
	}
}
