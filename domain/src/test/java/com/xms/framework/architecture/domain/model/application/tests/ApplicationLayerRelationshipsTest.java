package com.xms.framework.architecture.domain.model.application.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.xms.framework.architecture.domain.model.application.ApplicationComponent;
import com.xms.framework.architecture.domain.model.application.ApplicationInterface;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class ApplicationLayerRelationshipsTest extends ApplicationLayerTest {

	@Test
	public void testAplicationComponentComposedOfApplicationInterface() {

		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("component", null);

		ApplicationInterface applicationInterface = getApplicationInterfaceFactory()
				.createApplicationInterface("interface", null);

		assertTrue(applicationComponent.getComposedOf().size() == 0);
		assertTrue(applicationInterface.getComposes().size() == 0);
		applicationComponent
				.composedOfApplicationInterface(applicationInterface);
		assertTrue(applicationComponent.getComposedOf().size() == 1);
		assertTrue(applicationInterface.getComposes().size() == 1);
	}
}
