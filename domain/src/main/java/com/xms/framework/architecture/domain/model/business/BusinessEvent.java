package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.BehaviorElement;
import com.xms.framework.architecture.domain.model.PassiveStructureElement;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipFactory;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance
public class BusinessEvent extends BehaviorElement {

	// {{ Triggers
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getTriggers() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.TRIGGERING);
	}

	// A Business Event can trigger a Business Process or any descendant
	// (Business Function or Business Interaction).
	protected void addToTriggers(final BusinessProcess businessProcess) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, businessProcess,
						RelationshipType.TRIGGERING);
		// Check for no-op
		if (businessProcess == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, businessProcess,
				RelationshipType.TRIGGERING);
	}

	protected void removeFromTriggers(final BusinessProcess businessProcess) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, businessProcess,
						RelationshipType.TRIGGERING);
		// Check for no-op
		if (businessProcess == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	public void triggersBusinessProcessFunctionOrInteraction(
			BusinessProcess businessProcess) {
		this.addToTriggers(businessProcess);
	}

	public void notTriggersBusinessProcessFunctionOrInteraction(
			BusinessProcess businessProcess) {
		this.removeFromTriggers(businessProcess);
	}

	// }}

	// {{ TriggeredBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getTriggeredBy() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.TRIGGERING);
	}

	public void triggeredByBusinessProcessFunctionOrInteraction(
			final BusinessProcess businessProcess) {

		// It's the inverse Relationship.
		businessProcess.triggersBusinessEvent(this);

	}

	public void notTriggeredByBusinessProcessFunctionOrInteraction(
			final BusinessProcess businessProcess) {

		// Is the inverse relationship.
		businessProcess.notTriggersBusinessEvent(this);

	}

	// }}

	// {{ Accesses
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getAccesses() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.ACCESS);
	}

	protected void addToAccesses(
			final PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, passiveStructureElement,
				RelationshipType.ACCESS);
	}

	protected void removeFromAccesses(
			final PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	public void accessesBusinessObject(BusinessObject businessObject) {
		this.addToAccesses(businessObject);
	}

	public void notAccessesBusinessObject(BusinessObject businessObject) {
		this.removeFromAccesses(businessObject);
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	protected RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	// }}

}
