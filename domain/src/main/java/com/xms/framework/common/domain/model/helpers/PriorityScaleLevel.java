package com.xms.framework.common.domain.model.helpers;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Scale Level used for assessing the Priority of a Business BusinessProcess.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class PriorityScaleLevel extends GenericScaleLevelBin {

	@ManyToOne
	private PriorityScale priorityScale;

	public PriorityScale getPriorityScale() {
		return priorityScale;
	}

	public void setPriorityScale(PriorityScale priorityScale) {
		this.priorityScale = priorityScale;
	}

	@Override
	public GenericScale getScale() {
		return priorityScale;
	}

}
