package com.xms.framework.common.domain.model.helpers;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity;

/**
 * Element of a Matrix with Scale Levels as Rows and Columns.
 * 
 * The Element itself is a Scale Level.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@MappedSuperclass
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class GenericScaleMatrixElement extends
		AbstractMultiTenantUnnamedEntity {

	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScaleMatrix genericScaleMatrix;

	/**
	 * Level of the Row.
	 */
	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScaleLevel rowLevel;

	/**
	 * Level of the Column.
	 */
	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScaleLevel columnLevel;

	/**
	 * Level of the Element on that Row and Column.
	 */
	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScaleLevel elementLevel;

	public abstract GenericScaleLevel getRowLevel();

	public abstract GenericScaleLevel getColumnLevel();

	public abstract GenericScaleLevel getElementLevel();

	public abstract GenericScaleMatrix getGenericScaleMatrix();

	@Override
	public String getName() {
		return getGenericScaleMatrix().getName() + ": "
				+ getRowLevel().getName() + " - " + getColumnLevel().getName();
	}
}
