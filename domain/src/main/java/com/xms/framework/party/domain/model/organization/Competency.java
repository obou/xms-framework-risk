package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Competencia"),
		@XMSEntityName(locale = "en", name = "Competency") })
//@Entity
@PersistenceCapable
public class Competency extends AbstractMultiTenantEntity {

	@Enumerated(EnumType.STRING)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Tipo"),
			@XMSLocale(locale = "en", caption = "Type") })
	private CompetencyType type;

	public CompetencyType getType() {
		return type;
	}

	public void setType(CompetencyType type) {
		this.type = type;
	}

}
