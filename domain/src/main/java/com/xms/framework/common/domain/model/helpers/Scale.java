package com.xms.framework.common.domain.model.helpers;

import java.util.List;
import java.util.Locale;
/**
 * Generic interface for a qualitative or semi-quantitative Scale.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <L>
 *            Scale Level interface for this scale.
 * @param <V>
 *            Type for values managed by the Levels of this scale.
 */
public interface Scale<L extends ScaleLevel<?, V>, V extends Number> {

	/**
	 * @return Levels of this qualitative or semi-quantitative Scale.
	 */
	public List<L> getLevels();

	/**
	 * @param locale
	 *            Locale the User wants the Name. If a name on
	 *            that Locale does not exists, the implementation must return
	 *            the most similar one. If no similar language names
	 *            exist, the implementation must return the Name it
	 *            chooses, instead of returning an empty string.
	 * 
	 *            For example, if the User asks for an "American English"
	 *            name and only a "Spanish" one exists, the "Spanish" one
	 *            will be returned. If a "British English" name exists it
	 *            will be returned instead of the "Spanish" one.
	 * @return Translated name.
	 */
	public String getScaleName(Locale locale);

	/**
	 * @param locale
	 *            Locale the User wants the Description. If a description on
	 *            that Locale does not exists, the implementation must return
	 *            the most similar one. If no similar language descriptions
	 *            exist, the implementation must return the Description it
	 *            chooses, instead of returning an empty string.
	 * 
	 *            For example, if the User asks for an "American English"
	 *            description and only a "Spanish" one exists, the "Spanish" one
	 *            will be returned. If a "British English" description exists it
	 *            will be returned instead of the "Spanish" one.
	 * @return Translated description.
	 */
	public String getScaleDescription(Locale locale);

	/**
	 * @param value
	 *            Value that must be evaluated.
	 * @return The first Scale Level to which the value is included.
	 */
	public L getScaleLevelForValue(V value);

}