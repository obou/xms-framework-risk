package com.xms.framework.common.domain.model;

import org.apache.isis.applib.annotation.Hidden;

import com.xms.framework.domain.api.domain.MultiTenantDomainObject;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class AbstractMultiTenantObjectRepositoryAndFactory extends
		AbstractDomainObjectRepositoryAndFactory {

	protected <T extends MultiTenantDomainObject> T newTransientMultiTenantObject(
			final Class<T> ofType, String id) {

		T entity = newTransientDomainObject(ofType, id);
		entity.setTenantId(this.currentTenantId());
		return entity;
	}

	/**
	 * @return The current user's Tenant Id.
	 */
	@Hidden
	public String currentTenantId() {
		// TODO Return the proper tenant id.
		return "TENANT";
	}

	protected <T extends MultiTenantDomainObject> T newPersistentMultiTenantObject(
			final Class<T> ofType, String id) {

		T entity = newTransientMultiTenantObject(ofType, id);
		persist(entity);
		return entity;
	}

}
