package com.xms.framework.common.domain.model;

import com.xms.framework.domain.api.domain.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public abstract class AbstractSingleTenantEntityRepositoryAndFactory<T extends Entity>
		extends AbstractDomainObjectRepositoryAndFactory {

	protected T newTransientEntity(final Class<T> ofType, String id,
			String name, String description) {

		T entity = newTransientDomainObject(ofType, id);
		entity.setName(name);
		entity.setDescription(description);
		return entity;

	}

	protected T newPersistentEntity(final Class<T> ofType, String id,
			String name, String description) {

		T entity = newTransientEntity(ofType, id, name, description);
		persist(entity);
		return entity;

	}

}
