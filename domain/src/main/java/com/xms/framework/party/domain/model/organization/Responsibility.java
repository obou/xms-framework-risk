package com.xms.framework.party.domain.model.organization;

import java.util.Set;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * Represents an XMSAsset managed through an XMSAsset Information System. It can
 * be the main XMS System or an internal XMS Sub-System), or an External System
 * (such as a Monitoring System, Service Desk System, SIEM, CMDB, etc.).
 * <p>
 * On <b>ISO 27032:2011 Cybersecurity</b> there is an explicit taxonomy of
 * Physical and Virtual Assets, and of Personal and Organizational Assets.
 * <p>
 * <b>ISO 27032:2011 Cybersecurity</b> An asset is anything that has value to an
 * individual or an organization. There are many types of assets, including but
 * not limited to:
 * <ul>
 * <li>a) information;</li>
 * <li>b) software, such as a computer program;</li>
 * <li>c) physical, such as a computer;</li>
 * <li>d) services;</li>
 * <li>e) people, their qualifications, skills, and experience; and</li>
 * <li>f) intangibles, such as reputation and image.</li>
 * </ul>
 * <p>
 * NOTE 1 Often, assets are simplistically seen only as information or
 * resources.
 * <p>
 * NOTE 2 ISO/IEC 15408-1:2005 defines an asset as information or resources to
 * be protected by controls of a TOE (target of evaluation).
 * <p>
 * NOTE 3 ISO/IEC 19770-1 has been developed to enable an organization to prove
 * that it is performing Software XMSAsset Management (SAM) to a standard
 * sufficient to satisfy corporate governance requirements and ensure effective
 * support for IT service management overall. ISO/IEC 19770 is intended to align
 * closely to, and to support, ISO/IEC 20000.
 * <p>
 * NOTE 4 ISO/IEC 20000-1 promotes the adoption of an integrated process
 * approach when establishing, implementing, operating, monitoring, measuring,
 * reviewing and improving a Service Management System (SMS) to design and
 * deliver services which meet business needs and customer requirements.
 * <p>
 * For the purpose of this International Standard, assets in the Cyberspace are
 * classified into the following classes:
 * <ul>
 * <li>personal; and</li>
 * <li>organizational</li>
 * </ul>
 * For both classes, an asset can also be further classified as
 * <ul>
 * <li>a physical asset, whose form exists in the real world, or</li>
 * <li>a virtual asset, which only exists in the Cyberspace and cannot be seen
 * or touched in the real world.</li>
 * </ul>
 * 
 * @author Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 */
@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Responsabilidad"),
		@XMSEntityName(locale = "en", name = "Responsibility") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Responsibility extends AbstractMultiTenantEntity {

	/*
	 * @ManyToOne private ManagementSystem managementSystem;
	 */

	/**
	 * Parties responsables
	 */
	@OneToMany(mappedBy = "responsibility")
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Personas / Organizaciones"),
			@XMSLocale(locale = "en", caption = "Parties") })
	private Set<ResponsibilityParty> parties;

	public Set<ResponsibilityParty> getParties() {
		return parties;
	}

	public void setParties(Set<ResponsibilityParty> parties) {
		this.parties = parties;
	}

	/*
	 * public ManagementSystem getManagementSystem() { return managementSystem;
	 * }
	 * 
	 * public void setManagementSystem(ManagementSystem managementSystem) {
	 * this.managementSystem = managementSystem; }
	 */

}
