package com.xms.framework.architecture.domain.model.application.tests;

import org.apache.isis.viewer.junit.Service;
import org.apache.isis.viewer.junit.Services;
import org.junit.Before;

import com.xms.framework.architecture.domain.model.RelationshipFactory;
import com.xms.framework.architecture.domain.model.RelationshipRepository;
import com.xms.framework.architecture.domain.model.RelationshipRepositoryIsis;
import com.xms.framework.architecture.domain.model.application.ApplicationCollaborationFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationCollaborationRepository;
import com.xms.framework.architecture.domain.model.application.ApplicationComponentFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationComponentRepository;
import com.xms.framework.architecture.domain.model.application.ApplicationFunctionFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationFunctionRepository;
import com.xms.framework.architecture.domain.model.application.ApplicationInteractionFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationInteractionRepository;
import com.xms.framework.architecture.domain.model.application.ApplicationInterfaceFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationInterfaceRepository;
import com.xms.framework.architecture.domain.model.application.ApplicationServiceFactory;
import com.xms.framework.architecture.domain.model.application.ApplicationServiceRepository;
import com.xms.framework.architecture.domain.model.application.DataObjectFactory;
import com.xms.framework.architecture.domain.model.application.DataObjectRepository;
import com.xms.framework.frameworks.apache.isis.test.AbstractDomainTest;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Services(value = { @Service(ApplicationComponentRepository.class),
		@Service(ApplicationComponentFactory.class),
		@Service(ApplicationInterfaceRepository.class),
		@Service(ApplicationInterfaceFactory.class),
		@Service(ApplicationServiceRepository.class),
		@Service(ApplicationServiceFactory.class),
		@Service(ApplicationFunctionRepository.class),
		@Service(ApplicationFunctionFactory.class),
		@Service(DataObjectRepository.class),
		@Service(DataObjectFactory.class),
		@Service(ApplicationCollaborationRepository.class),
		@Service(ApplicationCollaborationFactory.class),
		@Service(ApplicationInteractionRepository.class),
		@Service(ApplicationInteractionFactory.class),
		@Service(RelationshipFactory.class),
		@Service(RelationshipRepositoryIsis.class) })
public abstract class ApplicationLayerTest extends AbstractDomainTest {

	// {{ injected: ApplicationComponentRepository
	private ApplicationComponentRepository applicationComponentRepository;

	public void setApplicationComponentRepository(
			final ApplicationComponentRepository applicationComponentRepository) {
		this.applicationComponentRepository = applicationComponentRepository;
	}

	public ApplicationComponentRepository getApplicationComponentRepository() {
		return applicationComponentRepository;
	}

	// }}

	// {{ injected: ApplicationComponentFactory
	private ApplicationComponentFactory applicationComponentFactory;

	public void setApplicationComponentFactory(
			final ApplicationComponentFactory applicationComponentFactory) {
		this.applicationComponentFactory = applicationComponentFactory;
	}

	public ApplicationComponentFactory getApplicationComponentFactory() {
		return applicationComponentFactory;
	}

	// }}

	// {{ injected: ApplicationInterfaceRepository
	private ApplicationInterfaceRepository applicationInterfaceRepository;

	public void setApplicationInterfaceRepository(
			final ApplicationInterfaceRepository applicationInterfaceRepository) {
		this.applicationInterfaceRepository = applicationInterfaceRepository;
	}

	public ApplicationInterfaceRepository getApplicationInterfaceRepository() {
		return applicationInterfaceRepository;
	}

	// }}

	// {{ injected: ApplicationInterfaceFactory
	private ApplicationInterfaceFactory applicationInterfaceFactory;

	public void setApplicationInterfaceFactory(
			final ApplicationInterfaceFactory applicationInterfaceFactory) {
		this.applicationInterfaceFactory = applicationInterfaceFactory;
	}

	public ApplicationInterfaceFactory getApplicationInterfaceFactory() {
		return applicationInterfaceFactory;
	}

	// }}

	// {{ injected: ApplicationServiceFactory
	private ApplicationServiceFactory applicationServiceFactory;

	public void setApplicationServiceFactory(
			final ApplicationServiceFactory applicationServiceFactory) {
		this.applicationServiceFactory = applicationServiceFactory;
	}

	public ApplicationServiceFactory getApplicationServiceFactory() {
		return applicationServiceFactory;
	}

	// }}

	// {{ injected: ApplicationServiceRepository
	private ApplicationServiceRepository applicationServiceRepository;

	public void setApplicationServiceRepository(
			final ApplicationServiceRepository applicationServiceRepository) {
		this.applicationServiceRepository = applicationServiceRepository;
	}

	public ApplicationServiceRepository getApplicationServiceRepository() {
		return applicationServiceRepository;
	}

	// }}

	// {{ injected: ApplicationFunctionFactory
	private ApplicationFunctionFactory applicationFunctionFactory;

	public void setApplicationFunctionFactory(
			final ApplicationFunctionFactory applicationFunctionFactory) {
		this.applicationFunctionFactory = applicationFunctionFactory;
	}

	public ApplicationFunctionFactory getApplicationFunctionFactory() {
		return applicationFunctionFactory;
	}

	// }}

	// {{ injected: ApplicationFunctionRepository
	private ApplicationFunctionRepository applicationFunctionRepository;

	public void setApplicationFunctionRepository(
			final ApplicationFunctionRepository applicationFunctionRepository) {
		this.applicationFunctionRepository = applicationFunctionRepository;
	}

	public ApplicationFunctionRepository getApplicationFunctionRepository() {
		return applicationFunctionRepository;
	}

	// }}

	// {{ injected: DataObjectFactory
	private DataObjectFactory dataObjectFactory;

	public void setDataObjectFactory(final DataObjectFactory dataObjectFactory) {
		this.dataObjectFactory = dataObjectFactory;
	}

	public DataObjectFactory getDataObjectFactory() {
		return dataObjectFactory;
	}

	// }}

	// {{ injected: DataObjectRepository
	private DataObjectRepository dataObjectRepository;

	public void setDataObjectRepository(
			final DataObjectRepository dataObjectRepository) {
		this.dataObjectRepository = dataObjectRepository;
	}

	public DataObjectRepository getDataObjectRepository() {
		return dataObjectRepository;
	}

	// }}

	// {{ injected: ApplicationCollaborationFactory
	private ApplicationCollaborationFactory applicationCollaborationFactory;

	public void setApplicationCollaborationFactory(
			final ApplicationCollaborationFactory applicationCollaborationFactory) {
		this.applicationCollaborationFactory = applicationCollaborationFactory;
	}

	public ApplicationCollaborationFactory getApplicationCollaborationFactory() {
		return applicationCollaborationFactory;
	}

	// }}

	// {{ injected: ApplicationCollaborationRepository
	private ApplicationCollaborationRepository applicationCollaborationRepository;

	public void setApplicationCollaborationRepository(
			final ApplicationCollaborationRepository applicationCollaborationRepository) {
		this.applicationCollaborationRepository = applicationCollaborationRepository;
	}

	public ApplicationCollaborationRepository getApplicationCollaborationRepository() {
		return applicationCollaborationRepository;
	}

	// }}

	// {{ injected: ApplicationInteractionFactory
	private ApplicationInteractionFactory applicationInteractionFactory;

	public void setApplicationInteractionFactory(
			final ApplicationInteractionFactory applicationInteractionFactory) {
		this.applicationInteractionFactory = applicationInteractionFactory;
	}

	public ApplicationInteractionFactory getApplicationInteractionFactory() {
		return applicationInteractionFactory;
	}

	// }}

	// {{ injected: ApplicationInteractionRepository
	private ApplicationInteractionRepository applicationInteractionRepository;

	public void setApplicationInteractionRepository(
			final ApplicationInteractionRepository applicationInteractionRepository) {
		this.applicationInteractionRepository = applicationInteractionRepository;
	}

	public ApplicationInteractionRepository getApplicationInteractionRepository() {
		return applicationInteractionRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	public RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	public RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	@Before
	public void wrapInjectedServices() throws Exception {
		applicationComponentFactory = wrapped(applicationComponentFactory);
		applicationComponentRepository = wrapped(applicationComponentRepository);
		applicationInterfaceFactory = wrapped(applicationInterfaceFactory);
		applicationInterfaceRepository = wrapped(applicationInterfaceRepository);
		applicationServiceFactory = wrapped(applicationServiceFactory);
		applicationServiceRepository = wrapped(applicationServiceRepository);
		applicationFunctionFactory = wrapped(applicationFunctionFactory);
		applicationFunctionRepository = wrapped(applicationFunctionRepository);
		dataObjectFactory = wrapped(dataObjectFactory);
		dataObjectRepository = wrapped(dataObjectRepository);
		applicationCollaborationFactory = wrapped(applicationCollaborationFactory);
		applicationCollaborationRepository = wrapped(applicationCollaborationRepository);
		applicationInteractionFactory = wrapped(applicationInteractionFactory);
		applicationInteractionRepository = wrapped(applicationInteractionRepository);
		relationshipFactory = wrapped(relationshipFactory);
		relationshipRepository = wrapped(relationshipRepository);
	}

}
