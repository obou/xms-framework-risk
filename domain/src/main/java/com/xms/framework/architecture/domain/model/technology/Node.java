package com.xms.framework.architecture.domain.model.technology;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import org.apache.isis.applib.annotation.ActionSemantics;
import org.apache.isis.applib.annotation.ActionSemantics.Of;
import org.apache.isis.applib.annotation.MaxLength;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Node extends AbstractNode {

	/***** SERVICES *****/
	private CommunicationPathFactory communicationPathFactory;
	/********************/

	// {{ AssociatedWithCommunicationPaths (Collection)

	// If "cascade-delete" --> Aggregate Root.
	@Element(mappedBy = "node", dependent = "true")
	private Set<NodeCommunicationPath> associatedWithCommunicationPaths = new LinkedHashSet<NodeCommunicationPath>();

	// Apache Isis does not support Value-Objects as parameters. It would appear
	// as an empty list of IpAddress, as they are not persisted Entities.
	// public void addIpAddress(IpAddress ipAddress) {
	//
	// associateWithCommunicationPathOnAddress(
	// communicationPathFactory.ipCommunicationPath(),
	// ipAddress.getIpAddress(), ipAddress.getIpMask());
	//

	public void addToAssociatedWithCommunicationPaths(
			final NodeCommunicationPath nodeAssociatedWithCommunicationPath) {
		// check for no-op
		if (nodeAssociatedWithCommunicationPath == null
				|| getAssociatedWithCommunicationPaths().contains(
						nodeAssociatedWithCommunicationPath)) {
			return;
		}
		// associate new
		getAssociatedWithCommunicationPaths().add(
				nodeAssociatedWithCommunicationPath);
		// additional business logic
		onAddToAssociatedWithCommunicationPaths(nodeAssociatedWithCommunicationPath);
	}

	// As it's equivalent to the getNodeFactory() method and Apache Isis
	// automatically registers the action for all Entities appearing as
	// parameters, it's redundant to declare it here.
	// But as the Ubiquitous Language "requires" that the Node will have this
	// method (as in tests) its better to re-introduce it here and mark the
	// getNodeFactory() methods as "protected" or "@Hidden".
	@MemberOrder(name = "associatedWithCommunicationPaths", sequence = "1")
	public NodeCommunicationPath associateWithCommunicationPath(
			final CommunicationPath communicationPath) {

		return getNodeFactory().createNodeAssociationWithCommunicationPath(
				this, communicationPath);

	}

	// As it's equivalent to the getNodeFactory() method and Apache Isis
	// automatically registers the action for all Entities appearing as
	// parameters, it's redundant to declare it here.
	// But as the Ubiquitous Language "requires" that the Node will have this
	// method (as in tests) its better to re-introduce it here and mark the
	// getNodeFactory() methods as "protected" or "@Hidden".
	@MemberOrder(name = "associatedWithCommunicationPaths", sequence = "2")
	public NodeCommunicationPathAddress associateWithCommunicationPathOnAddress(
			final @Named("Communication Path") CommunicationPath communicationPath,
			final @Named("Address") String address,
			final @Named("Mask") String mask) {

		return getNodeFactory()
				.createNodeAssociationWithCommunicationPathOnAddress(this,
						communicationPath, address, mask);

	}

	@MemberOrder(sequence = "100")
	public Set<NodeCommunicationPath> getAssociatedWithCommunicationPaths() {
		return associatedWithCommunicationPaths;
	}

	// }}

	protected void onAddToAssociatedWithCommunicationPaths(
			final NodeCommunicationPath communicationPathAssociatedWithNode) {
	}

	protected void onRemoveFromAssociatedWithCommunicationPaths(
			final NodeCommunicationPath communicationPathAssociatedWithNode) {
	}

	public void removeFromAssociatedWithCommunicationPaths(
			final NodeCommunicationPath communicationPathAssociatedWithNode) {
		// check for no-op
		if (communicationPathAssociatedWithNode == null
				|| !getAssociatedWithCommunicationPaths().contains(
						communicationPathAssociatedWithNode)) {
			return;
		}
		// dissociate existing
		getAssociatedWithCommunicationPaths().remove(
				communicationPathAssociatedWithNode);
		// additional business logic
		onRemoveFromAssociatedWithCommunicationPaths(communicationPathAssociatedWithNode);
	}

	public void setAssociatedWithCommunicationPaths(
			final Set<NodeCommunicationPath> associatedWithCommunicationPaths) {
		this.associatedWithCommunicationPaths = associatedWithCommunicationPaths;
	}

	// }

	// {{ ComposedOfDevices (Collection)
	@Persistent(mappedBy = "partOfNode")
	private Set<Device> composedOfDevices = new LinkedHashSet<Device>();

	@MemberOrder(sequence = "1")
	public Set<Device> getComposedOfDevices() {
		return composedOfDevices;
	}

	public void setComposedOfDevices(final Set<Device> composedOfDevices) {
		this.composedOfDevices = composedOfDevices;
	}

	public void addToComposedOfDevices(final Device device) {
		// check for no-op
		if (device == null || getComposedOfDevices().contains(device)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		device.clearPartOfNode();
		// associate arg
		device.setPartOfNode(this);
		getComposedOfDevices().add(device);
		// additional business logic
		onAddToComposedOfDevices(device);
	}

	public void removeFromComposedOfDevices(final Device device) {
		// check for no-op
		if (device == null || !getComposedOfDevices().contains(device)) {
			return;
		}
		// dissociate arg
		device.setPartOfNode(null);
		getComposedOfDevices().remove(device);
		// additional business logic
		onRemoveFromComposedOfDevices(device);
	}

	protected void onAddToComposedOfDevices(final Device device) {
	}

	protected void onRemoveFromComposedOfDevices(final Device device) {
	}

	// }}

	public void setCommunicationPathRepository(
			CommunicationPathFactory communicationPathRepository) {
		this.communicationPathFactory = communicationPathRepository;
	}

	/**
	 * @param string
	 * @param string2
	 * @return
	 */
	@MemberOrder(name = "composedOfDevices", sequence = "1")
	public Device composeOfNewDevice(@Named("Name") String name,
			@Named("Description") @Optional @MaxLength(2048) String description) {

		return getNodeFactory().createDevice(this, name, description);
	}

	@MemberOrder(name = "composedOfDevices", sequence = "2")
	public void composeOfExistingDevice(@Named("Device") Device device) {

		device.modifyPartOfNode(this);

	}

	// {{ IP Address.

	/**
	 * 
	 */
	@MemberOrder(sequence = "101")
	public Set<IpAddress> getIpAddresses() {
		Set<IpAddress> set = new HashSet<IpAddress>();
		for (NodeCommunicationPath current : this
				.getAssociatedWithCommunicationPaths()) {
			if (current.getCommunicationPath().equals(
					communicationPathFactory.ipCommunicationPath())) {
				for (NodeCommunicationPathAddress currentAddress : current
						.getAddresses()) {
					set.add(new IpAddress(currentAddress.getAddress(),
							currentAddress.getMask()));
				}
			}
		}
		return set;
	}

	/**
	 * @param ipAddress
	 */
	@ActionSemantics(Of.IDEMPOTENT)
	@MemberOrder(name = "ipAddresses", sequence = "1")
	public NodeCommunicationPathAddress addIpAddress(
			@Named("IP Address") String ipAddress,
			@Named("IP Mask") String ipMask) {

		return getNodeFactory()
				.createNodeAssociationWithCommunicationPathOnAddress(this,
						communicationPathFactory.ipCommunicationPath(),
						ipAddress, ipMask);
	}

	// }}
	//
	// // {{ AssignedToSystemSoftware (Collection)
	// @Element(mappedBy = "assignedTo", dependent = "true")
	// private Set<SystemSoftware> assignedToSystemSoftware = new
	// LinkedHashSet<SystemSoftware>();
	//
	// @MemberOrder(sequence = "1")
	// public Set<SystemSoftware> getAssignedToSystemSoftware() {
	// return assignedToSystemSoftware;
	// }
	//
	// public void setAssignedToSystemSoftware(
	// final Set<SystemSoftware> assignedSystemSoftware) {
	// this.assignedToSystemSoftware = assignedSystemSoftware;
	// }
	//
	// public void addToAssignedToSystemSoftware(
	// final SystemSoftware systemSoftware) {
	// // check for no-op
	// if (systemSoftware == null
	// || getAssignedToSystemSoftware().contains(systemSoftware)) {
	// return;
	// }
	// // dissociate arg from its current parent (if any).
	// systemSoftware.clearAssignedToNode();
	// // associate arg
	// systemSoftware.setAssignedTo(this);
	// getAssignedToSystemSoftware().add(systemSoftware);
	// // additional business logic
	// addToAssignedToSystemSoftware(systemSoftware);
	// }
	//
	// public void removeFromAssignedToSystemSoftware(
	// final SystemSoftware systemSoftware) {
	// // check for no-op
	// if (systemSoftware == null
	// || !getAssignedToSystemSoftware().contains(systemSoftware)) {
	// return;
	// }
	// // dissociate arg
	// systemSoftware.setAssignedTo(null);
	// getAssignedToSystemSoftware().remove(systemSoftware);
	// // additional business logic
	// onRemoveFromAssignedToSystemSoftware(systemSoftware);
	// }
	//
	// /**
	// * @param systemSoftware
	// */
	// private void onRemoveFromAssignedToSystemSoftware(
	// SystemSoftware systemSoftware) {
	// // Don't do nothing.
	//
	// }
	//
	// /**
	// * @param string
	// * @param object
	// * @return
	// */
	// public SystemSoftware assignToNewSystemSoftware(@Named("Name") String
	// name,
	// @Named("Description") @Optional @MaxLength(2048) String description) {
	//
	// return getNodeFactory().createSystemSoftware(this, name, description);
	// }
	//
	// // }}

}
