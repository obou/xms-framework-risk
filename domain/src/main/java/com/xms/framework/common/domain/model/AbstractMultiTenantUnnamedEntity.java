package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.listener.StoreCallback;

import org.apache.isis.applib.annotation.Bounded;
import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.NotPersisted;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.annotation.Title;
import org.apache.isis.applib.annotation.Where;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.domain.api.domain.MultiTenantEntity;

/**
 * Represents any Entity who has information
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM
 *         <http://GESCONSULTOR.COM></a>. All rights reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
@Bounded
// //@MappedSuperclass
public abstract class AbstractMultiTenantUnnamedEntity extends
		AbstractMultiTenantObject implements MultiTenantEntity, StoreCallback {

	/**
	 * Name of this Entity.
	 */
	// @Persistent
	// // Needed to be explicit as this class is not @PersistenceCapable
	// @XMSField(locales = { @XMSLocale(locale = "es", caption = "Nombre"),
	// @XMSLocale(locale = "en", caption = "Name") }, readOnly = true)
	// private String name;

	/**
	 * Description of this Entity.
	 * <p>
	 * Use it to document anything relevant that should be accessible any time
	 * anybody views this Entity.
	 */
	@Persistent
	// Needed to be explicit as this class is not @PersistenceCapable
	@Column(length = 10000)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Descripción"),
			@XMSLocale(locale = "en", caption = "Description") })
	private String description;

	@Optional
	@Override
	@Hidden(where = Where.ALL_TABLES)
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.jdo.listener.StoreCallback#jdoPreStore()
	 */
	@Override
	public void jdoPreStore() {
		this.setName(this.getName());

	}

	@NotPersisted
	@NotPersistent
	@Disabled
	@Title
	public abstract String getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.domain.api.domain.MultiTenantEntity#setName(java.lang
	 * .String)
	 */
	@Override
	public void setName(String name) {
		// Do Nothing. It cannot be set for an Unnamed Entity.

	}

}
