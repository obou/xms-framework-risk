package com.xms.framework.architecture.domain.model.business;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.Interface;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessInterface extends Interface {

	// {{ AssignedTo

	@MemberOrder(name = "assignedTo", sequence = "10")
	public void assignedToBusinessService(BusinessService businessService) {
		super.addToAssignedTo(businessService);
	}

	@MemberOrder(name = "assignedTo", sequence = "10")
	public void notAssignedToBusinessService(BusinessService businessService) {
		super.removeFromAssignedTo(businessService);
	}

	// }}

	// {{ Composes

	@MemberOrder(name = "composes", sequence = "10")
	public void composesBusinessRole(BusinessRole businessRole) {
		super.addToComposes(businessRole);
	}

	@MemberOrder(name = "composes", sequence = "20")
	public void notComposesBusinessRole(BusinessRole businessRole) {
		super.removeFromComposes(businessRole);
	}

	// }}

	// {{ UsedBy

	@MemberOrder(name = "usedBy", sequence = "10")
	public void usedByBusinessRole(BusinessRole businessRole) {
		super.addToUsedBy(businessRole);
	}

	@MemberOrder(name = "usedBy", sequence = "20")
	public void notUsedByBusinessRole(BusinessRole businessRole) {
		super.removeFromUsedBy(businessRole);
	}

	/**
	 * @param businessActor
	 */
	public void usedByBusinessActor(BusinessActor businessActor) {
		super.addToUsedBy(businessActor);
	}

	/**
	 * @param businessActor
	 */
	public void notUsedByBusinessActor(BusinessActor businessActor) {
		super.removeFromUsedBy(businessActor);
	}

	// }}

}
