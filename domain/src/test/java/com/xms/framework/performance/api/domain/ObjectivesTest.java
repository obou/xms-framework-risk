package com.xms.framework.performance.api.domain;

import static org.junit.Assert.assertTrue;

import org.apache.isis.viewer.junit.Service;
import org.apache.isis.viewer.junit.Services;
import org.junit.Before;
import org.junit.Test;

import com.xms.framework.frameworks.apache.isis.test.AbstractApacheIsisTest;
import com.xms.framework.performance.domain.model.Objective;
import com.xms.framework.performance.domain.model.ObjectiveFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Services(value = @Service(ObjectiveFactory.class))
public class ObjectivesTest extends AbstractApacheIsisTest {

	protected ObjectiveFactory objectives;

	@Before
	public void wrapInjectedServices() throws Exception {
		objectives = wrapped(objectives);
	}

	@Before
	public void setUp() {
		// tomEmployee =
		// wrapped(employeeRepository.findEmployees("Tom").get(0));
	}

	@Test
	public void testCreate() {

		// EN EL TEST VA PERFECTO PERO DA UN ERROR DE SingleFieldIdentity.class
		// HAY UN REGISTRO DE BUG DE MONGODB INDICANDO QUE NO ESTABA SOPORTADO.
		//
		// TAL VEZ CUANDO SE UTILIZA ISIS LA CLAVE PRIMARIA DEBA DECLARARLA �L
		// Y NOSOTROS NOS OLVIDAMOS DE ELLA. SER� LO M�S F�CIL. ES COMO LOS
		// EJEMPLOS
		// SI ES AS�, ES SIMPLEMENTE COMENTAR EL C�DIGO.
		//
		// PROBAR Y SI AS� VA ... A OTRA COSA. SEGUIR MODELANDO EL DOMINIO.
		//
		// TAL VEZ PARA DETECTAR DUPLICADOS SIN EXPLICITAR LA CLAVE PRIMARIA
		// SE REQUIERA IMPLEMENTAR LA INTERFAZ COMPARABLE.
		//
		// EN CLAIMS BUSCA EN EL REPOSITORY POR TITLE Y LAS CLASES
		// NO TIENEN CLAVE PRIMARIA.

		Objective objective = wrapped(objectives.create("objective",
				"description"));
		assertTrue(objective.getName() == "objective");
		assertTrue(objective.getDescription() == "description");

	}

	public ObjectiveFactory getObjectives() {
		return objectives;
	}

	public void setObjectives(ObjectiveFactory objectives) {
		this.objectives = objectives;
	}

}
