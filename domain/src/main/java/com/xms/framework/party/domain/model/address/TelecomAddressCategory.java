package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({
		@XMSEntityName(locale = "es", name = "Categoría de la Dirección de Telecomunicación"),
		@XMSEntityName(locale = "en", name = "Telecom Address Category") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class TelecomAddressCategory extends AbstractMultiTenantEntity {

	@Enumerated(EnumType.STRING)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Tipo"),
			@XMSLocale(locale = "en", caption = "Type") })
	private TelecomAddressCategoryType type;

	public TelecomAddressCategoryType getType() {
		return type;
	}

	public void setType(TelecomAddressCategoryType type) {
		this.type = type;
	}

}
