package com.xms.framework.architecture.domain.model.technology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Artifact extends Entity {

	// {{ AssignedToNode (property)
	private AbstractNode assignedToNode;

	@MemberOrder(sequence = "1")
	public AbstractNode getAssignedToNode() {
		return assignedToNode;
	}

	public void setAssignedToNode(final AbstractNode assignedToNode) {
		this.assignedToNode = assignedToNode;
	}

	public void modifyAssignedToNode(final AbstractNode abstractNode) {
		AbstractNode currentAssignedToNode = getAssignedToNode();
		// check for no-op
		if (abstractNode == null || abstractNode.equals(currentAssignedToNode)) {
			return;
		}
		// delegate to parent to associate
		abstractNode.addToAssignedToArtifacts(this);
		// additional business logic
		onModifyAssignedToNode(currentAssignedToNode, abstractNode);
	}

	/**
	 * @param currentAssignedToNode
	 * @param abstractNode
	 */
	private void onModifyAssignedToNode(AbstractNode currentAssignedToNode,
			AbstractNode abstractNode) {
		// TODO Auto-generated method stub

	}

	public void clearAssignedToNode() {
		AbstractNode currentAssignedToNode = getAssignedToNode();
		// check for no-op
		if (currentAssignedToNode == null) {
			return;
		}
		// delegate to parent to dissociate
		currentAssignedToNode.removeFromAssignedToArtifacts(this);
		// additional business logic
		onClearAssignedToNode(currentAssignedToNode);
	}

	/**
	 * @param currentAssignedToNode
	 */
	private void onClearAssignedToNode(AbstractNode currentAssignedToNode) {
		// TODO Auto-generated method stub

	}

	// }}

	// {{ AccessedByInfrastructureFunctions (Collection)
	private Set<InfrastructureFunction> accessedByInfrastructureFunctions = new LinkedHashSet<InfrastructureFunction>();

	@MemberOrder(sequence = "1")
	public Set<InfrastructureFunction> getAccessedByInfrastructureFunctions() {
		return accessedByInfrastructureFunctions;
	}

	public void setAccessedByInfrastructureFunctions(
			final Set<InfrastructureFunction> accessedByInfrastructureFunctions) {
		this.accessedByInfrastructureFunctions = accessedByInfrastructureFunctions;
	}

	public void addToAccessedByInfrastructureFunctions(
			final InfrastructureFunction infrastructureFunction) {
		// check for no-op
		if (infrastructureFunction == null
				|| getAccessedByInfrastructureFunctions().contains(
						infrastructureFunction)) {
			return;
		}
		// delegate to parent to add
		infrastructureFunction.addToAccessesArtifacts(this);
		// additional business logic
		onAddToAccessedByInfrastructureFunctions(infrastructureFunction);
	}

	/**
	 * @param infrastructureFunction
	 */
	private void onAddToAccessedByInfrastructureFunctions(
			InfrastructureFunction infrastructureFunction) {
		// Do nothing.

	}

	public void removeFromAccessedByInfrastructureFunctions(
			final InfrastructureFunction infrastructureFunction) {
		// check for no-op
		if (infrastructureFunction == null
				|| !getAccessedByInfrastructureFunctions().contains(
						infrastructureFunction)) {
			return;
		}
		// delegate to parent to remove
		infrastructureFunction.removeFromAccessesArtifacts(this);
		// additional business logic
		onRemoveFromAccessedByInfrastructureFunctions(infrastructureFunction);
	}

	/**
	 * @param infrastructureFunction
	 */
	private void onRemoveFromAccessedByInfrastructureFunctions(
			InfrastructureFunction infrastructureFunction) {
		// Do nothing.

	}

	// }}

}
