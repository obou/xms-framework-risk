package com.xms.framework.party.domain.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class OrganizationMember extends AbstractMultiTenantEntityRelationship {

	@ManyToOne(optional = false)
	private Organization organization;

	@ManyToOne(optional = false)
	private Person person;

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
