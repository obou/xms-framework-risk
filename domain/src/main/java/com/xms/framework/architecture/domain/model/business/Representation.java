package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Entity;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipFactory;
import com.xms.framework.architecture.domain.model.RelationshipRepository;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Representation extends Entity {

	// {{ Realizes (property)

	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getRealizes() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.REALIZATION);
	}

	/**
	 * @param businessRole
	 */
	// A Product can Aggregate both BusinessServices and Contracts.
	protected void addToRealizes(BusinessObject businessObject) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, businessObject,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (businessObject == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this, businessObject,
				RelationshipType.AGGREGATION);

	}

	/**
	 * @param businessRole
	 */
	protected void removeFromRealizes(BusinessObject businessObject) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, businessObject,
						RelationshipType.REALIZATION);
		// Check for no-op
		if (businessObject == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	public void realizesBusinessObject(BusinessObject businessObject) {
		this.addToRealizes(businessObject);
	}

	public void notRealizesBusinessObject(BusinessObject businessObject) {
		this.removeFromRealizes(businessObject);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	public RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	public RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	public DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	// }}

}
