package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import com.xms.framework.domain.api.domain.SingleTenantEntityRelationship;

@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
// //@MappedSuperclass
public class AbstractSingleTenantEntityRelationship extends
		AbstractSingleTenantObject implements SingleTenantEntityRelationship {

}
