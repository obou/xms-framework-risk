package com.xms.framework.architecture.domain.model.application.tests;

import org.junit.Test;

import com.xms.framework.architecture.domain.model.application.ApplicationInterface;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class ApplicationInterfaceTest extends ApplicationLayerTest {

	@Test
	public void testCreateDefault() {

		ApplicationInterface applicationInterface = getApplicationInterfaceFactory()
				.createApplicationInterface("application Interface", null);

		validateCommonAssertions(applicationInterface);

	}

}
