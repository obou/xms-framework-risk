package com.xms.framework.architecture.domain.model.technology;

//
///**
// * 
// * 
// * 
// * @author <p>
// *         Copyright � 2011-2012, <a
// *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
// *         reserved.
// *         </p>
// *         <p>
// *         It is acknowledged that there may be other brand, company, and
// *         product names used in the XMS Framework that may be covered by
// *         trademark protection and advises the reader to verify them
// *         independently.
// *         </p>
// * 
// */
//@PersistenceCapable
//@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
//public class SystemSoftwareOld extends AbstractNode {
//
//	// {{ Device (property)
//	private AbstractNode assignedTo;
//
//	@MemberOrder(sequence = "1")
//	public AbstractNode getAssignedTo() {
//		return assignedTo;
//	}
//
//	public void setAssignedTo(final AbstractNode abstractNode) {
//		this.assignedTo = abstractNode;
//	}
//
//	public String validateAssignedTo(final AbstractNode abstractNode) {
//		if (abstractNode == null)
//			return null;
//		if (abstractNode instanceof Device || abstractNode instanceof Node)
//			return null;
//		else
//			return "System Software can only be assigned to Devices and Nodes";
//	}
//
//	public void modifyAssignedTo(final AbstractNode abstractNode) {
//		AbstractNode currentAbstractNode = getAssignedTo();
//		// check for no-op
//		if (abstractNode == null || abstractNode.equals(currentAbstractNode)) {
//			return;
//		}
//		// delegate to parent to associate
//
//		// XMS: Nodes are not allowed to directly being assigned to System
//		// Software. A Device must be created. For Hardware Virtualization and
//		// other scenarios is needed that a System Software is assigned to
//		// System Software.
//		if (abstractNode instanceof Device)
//			((Device) abstractNode).addToAssignedToSystemSoftware(this);
//		else if (abstractNode instanceof SystemSoftware)
//			((Node) abstractNode).addToAssignedToSystemSoftware(this);
//
//		// additional business logic
//		onModifyAssignedToNode(currentAbstractNode, abstractNode);
//	}
//
//	/**
//	 * @param currentAbstractNode
//	 * @param abstractNode
//	 */
//	private void onModifyAssignedToNode(AbstractNode currentAbstractNode,
//			AbstractNode abstractNode) {
//		// Do nothing.
//
//	}
//
//	public void clearAssignedToNode() {
//		AbstractNode abstractNode = getAssignedTo();
//		// check for no-op
//		if (abstractNode == null) {
//			return;
//		}
//		// delegate to parent to dissociate
//		// XMS: Nodes are not allowed to directly being assigned to System
//		// Software. A Device must be created. For Hardware Virtualization and
//		// other scenarios is needed that a System Software is assigned to
//		// System Software.
//		if (abstractNode instanceof Device)
//			((Device) abstractNode).removeFromAssignedToSystemSoftware(this);
//		else if (abstractNode instanceof SystemSoftware)
//			((Node) abstractNode).removeFromAssignedToSystemSoftware(this);
//
//		// additional business logic
//		onClearAssignedToAbstractNode(abstractNode);
//	}
//
//	/**
//	 * @param currentAbstractNode
//	 */
//	private void onClearAssignedToAbstractNode(AbstractNode currentAbstractNode) {
//		// Do nothing.
//
//	}
//
//	// }}
//
// }
