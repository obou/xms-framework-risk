package com.xms.framework.common.domain.model.helpers;

import java.util.Locale;

/**
 * Interface for modeling Trees through their Nodes.
 * 
 * Each Node have a Localized Name and Description.
 * 
 * Each Node can have an Object from a generic type <T> attached as information.
 * 
 * It can be implemented by any kind of structures, such as classes or enums.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.

 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.

 * 
 * @param <T>
 *            Type of the Information attached to a Node.
 */
public interface TreeNode<T> {

	/**
	 * @return The Parent Node of this one.
	 */
	public TreeNode<T> getParent();

	/**
	 * @param locale
	 *            The Locale the user wants the Name localized.
	 * @return The localized Name.
	 */
	public String getName(Locale locale);

	/**
	 * @param locale
	 *            The Locale the user wants the Description localized.
	 * @return The localized Description.
	 */
	public String getDescription(Locale locale);

	/**
	 * @return The information attached to the Tree Node. Can be null.
	 */
	public T getInformation();

	/***** The following methods are not needed. *****/

	// Node's child are all those nodes with this one as Parent.
	// public List<TreeNode<T>> getChild();

	// Root Elements are all nodes with no Parent (null).
	// public List<TreeNode<T>> getRootElements();

	// All elements are the collection of all elements at any depth in the Tree.
	// public List<TreeNode<T>> getAllElements();

}