package com.xms.framework.architecture.domain.model;

import java.util.List;

import org.apache.isis.applib.filter.Filter;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class RelationshipRepositoryIsis extends
		AbstractMultiTenantEntityRepositoryAndFactory implements
		RelationshipRepository {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsSource
	 * (com.xms.framework.architecture.domain.model.Entity)
	 */
	@Override
	public List<Relationship> relationshipsAsSource(final Entity entity) {
		return allMatches(Relationship.class, new Filter<Relationship>() {

			@Override
			public boolean accept(Relationship t) {
				return t.getSourceEntityId().equals(entity.getId());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsSource
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public List<Relationship> relationshipsAsSourceOfType(final Entity entity,
			final RelationshipType relationshipType) {
		return allMatches(Relationship.class, new Filter<Relationship>() {

			@Override
			public boolean accept(Relationship t) {
				return t.getSourceEntityId().equals(entity.getId())
						&& t.getType().equals(relationshipType);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsTarget
	 * (com.xms.framework.architecture.domain.model.Entity)
	 */
	@Override
	public List<Relationship> relationshipsAsTarget(final Entity entity) {
		return allMatches(Relationship.class, new Filter<Relationship>() {

			@Override
			public boolean accept(Relationship t) {
				return t.getTargetEntityId().equals(entity.getId());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsTarget
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public List<Relationship> relationshipsAsTargetOfType(final Entity entity,
			final RelationshipType relationshipType) {
		return allMatches(Relationship.class, new Filter<Relationship>() {

			@Override
			public boolean accept(Relationship t) {
				return t.getTargetEntityId().equals(entity.getId())
						&& t.getType().equals(relationshipType);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * relationshipBySourceTargetAndType
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public Relationship relationshipBySourceTargetAndType(
			final Entity sourceEntity, final Entity targetEntity,
			final RelationshipType relationshipType) {
		List<Relationship> result = allMatches(Relationship.class,
				new Filter<Relationship>() {

					@Override
					public boolean accept(Relationship t) {
						return t.getSourceEntityId().equals(
								sourceEntity.getId())
								&& t.getTargetEntityId().equals(
										targetEntity.getId())
								&& t.getType().equals(relationshipType);
					}
				});

		assert result.size() <= 1 : "There's more than 1 Relationship of the same Tenant, Source, Target and Type!!!";

		if (result.size() != 0)
			return result.iterator().next();
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * relationshipEntity(java.lang.Class, java.lang.String)
	 */
	@Override
	public <T> T relationshipEntity(Class<T> clazz, final String id) {
		return firstMatch(clazz, new Filter<T>() {

			@Override
			public boolean accept(T t) {
				return ((Entity) t).getId().equals(id);
			}
		});
	}

}
