package com.xms.framework.architecture.domain.model;

import java.util.List;

import org.apache.isis.applib.annotation.Hidden;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public interface RelationshipRepository {

	List<Relationship> relationshipsAsSource(Entity entity);

	List<Relationship> relationshipsAsSourceOfType(Entity entity,
			RelationshipType relationshipType);

	List<Relationship> relationshipsAsTarget(Entity entity);

	List<Relationship> relationshipsAsTargetOfType(Entity entity,
			RelationshipType relationshipType);

	/**
	 * @param activeStructureElement
	 * @param interfaceInstance
	 * @param composition
	 * @return
	 */
	Relationship relationshipBySourceTargetAndType(Entity sourceEntity,
			Entity targetEntity, RelationshipType relationshipType);

	/**
	 * @param clazz
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Hidden
	<T> T relationshipEntity(Class<T> clazz, String id);

}