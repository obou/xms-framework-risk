package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Entity;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipFactory;
import com.xms.framework.architecture.domain.model.RelationshipRepository;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Product extends Entity {

	// {{ Aggregates
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAggregates() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.AGGREGATION);
	}

	/**
	 * @param businessRole
	 */
	// A Product can Aggregate both BusinessServices and Contracts.
	protected void addToAggregates(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this, entity,
				RelationshipType.AGGREGATION);

	}

	/**
	 * @param businessRole
	 */
	protected void removeFromAggregates(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "10")
	public void aggregatesBusinessService(BusinessService businessService) {
		addToAggregates(businessService);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "20")
	public void notAggregatesBusinessService(BusinessService businessService) {
		removeFromAggregates(businessService);
	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "30")
	public void aggregatesContract(Contract contract) {
		addToAggregates(contract);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "40")
	public void notAggregatesContract(Contract contract) {
		removeFromAggregates(contract);

	}

	// }}

	// {{ AssociatedWith
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssociatedWith() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.ASSOCIATION);
	}

	/**
	 * @param businessRole
	 */
	// A Product can Aggregate both BusinessServices and Contracts.
	protected void addToAssociatedWith(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this, entity,
				RelationshipType.AGGREGATION);

	}

	/**
	 * @param businessRole
	 */
	protected void removeFromAssociatedWith(Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (entity == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "30")
	public void associatedWithEntity(Entity entity) {
		addToAssociatedWith(entity);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "40")
	public void notAssociatedWithEntity(Entity entity) {
		removeFromAssociatedWith(entity);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "10")
	public void associatedWithValue(Value value) {
		associatedWithEntity(value);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "20")
	public void notAssociatedWithValue(Value value) {
		notAssociatedWithEntity(value);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	public RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	public RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	public DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	// }}

}
