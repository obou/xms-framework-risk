package com.xms.framework.architecture.domain.model;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class Service extends Entity {

	// {{ AssignedFrom
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssignedFrom() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.ASSIGNMENT);
	}

	protected void addToAssignedFrom(final Interface interfaceInstance) {

		// It's the inverse Relationship.
		interfaceInstance.addToAssignedTo(this);

	}

	protected void removeFromAssignedFrom(final Interface interfaceInstance) {

		// Is the inverse relationship.
		interfaceInstance.removeFromAssignedTo(this);

	}

	// }}

	// {{ UsedBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getUsedBy() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.USED_BY);
	}

	// UsedBy ApplicationFunction

	/**
	 * @param applicationComponent
	 */
	protected void addToUsedByBehaviorElement(BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.USED_BY);
		// Check for no-op
		if (behaviorElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, behaviorElement,
				RelationshipType.USED_BY);

	}

	/**
	 * @param applicationComponent
	 */
	protected void removeFromUsedByBehaviorElement(
			BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.USED_BY);
		// Check for no-op
		if (behaviorElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);

	}

	// UsedBy ApplicationComponent

	/**
	 * @param applicationComponent
	 */
	protected void addToUsedByActiveStructureElement(
			ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.USED_BY);
		// Check for no-op
		if (activeStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, activeStructureElement,
				RelationshipType.USED_BY);

	}

	/**
	 * @param applicationComponent
	 */
	protected void removeFromUsedByActiveStructureElement(
			ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.USED_BY);
		// Check for no-op
		if (activeStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);

	}

	// }}

	// {{ RealizedBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getRealizedBy() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.REALIZATION);
	}

	protected void addToRealizedBy(final BehaviorElement behaviorElement) {

		// It's the inverse Relationship.
		behaviorElement.addToRealizes(this);

	}

	protected void removeFromRealizedBy(final BehaviorElement behaviorElement) {

		// Is the inverse relationship.
		behaviorElement.removeFromRealizes(this);

	}

	// }}

	// {{ Accesses
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAccesses() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.ACCESS);
	}

	protected void addToAccesses(PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, passiveStructureElement,
				RelationshipType.ACCESS);

	}

	/**
	 * @param applicationComponent
	 */
	protected void removeFromAccesses(
			PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);

	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}
	// }}

}
