package com.xms.framework.architecture.domain.model;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.DescribedAs;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class ActiveStructureElement extends Entity {

	// {{ ComposedOf

	@NotPersisted
	@NotPersistent
	@MemberOrder(sequence = "1")
	@DescribedAs("Published Interfaces")
	public List<Relationship> getComposedOf() {

		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.COMPOSITION);

	}

	protected void addToComposedOf(final Interface interfaceInstance) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, interfaceInstance,
						RelationshipType.COMPOSITION);
		// Check for no-op
		if (interfaceInstance == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, interfaceInstance,
				RelationshipType.COMPOSITION);
	}

	protected void removeFromComposedOf(final Interface interfaceInstance) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, interfaceInstance,
						RelationshipType.COMPOSITION);
		// Check for no-op
		if (interfaceInstance == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ Uses

	@NotPersisted
	@NotPersistent
	@MemberOrder(sequence = "1")
	public List<Relationship> getUses() {

		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.USED_BY);
	}

	protected void usesInterface(final Interface interfaceInstance) {

		// Is the inverse relationship.
		interfaceInstance.addToUsedBy(this);

	}

	protected void notUsesInterface(final Interface interfaceInstance) {

		// It's the inverse relationship.
		interfaceInstance.removeFromUsedBy(this);

	}

	protected void usesService(final Service service) {

		service.addToUsedByActiveStructureElement(this);

	}

	protected void notUsesService(final Service service) {

		service.removeFromUsedByActiveStructureElement(this);

	}

	// }}

	// {{ AssignedTo
	@NotPersisted
	@NotPersistent
	@MemberOrder(sequence = "1")
	public List<Relationship> getAssignedTo() {

		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.ASSIGNMENT);

	}

	protected void addAssignedTo(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (behaviorElement == null || relationship != null) {
			return;
		}

		// Create new
		relationshipFactory.createRelationship(this, behaviorElement,
				RelationshipType.ASSIGNMENT);
	}

	protected String validateAddAssignedTo(final BehaviorElement behaviorElement) {
		// Check for Interactions and Collaborations.
		if ((this instanceof Collaboration)
				&& !(behaviorElement instanceof Interaction)) {
			return "Collaborations can only be assigned to Interactions, and Interactions can only be assigned to Collaborations.";
		}
		return null;
	}

	protected void removeFromAssignedTo(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (behaviorElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	protected RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	protected RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	protected DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	// }}

}
