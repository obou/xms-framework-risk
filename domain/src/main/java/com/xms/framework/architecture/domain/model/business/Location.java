package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Entity;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipFactory;
import com.xms.framework.architecture.domain.model.RelationshipRepository;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Location extends Entity {

	// {{ AssignedTo
	@NotPersisted
	@NotPersistent
	@MemberOrder(sequence = "1")
	public List<Relationship> getAssignedTo() {

		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.ASSIGNMENT);

	}

	protected void addToAssignedTo(final Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (entity == null || relationship != null) {
			return;
		}

		// Create new
		relationshipFactory.createRelationship(this, entity,
				RelationshipType.ASSIGNMENT);
	}

	protected void removeFromAssignedTo(final Entity entity) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, entity,
						RelationshipType.ASSIGNMENT);
		// Check for no-op
		if (entity == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	protected String validateAddAssignedTo(final Entity entity) {
		// Entity, Meaning and Value are the only Entity types not allowed to be
		// assigned to a Location.
		if ((entity instanceof Product) || (entity instanceof Meaning)
				|| (entity instanceof Value)) {
			return "The only Archimate entities that are not allowed to be assigned to Location are 'Product', 'Meaning' or 'Value' as per Archimate version 2.";
		}
		return null;
	}

	// Action available for all Entities.
	public void assignToLocation(Entity entity) {
		this.addToAssignedTo(entity);
	}

	// Action available for all Entities.
	public void notAssignToLocation(Entity entity) {
		this.removeFromAssignedTo(entity);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	protected RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	protected RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	protected DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	// }}

}
