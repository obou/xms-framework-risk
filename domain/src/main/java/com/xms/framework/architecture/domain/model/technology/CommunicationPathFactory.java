package com.xms.framework.architecture.domain.model.technology;

import java.util.List;

import org.apache.isis.applib.annotation.ActionSemantics;
import org.apache.isis.applib.annotation.ActionSemantics.Of;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.MultiLine;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class CommunicationPathFactory extends
		AbstractMultiTenantEntityRepositoryAndFactory {

	// }}

	public CommunicationPath create(
			@Named("Name") String name,
			@Named("Description") @Optional @MultiLine(numberOfLines = 5) String description) {
		return this.newPersistentMultiTenantEntity(CommunicationPath.class,
				nextIdentity(), name, description);
	}

	protected CommunicationPath createIpCommunicationPath() {
		return newPersistentMultiTenantEntity(CommunicationPath.class, this
				.currentTenantId().concat("::IP"), "IP", null);
	}

	// {{ IpCommunicationPath singleton (action)
	@ActionSemantics(Of.SAFE)
	@MemberOrder(sequence = "4")
	public CommunicationPath ipCommunicationPath() {

		final List<CommunicationPath> ipCommunicationPathInstances = allMatches(
				CommunicationPath.class,
				CommunicationPath.filterThoseWithId(this.currentTenantId()
						.concat("::IP")));

		CommunicationPath ipCommunicationPathInstance;
		if (ipCommunicationPathInstances.size() == 0) {
			ipCommunicationPathInstance = createIpCommunicationPath();
		} else {
			// Return the first instance with the "IP" name.
			ipCommunicationPathInstance = ipCommunicationPathInstances.get(0);
		}

		return ipCommunicationPathInstance;
	}
}
