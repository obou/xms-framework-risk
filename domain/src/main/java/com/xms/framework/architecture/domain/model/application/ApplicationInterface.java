package com.xms.framework.architecture.domain.model.application;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.Interface;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
@javax.jdo.annotations.Queries({ @javax.jdo.annotations.Query(name = "entity_by_id", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.application.ApplicationInterface e WHERE e.id == :id") })
public class ApplicationInterface extends Interface {

	// {{ AssignedTo

	@MemberOrder(name = "assignedTo", sequence = "10")
	public void assignedToApplicationService(
			ApplicationService applicationService) {
		super.addToAssignedTo(applicationService);
	}

	@MemberOrder(name = "assignedTo", sequence = "10")
	public void notAssignedToApplicationService(
			ApplicationService applicationService) {
		super.removeFromAssignedTo(applicationService);
	}

	// }}

	// {{ Composes

	@MemberOrder(name = "composes", sequence = "10")
	public void composesApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.addToComposes(applicationComponent);
	}

	@MemberOrder(name = "composes", sequence = "20")
	public void notComposesApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.removeFromComposes(applicationComponent);
	}

	// }}

	// {{ UsedBy

	@MemberOrder(name = "usedBy", sequence = "10")
	public void usedByApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.addToUsedBy(applicationComponent);
	}

	@MemberOrder(name = "usedBy", sequence = "20")
	public void notUsedByApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.removeFromUsedBy(applicationComponent);
	}

	// }}

}
