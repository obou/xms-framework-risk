package com.xms.framework.common.domain.model.helpers;

import java.util.Locale;

/**
 * Represents a Scale Level for qualitative and semi-quantitative Scales.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <S>
 *            Scale this Scale Level is related to.
 * @param <V>
 *            Type used to express the Scale Level value.
 */
public interface ScaleLevel<S extends Scale<?, ?>, V extends Number> {

	/**
	 * @return The Scale class.
	 */
	public S getScale();

	/**
	 * @return The double Value that represents this Level. It can be use for
	 *         prioritazing, ordering and making computations (despite the
	 *         result does not have any predefined meaning on ordinal scales).
	 */
	public V getValue();

	/**
	 * @param locale
	 *            Locale the User wants the Name. If a name on that Locale does
	 *            not exists, the implementation must return the most similar
	 *            one. If no similar language names exist, the implementation
	 *            must return the English name, instead of returning an empty
	 *            string.
	 * 
	 */
	public String getLevelName(Locale locale);

	/**
	 * @param locale
	 *            Locale the User wants the Description. If a description on
	 *            that Locale does not exists, the implementation must return
	 *            the most similar one. If no similar langauge descriptions
	 *            exist, the implementation must return the English name,
	 *            instead of returning an empty string.
	 * 
	 *            For example, if the User asks for an "American English"
	 *            description and only a "Spanish" one exists, the "Spanish" one
	 *            will be returned. If a "British English" description exists it
	 *            will be returned instead of the "Spanish" one, as it's on
	 *            English.
	 * 
	 */
	public String getLevelDescription(Locale locale);

	/**
	 * @param value
	 *            Value to evaluate if it's included on this Level.
	 * @return If this Value is included on this Level or not.
	 */
	public Boolean isValueIncluded(V value);

	/**
	 * @param locale
	 * 
	 */
	String getScaleName(Locale locale);

}