package com.xms.framework.architecture.domain.model.application;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Collaboration;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
@javax.jdo.annotations.Queries({ @javax.jdo.annotations.Query(name = "entity_by_id", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.application.ApplicationCollaboration e WHERE e.id == :id") })
public class ApplicationCollaboration extends ApplicationComponent implements
		Collaboration {

	// {{ Aggregates
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAggregates() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.AGGREGATION);
	}

	/**
	 * @param applicationComponent
	 */
	protected void addToAggregates(ApplicationComponent applicationComponent) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, applicationComponent,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (applicationComponent == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this, applicationComponent,
				RelationshipType.AGGREGATION);

	}

	/**
	 * @param applicationComponent
	 */
	protected void removeFromAggregates(
			ApplicationComponent applicationComponent) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this, applicationComponent,
						RelationshipType.AGGREGATION);
		// Check for no-op
		if (applicationComponent == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "aggregates", sequence = "10")
	public void aggregatesApplicationComponent(
			ApplicationComponent applicationComponent) {
		addToAggregates(applicationComponent);

	}

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "aggregates", sequence = "20")
	public void notAggregatesApplicationComponent(
			ApplicationComponent applicationComponent) {
		removeFromAggregates(applicationComponent);

	}

	// }}

	// {{ AssignedTo

	/**
	 * @param applicationInteraction
	 */
	@MemberOrder(name = "assignedTo", sequence = "100")
	public void assignedToApplicationInteraction(
			ApplicationInteraction applicationInteraction) {
		addAssignedTo(applicationInteraction);
	}

	/**
	 * @param applicationInteraction
	 */
	@MemberOrder(name = "assignedTo", sequence = "110")
	public void notAssignedToApplicationInteraction(
			ApplicationInteraction applicationInteraction) {
		removeFromAssignedTo(applicationInteraction);

	}

	// }}

}
