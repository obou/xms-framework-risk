package com.xms.framework.performance.domain.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

//@Entity
@PersistenceCapable
public class Objective extends AbstractMultiTenantEntity {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Objetivo Padre") })
	// @ManyToOne
	private Objective parentObjective;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Objetivos Hijo") })
	@OneToMany(mappedBy = "parentObjective")
	@Persistent(mappedBy = "parentObjective")
	@Element(dependent = "true")
	// Equivalent to JPA "orphanRemove". Deletes child objects.
	private Set<Objective> childObjectives = new LinkedHashSet<Objective>();

	// }}

	//
	// // @XMSField(locales = { @XMSLocale(locale = "es", caption =
	// // "KPIs - Indicadores Clave de Rendimiento") })
	// // @OneToMany(mappedBy = "objective")
	// // private Set<ObjectiveIndicator> keyPerformanceIndicators;
	//
	public Objective getParentObjective() {
		return parentObjective;
	}

	public void setParentObjective(Objective parentObjective) {
		this.parentObjective = parentObjective;
	}

	public Set<Objective> getChildObjectives() {
		return childObjectives;
	}

	public void setChildObjectives(Set<Objective> childObjectives) {
		this.childObjectives = childObjectives;
	}

	//
	// public Set<ObjectiveIndicator> getKeyPerformanceIndicators() {
	// return keyPerformanceIndicators;
	// }
	//
	// public void setKeyPerformanceIndicators(
	// Set<ObjectiveIndicator> keyPerformanceIndicators) {
	// this.keyPerformanceIndicators = keyPerformanceIndicators;
	// }

}
