package com.xms.framework.architecture.domain.model;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */

@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class Entity extends CoreElement {

	// TODO: relationshipRepository is not properly injected
	// (java.lang.NullPointerException at
	// com.xms.framework.architecture.domain.model.Entity.getRelationshipsAsSource(Entity.java:35)

	// // {{ RelationshipsAsSource (Collection)
	// @MemberOrder(sequence = "1")
	// public List<Relationship> getRelationshipsAsSource() {
	// return relationshipRepository.entityRelationshipsAsSource(this);
	// }
	//
	// // }}
	//
	// // {{ RelationshipsAsTarget (Collection)
	// @MemberOrder(sequence = "1")
	// public List<Relationship> getRelationshipsAsTarget() {
	// return relationshipRepository.entityRelationshipsAsTarget(this);
	// }
	//
	// // }}
	//
	// // {{ injected: RelationshipRepository
	// private RelationshipRepository relationshipRepository;
	//
	// public void setRelationshipRepository(
	// final RelationshipRepository relationshipRepository) {
	// this.relationshipRepository = relationshipRepository;
	// }
	// // }}

}
