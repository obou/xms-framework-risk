package com.xms.framework.frameworks.apache.isis.test;

import static org.junit.Assert.assertTrue;

import com.xms.framework.api.exception.XMSRuntimeException;
import com.xms.framework.domain.api.domain.DomainObject;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public abstract class AbstractDomainTest extends AbstractApacheIsisTest {

	/**
	 * @param communicationPathAssociatedWithNode
	 */
	public void validateCommonAssertions(DomainObject domainObject) {

		// The Business Id. must be assigned.
		assertTrue(!domainObject.getId().isEmpty());

		// Test if the Domain Object has been persisted.
		assertTrue(this.getDomainObjectContainer().isPersistent(domainObject));

		// Validation must be invoked over the original Domain Object, not the
		// wrapped object (it would fail for setters of Services injected to the
		// Domain Object).

		String validationResult = this.getDomainObjectContainer().validate(
				domainObject);
		if (validationResult != null) {
			throw new XMSRuntimeException(validationResult);
		}

		// Node must be wrapped for the Apache Isis validators to be launched.
		domainObject = wrapped(domainObject);

	}
}
