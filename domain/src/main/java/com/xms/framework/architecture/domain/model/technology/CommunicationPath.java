package com.xms.framework.architecture.domain.model.technology;

import java.util.SortedSet;
import java.util.TreeSet;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.filter.Filter;

import com.google.common.base.Objects;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
public class CommunicationPath extends AbstractMultiTenantEntity {

	private NodeFactory nodeFactory;

	// {{ AssociatedWithNodes (Collection)
	@Element(mappedBy = "communicationPath")
	private SortedSet<NodeCommunicationPath> associatedWithNodes = new TreeSet<NodeCommunicationPath>();

	@org.apache.isis.applib.annotation.Paged(value = 10)
	@MemberOrder(sequence = "1")
	public SortedSet<NodeCommunicationPath> getAssociatedWithNodes() {
		return associatedWithNodes;
	}

	public void setAssociatedWithNodes(
			final SortedSet<NodeCommunicationPath> associatedWithNodes) {
		this.associatedWithNodes = associatedWithNodes;
	}

	// }}

	public static Filter<CommunicationPath> filterThoseWithName(
			final String name) {
		return new Filter<CommunicationPath>() {
			@Override
			public boolean accept(final CommunicationPath communicationPath) {
				return Objects.equal(communicationPath.getName(), name);
			}

		};
	}

	public static Filter<CommunicationPath> filterThoseWithId(final String id) {
		return new Filter<CommunicationPath>() {
			@Override
			public boolean accept(final CommunicationPath communicationPath) {
				return Objects.equal(communicationPath.getId(), id);
			}

		};
	}

	// {{ associatedWithNode (action)
	@MemberOrder(sequence = "1")
	public NodeCommunicationPath associateWithNode(
			@Named("Node") Node node) {
		return nodeFactory.createNodeAssociationWithCommunicationPath(node,
				this);
	}

	// }}

	// {{ associatedWithNode (action)
	@MemberOrder(sequence = "1")
	public NodeCommunicationPathAddress associateWithNodeOnAddress(
			final @Named("Node") Node node,
			final @Named("Address") String address,
			@Named("Mask") final String mask) {
		return nodeFactory.createNodeAssociationWithCommunicationPathOnAddress(
				node, this, address, mask);
	}

	// }}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

}
