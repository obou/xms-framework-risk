package com.xms.framework.enterprisearchitecture.application.domain;

import org.apache.isis.progmodel.wrapper.applib.WrapperFactory;
import org.apache.isis.viewer.junit.Service;
import org.apache.isis.viewer.junit.Services;
import org.junit.Before;
import org.junit.Test;

import com.xms.framework.enterprisearchitecture.domain.model.application.ApplicationComponentFactory;
import com.xms.framework.enterprisearchitecture.domain.model.application.ApplicationFunctionFactory;
import com.xms.framework.frameworks.apache.isis.test.AbstractApacheIsisTest;
import com.xms.framework.party.domain.model.PersonFactory;
import com.xms.framework.performance.domain.model.Objective;
import com.xms.framework.performance.domain.model.ObjectiveFactory;

import dom.todo.ToDoItems;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Services({ @Service(ApplicationComponentFactory.class),
		@Service(ApplicationFunctionFactory.class), @Service(ToDoItems.class),
		@Service(PersonFactory.class), @Service(ObjectiveFactory.class) })
public class ApplicationLayerTests extends AbstractApacheIsisTest {

	// //////////////////////////////////////////////////////
	// Injected.
	// //////////////////////////////////////////////////////

	/**
	 * The {@link WrapperFactory#wrap(Object) wrapped} equivalent of the
	 * {@link #setApplicationComponents(ApplicationComponentFactory) injected}
	 * {@link ApplicationComponentFactory}.
	 */
	// IMPORTANT: They must have setters and getters.
	protected ApplicationComponentFactory applicationComponents;
	protected ApplicationFunctionFactory applicationFunctions;
	protected PersonFactory persons;
	protected ObjectiveFactory objectives;
	protected ToDoItems toDoItems;

	// Employee tomEmployee;

	@Before
	public void wrapInjectedServices() throws Exception {
		applicationComponents = wrapped(applicationComponents);
		applicationFunctions = wrapped(applicationFunctions);
		persons = wrapped(persons);
		toDoItems = wrapped(toDoItems);
		objectives = wrapped(objectives);
	}

	@Before
	public void setUp() {
		// tomEmployee =
		// wrapped(employeeRepository.findEmployees("Tom").get(0));
	}

	@Test
	public void testApplicationComponent() {

		// // SI TIENE WICKET VIEWER Y VA QUIZ� LO MEJOR SEA COPIAR LAS CLASES Y
		// // DECLARAR LA DEPENDENCIA CON XMS EN EL CLAIMS.

		// ToDoItem toDoItem = wrapped(toDoItems.newToDo("hi",
		// ToDoItem.Category.Domestic, org.joda.time.LocalDate.now()));
		// toDoItem.completed();

		// ApplicationComponents applicationComponents = new
		// ApplicationComponents();

		// AbstractAsset abstractAsset = wrapped(new AbstractAsset());

		// AbstractApplicationLayerElementActiveStructure structure = new
		// AbstractApplicationLayerElementActiveStructure();

		@SuppressWarnings("unused")
		Objective objetive = wrapped(objectives.create("objective",
				"description"));

		// Person person = wrapped(persons.create("id", "Oscar", "desc"));

		// ApplicationComponent applicationComponent =
		// wrapped(applicationComponents
		// .create("Application Component", "asas"));

		// applicationComponent
		// .assignNewApplicationFunction("Application Function Name");

		// ApplicationFunctions applicationFunctions = new
		// ApplicationFunctions();
		// ApplicationFunction applicationFunction = applicationFunctions
		// .newPersistentInstance(ApplicationFunction.class);
		// applicationComponent.assignExistingApplicationFunction();
		//
		// applicationComponent
		// .assignExistingApplicationFunction(applicationFunction);

	}

	public ApplicationComponentFactory getApplicationComponents() {
		return applicationComponents;
	}

	public void setApplicationComponents(
			ApplicationComponentFactory applicationComponents) {
		this.applicationComponents = applicationComponents;
	}

	public ApplicationFunctionFactory getApplicationFunctions() {
		return applicationFunctions;
	}

	public void setApplicationFunctions(
			ApplicationFunctionFactory applicationFunctions) {
		this.applicationFunctions = applicationFunctions;
	}

	public ToDoItems getToDoItems() {
		return toDoItems;
	}

	public void setToDoItems(ToDoItems toDoItems) {
		this.toDoItems = toDoItems;
	}

	public PersonFactory getPersons() {
		return persons;
	}

	public void setPersons(PersonFactory persons) {
		this.persons = persons;
	}

	public ObjectiveFactory getObjectives() {
		return objectives;
	}

	public void setObjectives(ObjectiveFactory objectives) {
		this.objectives = objectives;
	}

}
