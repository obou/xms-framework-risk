package com.xms.framework.architecture.domain.model.business;

import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class BusinessObjectFactory extends
		AbstractMultiTenantEntityRepositoryAndFactory {

	/**
	 * @param string
	 * @param object
	 * @return
	 */
	public BusinessObject createBusinessObject(@Named("Name") String name,
			@Named("Description") @Optional String description) {

		return newPersistentMultiTenantEntity(BusinessObject.class, null, name,
				description);

	}

}
