package com.xms.framework.common.domain.model;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import com.xms.framework.domain.api.domain.MultiTenantEntityRelationship;

@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
// //@MappedSuperclass
public abstract class AbstractMultiTenantEntityRelationship extends
		AbstractMultiTenantObject implements MultiTenantEntityRelationship {

	// TODO: public abstract String title();

}
