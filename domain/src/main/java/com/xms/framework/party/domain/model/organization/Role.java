package com.xms.framework.party.domain.model.organization;

import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;
import com.xms.framework.party.domain.model.Party;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Rol"),
		@XMSEntityName(locale = "en", name = "Role") })
//@Entity
@PersistenceCapable
public class Role extends AbstractMultiTenantEntity {

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Funciones a realizar"),
			@XMSLocale(locale = "en", caption = "Functions to Perform") })
	private Set<RoleFunction> functionsToPerform;

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Competencias requeridas (Conocimiento y Habilidades)"),
			@XMSLocale(locale = "en", caption = "Competencies required (Knowledge and Skills)") })
	private Set<RoleCompetency> competenciesRequired;

	private String userName;

	//@Transient
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Personas/Organizaciones con este Rol") })
	private Set<Party> partiesWithThisRole;

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Puestos de Trabajo que lo requieren") })
	private Set<JobRole> jobsRequiringThisRole;

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Puestos de Trabajo que lo requieren") })
	private Set<PositionRole> positionsExtendedWithThisRole;

	protected Set<RoleFunction> getFunctionsToPerform() {
		return functionsToPerform;
	}

	protected void setFunctionsToPerform(Set<RoleFunction> functionsToPerform) {
		this.functionsToPerform = functionsToPerform;
	}

	protected Set<RoleCompetency> getCompetenciesRequired() {
		return competenciesRequired;
	}

	protected void setCompetenciesRequired(
			Set<RoleCompetency> competenciesRequired) {
		this.competenciesRequired = competenciesRequired;
	}

	protected Set<Party> getPartiesWithThisRole() {
		Set<Party> set = new HashSet<Party>();

		// Parties with a Position whose Job has this Role assigned.
		for (JobRole current : this.getJobsRequiringThisRole()) {
			for (Position currentPosition : current.getJob().getPositions()) {
				set.add(currentPosition.getHoldedByPerson());
			}
		}

		// Positions that have been extended with this Role.
		for (PositionRole current : this.getPositionsExtendedWithThisRole()) {
			set.add(current.getPosition().getHoldedByPerson());
		}

		return partiesWithThisRole;
	}

	protected Set<JobRole> getJobsRequiringThisRole() {
		return jobsRequiringThisRole;
	}

	protected void setJobsRequiringThisRole(Set<JobRole> jobsRequiringThisRole) {
		this.jobsRequiringThisRole = jobsRequiringThisRole;
	}

	public Set<PositionRole> getPositionsExtendedWithThisRole() {
		return positionsExtendedWithThisRole;
	}

	public void setPositionsExtendedWithThisRole(
			Set<PositionRole> positionsExtendedWithThisRole) {
		this.positionsExtendedWithThisRole = positionsExtendedWithThisRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
