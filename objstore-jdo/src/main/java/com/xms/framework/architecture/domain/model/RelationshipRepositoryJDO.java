package com.xms.framework.architecture.domain.model;

import java.util.List;

import org.apache.isis.applib.query.QueryDefault;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class RelationshipRepositoryJDO extends RelationshipRepositoryIsis {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsSource
	 * (com.xms.framework.architecture.domain.model.Entity)
	 */
	@Override
	public List<Relationship> relationshipsAsSource(final Entity entity) {

		return allMatches(new QueryDefault<Relationship>(Relationship.class,
				"relationships_as_source", "sourceEntityId", entity.getId()));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsSource
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public List<Relationship> relationshipsAsSourceOfType(final Entity entity,
			final RelationshipType relationshipType) {
		return allMatches(new QueryDefault<Relationship>(Relationship.class,
				"relationships_as_source_of_type", "sourceEntityId",
				entity.getId(), "type", relationshipType));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsTarget
	 * (com.xms.framework.architecture.domain.model.Entity)
	 */
	@Override
	public List<Relationship> relationshipsAsTarget(final Entity entity) {

		return allMatches(new QueryDefault<Relationship>(Relationship.class,
				"relationships_as_target", "targetEntityId", entity.getId()));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * entityRelationshipsAsTarget
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public List<Relationship> relationshipsAsTargetOfType(final Entity entity,
			final RelationshipType relationshipType) {

		return allMatches(new QueryDefault<Relationship>(Relationship.class,
				"relationships_as_target_of_type", "targetEntityId",
				entity.getId(), "type", relationshipType));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * relationshipBySourceTargetAndType
	 * (com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.Entity,
	 * com.xms.framework.architecture.domain.model.RelationshipType)
	 */
	@Override
	public Relationship relationshipBySourceTargetAndType(
			final Entity sourceEntity, final Entity targetEntity,
			final RelationshipType relationshipType) {
		return firstMatch(new QueryDefault<Relationship>(Relationship.class,
				"relationships_of_source_and_target_and_type",
				"sourceEntityId", sourceEntity.getId(), "targetEntityId",
				targetEntity.getId(), "type", relationshipType));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.architecture.domain.model.RelationshipRepository#
	 * relationshipEntity(java.lang.Class, java.lang.String)
	 */
	@Override
	public <T> T relationshipEntity(Class<T> clazz, final String id) {
		return firstMatch(new QueryDefault<T>(clazz, "entity_by_id", "id", id));
	}
}
