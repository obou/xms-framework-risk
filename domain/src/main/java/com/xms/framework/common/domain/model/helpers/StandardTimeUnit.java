package com.xms.framework.common.domain.model.helpers;


/**
 * XMS Taxonomy for common Units of Measurement of Time.
 * 
 * It represents time durations at a given unit of granularity and provides
 * utility methods to convert across sourceUnit, and to perform timing and delay
 * operations in these sourceUnit.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 */
public enum StandardTimeUnit {
	MILLISECONDS {
		@Override
		public double getDailyOccurrences() {
			return 24 * 60 * 60 * 1000;
		}
	},
	SECONDS {
		@Override
		public double getDailyOccurrences() {
			return 24 * 60 * 60;
		}
	},
	MINUTES {

		@Override
		public double getDailyOccurrences() {
			return 24 * 60;
		}
	},
	HOURS {

		@Override
		public double getDailyOccurrences() {
			return 24;
		}
	},
	DAYS {
		@Override
		public double convert(double duration, StandardTimeUnit sourceUnit) {
			return sourceUnit.getDailyOccurrences() * duration;
		}

		@Override
		public double getDailyOccurrences() {
			return 1;
		}
	},
	WEEKS {
		@Override
		public double getDailyOccurrences() {
			return 1 / 7.0;
		}
	},
	MONTHS_30_DAYS {
		@Override
		public double getDailyOccurrences() {
			return 1 / 30.0;
		}
	},
	QUARTERS_90_DAYS {

		@Override
		public double getDailyOccurrences() {
			return 1 / 90.0;
		}
	},
	YEARS_365_DAYS {

		@Override
		public double getDailyOccurrences() {
			return 1 / 365.0;
		}
	};

	public double convert(double duration, StandardTimeUnit sourceUnit) {
		return StandardTimeUnit.DAYS.convert(duration, sourceUnit)
				/ this.getDailyOccurrences();
	}

	public abstract double getDailyOccurrences();

}
