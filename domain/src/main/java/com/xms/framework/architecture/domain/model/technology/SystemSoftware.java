package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class SystemSoftware extends AbstractNode {

	// {{ Device (property)
	private Device assignedToDevice;

	@MemberOrder(sequence = "1")
	public Device getAssignedToDevice() {
		return assignedToDevice;
	}

	public void setAssignedToDevice(final Device device) {
		this.assignedToDevice = device;
	}

	public void modifyAssignedToDevice(final Device device) {
		Device currentDevice = getAssignedToDevice();
		// check for no-op
		if (device == null || device.equals(currentDevice)) {
			return;
		}
		// delegate to parent to associate

		// XMS: Nodes are not allowed to directly being assigned to System
		// Software. A Device must be created. For Hardware Virtualization and
		// other scenarios is needed that a System Software is assigned to
		// System Software.
		device.addToAssignedToSystemSoftware(this);

		// additional business logic
		onModifyAssignedToDevice(currentDevice, device);
	}

	/**
	 * @param currentAbstractNode
	 * @param abstractNode
	 */
	private void onModifyAssignedToDevice(Device currentDevice, Device device) {
		// Do nothing.

	}

	public void clearAssignedToDevice() {
		Device device = getAssignedToDevice();
		// check for no-op
		if (device == null) {
			return;
		}
		// delegate to parent to dissociate
		// XMS: Nodes are not allowed to directly being assigned to System
		// Software. A Device must be created. For Hardware Virtualization and
		// other scenarios is needed that a System Software is assigned to
		// System Software.
		device.removeFromAssignedToSystemSoftware(this);

		// additional business logic
		onClearAssignedToDevice(device);
	}

	/**
	 * @param currentAbstractNode
	 */
	private void onClearAssignedToDevice(AbstractNode currentAbstractNode) {
		// Do nothing.

	}

	// }}

}
