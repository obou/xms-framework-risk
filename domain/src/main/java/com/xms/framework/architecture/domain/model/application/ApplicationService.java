package com.xms.framework.architecture.domain.model.application;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.Service;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class ApplicationService extends Service {

	// {{ AssignedFrom

	/**
	 * @param applicationInterface
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void assignedFromApplicationInterface(
			ApplicationInterface applicationInterface) {
		super.addToAssignedFrom(applicationInterface);
	}

	/**
	 * @param applicationInterface
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void notAssignedFromApplicationInterface(
			ApplicationInterface applicationInterface) {
		super.removeFromAssignedFrom(applicationInterface);
	}

	// }}

	// {{ UsedBy

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "usedBy", sequence = "10")
	public void usedByApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.addToUsedByActiveStructureElement(applicationComponent);
	}

	/**
	 * @param applicationComponent
	 */
	@MemberOrder(name = "usedBy", sequence = "20")
	public void notUsedByApplicationComponent(
			ApplicationComponent applicationComponent) {
		super.removeFromUsedByActiveStructureElement(applicationComponent);
	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "usedBy", sequence = "30")
	public void usedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addToUsedByBehaviorElement(applicationFunction);

	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "usedBy", sequence = "40")
	public void notUsedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromUsedByBehaviorElement(applicationFunction);

	}

	// }}

	// {{ RealizedBy
	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "realizedBy", sequence = "10")
	public void realizedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addToRealizedBy(applicationFunction);
	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "realizedBy", sequence = "20")
	public void notRealizedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromRealizedBy(applicationFunction);
	}

	// }}

	// {{ Accesses

	/**
	 * @param dataObject
	 */
	@MemberOrder(name = "accesses", sequence = "10")
	public void accessesDataObject(DataObject dataObject) {
		super.addToAccesses(dataObject);
	}

	/**
	 * @param dataObject
	 */
	@MemberOrder(name = "accesses", sequence = "20")
	public void notAccessesDataObject(DataObject dataObject) {
		super.removeFromAccesses(dataObject);
	}

	// }}

}
