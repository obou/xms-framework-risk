package com.xms.framework.architecture.domain.model.technology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.ActionSemantics;
import org.apache.isis.applib.annotation.ActionSemantics.Of;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.architecture.domain.model.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class AbstractNode extends Entity {

	// {{ AssignedToInfrastructureServices (Collection)
	private Set<InfrastructureService> assignedToInfrastructureServices = new LinkedHashSet<InfrastructureService>();

	@MemberOrder(sequence = "20")
	public Set<InfrastructureService> getAssignedToInfrastructureServices() {
		return assignedToInfrastructureServices;
	}

	protected void setAssignedToInfrastructureServices(
			final Set<InfrastructureService> assignedToInfrastructureServices) {
		this.assignedToInfrastructureServices = assignedToInfrastructureServices;
	}

	public void addToAssignedToInfrastructureServices(
			final InfrastructureService infrastructureService) {
		// check for no-op
		if (infrastructureService == null
				|| getAssignedToInfrastructureServices().contains(
						infrastructureService)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureService.clearAssignedToNode();
		// associate arg
		infrastructureService.setAssignedToNode(this);
		getAssignedToInfrastructureServices().add(infrastructureService);
		// additional business logic
		onAddToAssignedToInfrastructureServices(infrastructureService);
	}

	/**
	 * @param infrastructureService
	 */
	private void onAddToAssignedToInfrastructureServices(
			InfrastructureService infrastructureService) {
		// Do nothing.

	}

	public void removeFromAssignedToInfrastructureServices(
			final InfrastructureService infrastructureService) {
		// check for no-op
		if (infrastructureService == null
				|| !getAssignedToInfrastructureServices().contains(
						infrastructureService)) {
			return;
		}
		// dissociate arg
		infrastructureService.setAssignedToNode(null);
		getAssignedToInfrastructureServices().remove(infrastructureService);
		// additional business logic
		onRemoveFromAssignedToInfrastructureServices(infrastructureService);
	}

	/**
	 * @param infrastructureService
	 */
	private void onRemoveFromAssignedToInfrastructureServices(
			InfrastructureService infrastructureService) {
		// Do nothing.

	}

	/**
	 * @param string
	 * @param object
	 * @return
	 */
	@ActionSemantics(Of.NON_IDEMPOTENT)
	@MemberOrder(name = "assignedToInfrastructureServices", sequence = "2")
	public InfrastructureService assignToNewInfrastructureService(
			@Named("Name") String name,
			@Named("Description") @Optional String description,

			@Named("Interface") @Optional String interfaceName) {
		InfrastructureService infrastructureService = nodeFactory
				.createInfrastructureService(this, name, description);

		if (interfaceName != null && !interfaceName.isEmpty())
			infrastructureService.assignToNewInfrastructureInterface(
					interfaceName, null);

		return infrastructureService;

	}

	@ActionSemantics(Of.NON_IDEMPOTENT)
	@MemberOrder(name = "assignedToInfrastructureServices", sequence = "2")
	public InfrastructureService assignToNewInfrastructureServiceOnIp(
			@Named("Name") String name,
			@Named("Description") @Optional String description,
			@Named("Interface") @Optional String interfaceName,
			@Named("Node Address") @Optional NodeCommunicationPathAddress nodeAddress,
			@Named("IP Protocol") @Optional IpProtocol ipProtocol,
			@Named("IP Port") @Optional Integer ipPort) {
		InfrastructureService infrastructureService = nodeFactory
				.createInfrastructureService(this, name, description);

		infrastructureService.assignToNewInfrastructureInterfaceOnIp(
				nodeAddress, ipProtocol, ipPort);

		return infrastructureService;

	}

	@ActionSemantics(Of.NON_IDEMPOTENT)
	@MemberOrder(name = "assignedToInfrastructureServices", sequence = "2")
	public void assignToExistingInfrastructureService(
			@Named("Infrastructure Service") InfrastructureService infrastructureService) {
		infrastructureService.modifyAssignedToNode(this);
	}

	@MemberOrder(name = "assignedToInfrastructureServices", sequence = "2")
	public InfrastructureService assignToNewInfrastructureServiceOnIp(
			String name, String description,
			NodeCommunicationPathAddress nodeAddress, IpProtocol ipProtocol,
			Integer ipPort) {

		InfrastructureService infrastructureService = this
				.assignToNewInfrastructureService(name, description, null);

		InfrastructureInterface infrastructureInterface = nodeFactory
				.createIpInfrastructureInterface(nodeAddress, ipProtocol,
						ipPort);

		infrastructureInterface
				.modifyAssignedToInfrastructureService(infrastructureService);

		return infrastructureService;

	}

	// }}

	// {{ AssignedToArtifacts (Collection)
	private Set<Artifact> assignedToArtifacts = new LinkedHashSet<Artifact>();

	@MemberOrder(sequence = "30")
	public Set<Artifact> getAssignedToArtifacts() {
		return assignedToArtifacts;
	}

	public void setAssignedToArtifacts(final Set<Artifact> assignedToArtifacts) {
		this.assignedToArtifacts = assignedToArtifacts;
	}

	public void addToAssignedToArtifacts(final Artifact artifact) {
		// check for no-op
		if (artifact == null || getAssignedToArtifacts().contains(artifact)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		artifact.clearAssignedToNode();
		// associate arg
		artifact.setAssignedToNode(this);
		getAssignedToArtifacts().add(artifact);
		// additional business logic
		onAddToAssignedToArtifacts(artifact);
	}

	/**
	 * @param artifact
	 */
	private void onAddToAssignedToArtifacts(Artifact artifact) {
		// Do nothing.

	}

	public void removeFromAssignedToArtifacts(final Artifact artifact) {
		// check for no-op
		if (artifact == null || !getAssignedToArtifacts().contains(artifact)) {
			return;
		}
		// dissociate arg
		artifact.setAssignedToNode(null);
		getAssignedToArtifacts().remove(artifact);
		// additional business logic
		onRemoveFromAssignedToArtifacts(artifact);
	}

	/**
	 * @param artifact
	 */
	private void onRemoveFromAssignedToArtifacts(Artifact artifact) {
		// Do nothing.

	}

	/**
	 * @param string
	 * @param object
	 * @return
	 */
	@MemberOrder(name = "assignedToArtifacts", sequence = "1")
	public Artifact assignToNewArtifact(@Named("Name") String name,
			@Named("Description") @Optional String description) {
		return getNodeFactory().createArtifact(this, name, description);
	}

	/**
	 * @param string
	 * @param object
	 * @return
	 */
	@MemberOrder(name = "assignedToArtifacts", sequence = "2")
	public void assignToExistingArtifact(@Named("Artifact") Artifact artifact) {
		artifact.modifyAssignedToNode(this);
	}

	// }}

	// {{ ComposedOfInfrastructureInterfaces (Collection)
	private Set<InfrastructureInterface> composedOfInfrastructureInterfaces = new LinkedHashSet<InfrastructureInterface>();

	@MemberOrder(sequence = "10")
	public Set<InfrastructureInterface> getComposedOfInfrastructureInterfaces() {
		return composedOfInfrastructureInterfaces;
	}

	public void setComposedOfInfrastructureInterfaces(
			final Set<InfrastructureInterface> composedOfInfrastructureInterfaces) {
		this.composedOfInfrastructureInterfaces = composedOfInfrastructureInterfaces;
	}

	public void addToComposedOfInfrastructureInterfaces(
			final InfrastructureInterface infrastructureInterface) {
		// check for no-op
		if (infrastructureInterface == null
				|| getComposedOfInfrastructureInterfaces().contains(
						infrastructureInterface)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureInterface.clearPartOfNode();
		// associate arg
		infrastructureInterface.setPartOfNode(this);
		getComposedOfInfrastructureInterfaces().add(infrastructureInterface);
		// additional business logic
		onAddToComposedOfInfrastructureInterfaces(infrastructureInterface);
	}

	/**
	 * @param infrastructureInterface
	 */
	private void onAddToComposedOfInfrastructureInterfaces(
			InfrastructureInterface infrastructureInterface) {
		// Do nothing.

	}

	public void removeFromComposedOfInfrastructureInterfaces(
			final InfrastructureInterface infrastructureInterface) {
		// check for no-op
		if (infrastructureInterface == null
				|| !getComposedOfInfrastructureInterfaces().contains(
						infrastructureInterface)) {
			return;
		}
		// dissociate arg
		infrastructureInterface.setPartOfNode(null);
		getComposedOfInfrastructureInterfaces().remove(infrastructureInterface);
		// additional business logic
		onRemoveFromComposedOfInfrastructureInterfaces(infrastructureInterface);
	}

	/**
	 * @param infrastructureInterface
	 */
	private void onRemoveFromComposedOfInfrastructureInterfaces(
			InfrastructureInterface infrastructureInterface) {
		// Do nothing.

	}

	@MemberOrder(name = "composedOfInfrastructureInterfaces", sequence = "1")
	public InfrastructureInterface composeOfNewInfrastructureInterface(
			@Named("Name") String name,
			@Named("Description") @Optional String description) {
		return nodeFactory.createInfrastructureInterface(this, name,
				description);
	}

	/**
	 * @param nodeAddress
	 * @param tcp
	 * @param i
	 * @return
	 */
	@MemberOrder(name = "composedOfInfrastructureInterfaces", sequence = "2")
	public InfrastructureInterface composeOfNewIpInfrastructureInterface(
			@Named("Node Address") NodeCommunicationPathAddress nodeAddress,
			@Named("Ip Protocol") IpProtocol ipProtocol,
			@Named("Ip Port") int port) {

		return nodeFactory.createIpInfrastructureInterface(nodeAddress,
				ipProtocol, port);

	}

	@MemberOrder(name = "composedOfInfrastructureInterfaces", sequence = "3")
	public void composeOfExistingInfrastructureInterface(
			@Named("Infrastructure Interface") InfrastructureInterface infrastructureInterface) {
		infrastructureInterface.modifyPartOfNode(this);
	}

	// }}

	// {{ Services

	private NodeFactory nodeFactory;

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	// Needed as it's inherited.
	@Hidden
	public NodeFactory getNodeFactory() {
		return nodeFactory;
	}

	// }}

}
