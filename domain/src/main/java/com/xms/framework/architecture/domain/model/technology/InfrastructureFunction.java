package com.xms.framework.architecture.domain.model.technology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.architecture.domain.model.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class InfrastructureFunction extends Entity {

	// {{ RealisesInfrastructureService (property)
	private InfrastructureService realisesInfrastructureService;

	@MemberOrder(sequence = "1")
	public InfrastructureService getRealisesInfrastructureService() {
		return realisesInfrastructureService;
	}

	protected void setRealisesInfrastructureService(
			final InfrastructureService realisesInfrastructureService) {
		this.realisesInfrastructureService = realisesInfrastructureService;
	}

	public void modifyRealisesInfrastructureService(
			final InfrastructureService infrastructureService) {
		InfrastructureService currentRealisesInfrastructureService = getRealisesInfrastructureService();
		// check for no-op
		if (infrastructureService == null
				|| infrastructureService
						.equals(currentRealisesInfrastructureService)) {
			return;
		}
		// delegate to parent to associate
		infrastructureService.addToRealisedByInfrastructureFunctions(this);
		// additional business logic
		onModifyRealisesInfrastructureService(
				currentRealisesInfrastructureService, infrastructureService);
	}

	public void clearRealisesInfrastructureService() {
		InfrastructureService currentRealisesInfrastructureService = getRealisesInfrastructureService();
		// check for no-op
		if (currentRealisesInfrastructureService == null) {
			return;
		}
		// delegate to parent to dissociate
		currentRealisesInfrastructureService
				.removeFromRealisedByInfrastructureFunctions(this);
		// additional business logic
		onClearRealisesInfrastructureService(currentRealisesInfrastructureService);
	}

	protected void onModifyRealisesInfrastructureService(
			final InfrastructureService oldRealisesInfrastructureService,
			final InfrastructureService newRealisesInfrastructureService) {
	}

	protected void onClearRealisesInfrastructureService(
			final InfrastructureService oldRealisesInfrastructureService) {
	}

	// }}

	// {{ AccessesArtifacts (Collection)
	private Set<Artifact> accessesArtifacts = new LinkedHashSet<Artifact>();

	@MemberOrder(sequence = "1")
	public Set<Artifact> getAccessesArtifacts() {
		return accessesArtifacts;
	}

	public void setAccessesArtifacts(final Set<Artifact> accessesArtifacts) {
		this.accessesArtifacts = accessesArtifacts;
	}

	public void addToAccessesArtifacts(final Artifact artifact) {
		// check for no-op
		if (artifact == null || getAccessesArtifacts().contains(artifact)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		artifact.removeFromAccessedByInfrastructureFunctions(this);
		// associate arg
		artifact.getAccessedByInfrastructureFunctions().add(this);
		getAccessesArtifacts().add(artifact);
		// additional business logic
		onAddToAccessesArtifacts(artifact);
	}

	/**
	 * @param artifact
	 */
	private void onAddToAccessesArtifacts(Artifact artifact) {
		// Do nothing.

	}

	public void removeFromAccessesArtifacts(final Artifact artifact) {
		// check for no-op
		if (artifact == null || !getAccessesArtifacts().contains(artifact)) {
			return;
		}
		// dissociate arg
		artifact.getAccessedByInfrastructureFunctions().remove(this);
		getAccessesArtifacts().remove(artifact);
		// additional business logic
		onRemoveFromAccessesArtifacts(artifact);
	}

	/**
	 * @param artifact
	 */
	private void onRemoveFromAccessesArtifacts(Artifact artifact) {
		// Do nothing.

	}

	/**
	 * @param string
	 * @param object
	 */
	public void accessesNewArtifact(@Named("Name") String name,
			@Named("Description") @Optional String description) {

		nodeFactory.createArtifact(name, description);

	}

	// }}

	// {{ Services

	private NodeFactory nodeFactory;

	protected void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}
	// }}

}
