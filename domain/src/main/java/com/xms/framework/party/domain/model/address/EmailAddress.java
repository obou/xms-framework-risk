package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;

@XMSEntityNames({
		@XMSEntityName(locale = "es", name = "Dirección de Correo Electrónico"),
		@XMSEntityName(locale = "en", name = "Email Address") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class EmailAddress extends Address {

}
