package com.xms.framework.architecture.domain.model.business;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.Service;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessService extends Service {

	// {{ AssignedFrom

	/**
	 * @param BusinessInterface
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void assignedFromBusinessInterface(
			BusinessInterface businessInterface) {
		super.addToAssignedFrom(businessInterface);
	}

	/**
	 * @param BusinessInterface
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void notAssignedFromBusinessInterface(
			BusinessInterface businessInterface) {
		super.removeFromAssignedFrom(businessInterface);
	}

	// }}

	// {{ UsedBy

	/**
	 * @param BusinessRole
	 */
	@MemberOrder(name = "usedBy", sequence = "10")
	public void usedByBusinessRole(BusinessRole businessRole) {
		super.addToUsedByActiveStructureElement(businessRole);
	}

	/**
	 * @param BusinessRole
	 */
	@MemberOrder(name = "usedBy", sequence = "20")
	public void notUsedByBusinessRole(BusinessRole businessRole) {
		super.removeFromUsedByActiveStructureElement(businessRole);
	}

	/**
	 * @param BusinessFunction
	 */
	@MemberOrder(name = "usedBy", sequence = "30")
	public void usedByBusinessFunction(BusinessFunction businessFunction) {
		super.addToUsedByBehaviorElement(businessFunction);

	}

	/**
	 * @param BusinessFunction
	 */
	@MemberOrder(name = "usedBy", sequence = "40")
	public void notUsedByBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromUsedByBehaviorElement(businessFunction);

	}

	/**
	 * @param businessActor
	 */
	public void usedByBusinessActor(BusinessActor businessActor) {
		super.addToUsedByActiveStructureElement(businessActor);

	}

	/**
	 * @param businessActor
	 */
	public void notUsedByBusinessActor(BusinessActor businessActor) {
		super.removeFromUsedByActiveStructureElement(businessActor);

	}

	// }}

	// {{ RealizedBy
	/**
	 * @param BusinessFunction
	 */
	@MemberOrder(name = "realizedBy", sequence = "10")
	public void realizedByBusinessFunction(BusinessFunction businessFunction) {
		super.addToRealizedBy(businessFunction);
	}

	/**
	 * @param BusinessFunction
	 */
	@MemberOrder(name = "realizedBy", sequence = "20")
	public void notRealizedByBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromRealizedBy(businessFunction);
	}

	// }}

	// {{ Accesses

	/**
	 * @param BusinessObject
	 */
	@MemberOrder(name = "accesses", sequence = "10")
	public void accessesBusinessObject(BusinessObject businessObject) {
		super.addToAccesses(businessObject);
	}

	/**
	 * @param BusinessObject
	 */
	@MemberOrder(name = "accesses", sequence = "20")
	public void notAccessesBusinessObject(BusinessObject businessObject) {
		super.removeFromAccesses(businessObject);
	}

	// }}

}
