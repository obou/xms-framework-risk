package com.xms.framework.common.domain.model.helpers;

import java.util.Set;

import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * Matrix for assessing the Likelihood of Occurrence from the Likelihood of
 * Initiation and the Likelihood of Success.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * @param <RL>
 * @param <CL>
 * 
 */
//@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "DTYPE", length = 64)
@PersistenceCapable
@javax.jdo.annotations.Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class GenericScaleMatrix extends AbstractMultiTenantEntity {

	/**
	 * Elements of this Matrix.
	 */
	//@Transient
	@XMSField(show = false)
	private Set<GenericScaleMatrixElement> elements;

	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScale rowScale;

	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScale columnScale;

	@SuppressWarnings("unused")
	//@Transient
	@XMSField(show = false)
	private GenericScale elementScale;

	/**
	 * @param rowLevel
	 *            Row Scale Level
	 * @param columnLevel
	 *            Column Scale Level
	 * @return The Matrix Element Level on that Row and Column
	 */
	public GenericScaleLevel getScaleLevelForValues(GenericScaleLevel rowLevel,
			GenericScaleLevel columnLevel) {
		GenericScaleLevel foundLevel = null;
		for (GenericScaleMatrixElement currentElement : elements) {
			// As GenericScaleLevel has overridden equals() and hashCode() two
			// instances can be safely compared.
			if ((currentElement.getRowLevel().equals(rowLevel))
					&& (currentElement.getColumnLevel().equals(columnLevel)))
				foundLevel = currentElement.getElementLevel();
		}

		return foundLevel;
	}

	public abstract Set<GenericScaleMatrixElement> getElements();

	public abstract GenericScale getRowScale();

	public abstract GenericScale getColumnScale();

	public abstract GenericScale getElementScale();

}
