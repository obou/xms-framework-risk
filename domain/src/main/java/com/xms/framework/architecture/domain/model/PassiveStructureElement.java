package com.xms.framework.architecture.domain.model;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class PassiveStructureElement extends Entity {

	// {{ AccessedBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAccessedBy() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.ACCESS);
	}

	protected void addToAccessedByBehaviorElement(
			final BehaviorElement behaviorElement) {

		// It's the inverse Relationship.
		behaviorElement.addToAccesses(this);

	}

	protected void removeFromAccessedByBehaviorElement(
			final BehaviorElement behaviorElement) {

		// Is the inverse relationship.
		behaviorElement.removeFromAccesses(this);

	}

	protected void addToAccessedByService(final Service service) {

		// It's the inverse Relationship.
		service.addToAccesses(this);

	}

	protected void removeFromAccessedByService(final Service service) {

		// Is the inverse relationship.
		service.removeFromAccesses(this);

	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	public RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	// Used on BusinessObject
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	public RelationshipFactory getRelationshipFactory() {
		return relationshipFactory;
	}

	// {{ injected: DomainObjectContainer
	// Used on BusinessObject
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}

	public DomainObjectContainer getDomainObjectContainer() {
		return domainObjectContainer;
	}

	// }}

	// }}

}
