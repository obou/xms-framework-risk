package com.xms.framework.architecture.domain.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.motivation.Requirement;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class CoreElement extends AbstractMultiTenantEntity {

	// {{ Realises (Collection)
	// TODO: It should be derived from the �Requirement.realisedBy" Collection.
	private Set<Requirement> realises = new LinkedHashSet<Requirement>();

	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "100")
	public Set<Requirement> getRealises() {
		return realises;
	}

	@MemberOrder(name = "realises", sequence = "10")
	public void addToRealises(final Requirement requirement) {
		// check for no-op
		if (requirement == null || getRealises().contains(requirement)) {
			return;
		}
		// associate new
		getRealises().add(requirement);
		// additional business logic
		onAddToRealises(requirement);
	}

	@MemberOrder(name = "realises", sequence = "20")
	public void removeFromRealises(final Requirement requirement) {
		// check for no-op
		if (requirement == null || !getRealises().contains(requirement)) {
			return;
		}
		// dissociate existing
		getRealises().remove(requirement);
		// additional business logic
		onRemoveFromRealises(requirement);
	}

	protected void onAddToRealises(final Requirement requirement) {
	}

	protected void onRemoveFromRealises(final Requirement requirement) {
	}

	@MemberOrder(name = "realises", sequence = "10")
	public void realisesRequirement(Requirement requirement) {
		this.addToRealises(requirement);
	}

	@MemberOrder(name = "realises", sequence = "20")
	public void notRealisesRequirement(Requirement requirement) {
		this.removeFromRealises(requirement);
	}

	// }}

}
