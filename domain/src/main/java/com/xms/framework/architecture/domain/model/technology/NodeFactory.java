package com.xms.framework.architecture.domain.model.technology;

import java.util.List;

import org.apache.isis.applib.annotation.DescribedAs;
import org.apache.isis.applib.annotation.MultiLine;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.filter.Filter;
import org.apache.isis.applib.util.TitleBuffer;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class NodeFactory extends AbstractMultiTenantEntityRepositoryAndFactory {

	public Node createNode(
			@Named("Name") String name,
			@Named("Description") @Optional @MultiLine(numberOfLines = 5) String description) {

		Node node = this.newTransientMultiTenantEntity(Node.class,
				this.nextIdentity(), name, description);
		persist(node);
		return node;
	}

	public Node createNodeWithDeviceAndSystemSoftware(
			@Named("Name") String name,
			@Named("Description") @Optional @MultiLine(numberOfLines = 5) String description,
			@Named("Device Name") @Optional @DescribedAs("It can be the Asset ID, FQDN, Serial Number, If none specified, the Node name will be used.") String deviceName,
			@Named("Operating System Name") String operatingSystemName,
			@Named("IP Address") @Optional String ipAddress,
			@Named("IP Mask") @Optional String ipMask) {

		Node node = this.createNode(name, description);
		if (deviceName == null || deviceName.isEmpty())
			deviceName = name;
		Device device = node.composeOfNewDevice(deviceName, null);
		device.assignToNewSystemSoftware(operatingSystemName, null);

		if (ipAddress != null && ipMask != null)
			node.addIpAddress(ipAddress, ipMask);

		return node;
	}

	/**
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param b
	 * @return
	 */
	public Node createNodeOnIp(@Named("Name") String name,
			@Named("Description") @Optional String description,
			@Named("IP Address") String ipAddress,
			@Named("IP Mask") String ipMask) {

		Node node = this.createNode(name, description);
		node.addIpAddress(ipAddress, ipMask);
		return node;

	}

	// }}

	// This action is generalized by passing a "Node" as a parameter, and not
	// implemented directly on "Node", because as it appears on a Repository or
	// Factory
	// it's automatically registered as an Apache Isis Action for all Entities
	// appearing as parameters. On this way, this action will automatically be
	// registered as a "Node" and "CommunicationPath" action without needing to
	// redeclare it on the "CommunicationPath" class definition.
	// For better express the model in the Ubiquitous Language, is preferred
	// that this methods appear as actions on "Node" and "CommunicationPath"
	// entities. In order to avoid them to appear as "duplicated" it must be
	// marked as or declared as "protected".
	protected NodeCommunicationPath createNodeAssociationWithCommunicationPath(
			@Named("Node") Node node,
			@Named("Communication Path") CommunicationPath communicationPath) {

		NodeCommunicationPath nodeAssociationWithCommunicationPath = null;

		// See if the Communication Path was previously associated
		// with this Node.
		for (NodeCommunicationPath currentPathAssociationWithCommunicationPath : node
				.getAssociatedWithCommunicationPaths()) {

			if (currentPathAssociationWithCommunicationPath
					.getCommunicationPath().equals(communicationPath)) {

				nodeAssociationWithCommunicationPath = currentPathAssociationWithCommunicationPath;
			}
		}

		// if not previously associated, create the association.
		if (nodeAssociationWithCommunicationPath == null) {

			nodeAssociationWithCommunicationPath = newTransientDomainObject(
					NodeCommunicationPath.class, null);

			nodeAssociationWithCommunicationPath
					.setCommunicationPath(communicationPath);
			nodeAssociationWithCommunicationPath.setNode(node);

			persist(nodeAssociationWithCommunicationPath);

			node.getAssociatedWithCommunicationPaths().add(
					nodeAssociationWithCommunicationPath);
			communicationPath.getAssociatedWithNodes().add(
					nodeAssociationWithCommunicationPath);

			return nodeAssociationWithCommunicationPath;
		}
		return nodeAssociationWithCommunicationPath;

	}

	protected NodeCommunicationPathAddress createNodeAssociationWithCommunicationPathOnAddress(
			@Named("Node") Node node,
			@Named("Communication Path") CommunicationPath communicationPath,
			@Named("Address") String address, @Named("Mask") String mask) {

		NodeCommunicationPath nodeAssociatedWithCommunicationPath = this
				.createNodeAssociationWithCommunicationPath(node,
						communicationPath);

		NodeCommunicationPathAddress nodeAssociatedWithCommunicationPathAddress = null;
		for (NodeCommunicationPathAddress currentAddress : nodeAssociatedWithCommunicationPath
				.getAddresses()) {

			if (currentAddress.getAddress().equals(address)
					&& currentAddress.getMask().equals(mask)) {
				nodeAssociatedWithCommunicationPathAddress = currentAddress;
			}
		}
		if (nodeAssociatedWithCommunicationPathAddress == null) {
			nodeAssociatedWithCommunicationPathAddress = newTransientDomainObject(
					NodeCommunicationPathAddress.class, null);
			nodeAssociatedWithCommunicationPathAddress
					.setNodeAssociatedWithCommunicationPath(nodeAssociatedWithCommunicationPath);
			nodeAssociatedWithCommunicationPathAddress.setAddress(address);
			nodeAssociatedWithCommunicationPathAddress.setMask(mask);

			persist(nodeAssociatedWithCommunicationPathAddress);

			nodeAssociatedWithCommunicationPath
					.addToAddresses(nodeAssociatedWithCommunicationPathAddress);

		}

		return nodeAssociatedWithCommunicationPathAddress;

	}

	/**
	 * @param node
	 * @param name
	 * @param description
	 * @return
	 */
	protected Device createDevice(Node node, String name, String description) {
		Device device = newTransientMultiTenantEntity(Device.class, null, name,
				description);
		device.modifyPartOfNode(node);
		persist(device);
		return device;
	}

	/**
	 * @param device
	 * @param name
	 * @param description
	 * @return
	 */
	protected SystemSoftware createSystemSoftware(Device device, String name,
			String description) {
		SystemSoftware systemSoftware = newTransientMultiTenantEntity(
				SystemSoftware.class, null, name, description);
		systemSoftware.modifyAssignedToDevice(device);
		persist(systemSoftware);
		return systemSoftware;
	}

	/**
	 * @param node
	 * @param name
	 * @param description
	 */
	protected InfrastructureService createInfrastructureService(
			AbstractNode node, String name, String description) {
		InfrastructureService infrastructureService = newTransientMultiTenantEntity(
				InfrastructureService.class, null, name, description);
		infrastructureService.modifyAssignedToNode(node);
		persist(infrastructureService);

		// Create the internal Infrastructure Function with same name and
		// description.
		InfrastructureFunction infrastructureFunction = newTransientMultiTenantEntity(
				InfrastructureFunction.class, null, name, description);

		persist(infrastructureFunction);

		infrastructureFunction
				.modifyRealisesInfrastructureService(infrastructureService);

		return infrastructureService;

	}

	/**
	 * @param node
	 * @param name
	 * @param description
	 * @return
	 */
	protected Artifact createArtifact(AbstractNode node, String name,
			String description) {
		Artifact artifact = newTransientMultiTenantEntity(Artifact.class, null,
				name, description);
		artifact.modifyAssignedToNode(node);
		persist(artifact);

		return artifact;
	}

	/**
	 * @param name
	 * @param description
	 * @return
	 */
	protected InfrastructureInterface createInfrastructureInterface(
			AbstractNode node, String name, String description) {
		InfrastructureInterface infrastructureInterface = newTransientMultiTenantEntity(
				InfrastructureInterface.class, null, name, description);

		assert node != null : "Node is Null!";

		infrastructureInterface.modifyPartOfNode(node);

		infrastructureInterface = persist(infrastructureInterface);

		return infrastructureInterface;
	}

	/**
	 * @param ipProtocol
	 * @param ipPort
	 * @return
	 */
	protected InfrastructureInterface createIpInfrastructureInterface(
			NodeCommunicationPathAddress nodeAddress, IpProtocol ipProtocol,
			int ipPort) {

		InfrastructureInterface infrastructureInterface = createInfrastructureInterface(
				nodeAddress.getNodeAssociatedWithCommunicationPath().getNode(),
				new TitleBuffer().append(ipProtocol.name()).append(":", ipPort)
						.toString(), null);

		// TODO: Remove Embedded or test with newer version. The following
		// instruction raises a
		// java.lang.NullPointerException at
		// org.apache.isis.objectstore.jdo.datanucleus.persistence.FrameworkSynchronizer$3.run(FrameworkSynchronizer.java:163)
		infrastructureInterface.getIpInterface().setNodeAddress(nodeAddress);
		infrastructureInterface.getIpInterface().setIpProtocol(ipProtocol);
		infrastructureInterface.getIpInterface().setIpPort(ipPort);

		return infrastructureInterface;
	}

	/**
	 * @param name
	 * @param description
	 */
	protected Artifact createArtifact(String name, String description) {

		return newPersistentMultiTenantEntity(Artifact.class, null, name,
				description);

	}

	/**
	 * @param infrastructureService
	 * @param infrastructureService2
	 */
	protected InfrastructureServiceUsedByInfrastructureService createInfrastructureServiceUsedByInfrastructureService(
			InfrastructureService sourceInfrastructureService,
			InfrastructureService targetInfrastructureService) {

		InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService = newPersistentMultiTenantObject(
				InfrastructureServiceUsedByInfrastructureService.class, null);
		infrastructureServiceUsedByInfrastructureService
				.setSourceInfrastructureService(sourceInfrastructureService);
		infrastructureServiceUsedByInfrastructureService
				.setTargetInfrastructureService(targetInfrastructureService);
		return infrastructureServiceUsedByInfrastructureService;

	}

	/**
	 * @param infrastructureService
	 * @return
	 */
	protected List<InfrastructureServiceUsedByInfrastructureService> getInfrastructureServicesThatUsesInfrastructureService(
			final InfrastructureService infrastructureService) {

		return allMatches(
				InfrastructureServiceUsedByInfrastructureService.class,
				new Filter<InfrastructureServiceUsedByInfrastructureService>() {

					@Override
					public boolean accept(
							InfrastructureServiceUsedByInfrastructureService t) {
						return t.getSourceInfrastructureService().equals(
								infrastructureService);
					}
				});

	}

	/**
	 * @param infrastructureInterface
	 * @param infrastructureService
	 * @return
	 */
	protected InfrastructureInterfaceUsedByInfrastructureService createInfrastructureInterfaceUsedByInfrastructureService(
			InfrastructureInterface infrastructureInterface,
			InfrastructureService infrastructureService) {

		InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService = newPersistentMultiTenantObject(
				InfrastructureInterfaceUsedByInfrastructureService.class, null);
		infrastructureInterfaceUsedByInfrastructureService
				.setInfrastructureInterface(infrastructureInterface);
		infrastructureInterfaceUsedByInfrastructureService
				.setInfrastructureService(infrastructureService);
		return infrastructureInterfaceUsedByInfrastructureService;

	}

	/**
	 * @param infrastructureInterface
	 * @return
	 */
	protected List<InfrastructureInterfaceUsedByInfrastructureService> getInfrastructureServicesThatUsesInfrastructureInterface(
			final InfrastructureInterface infrastructureInterface) {

		return allMatches(
				InfrastructureInterfaceUsedByInfrastructureService.class,
				new Filter<InfrastructureInterfaceUsedByInfrastructureService>() {

					@Override
					public boolean accept(
							InfrastructureInterfaceUsedByInfrastructureService t) {
						return t.getInfrastructureInterface().equals(
								infrastructureInterface);
					}
				});

	}

	/**
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @param string6
	 * @param string7
	 * @param string8
	 * @param string9
	 * @param object
	 * @return
	 */
	public Node createNodeWithHardwareVirtualization(
			@Named("Virtualization Node Name") String virtualizationNodeName,
			@Named("Virtualization Node Description") @Optional String virtualizationNodeDescription,
			@Named("Device 1 Name") String device1Name,
			@Named("Device 2 Name") String device2Name,
			@Named("Name of the Common Operating System on Devices") String nameOfTheCommonOperatingSystemOnDevices,
			@Named("Virtualization Software name") String virtualizationSoftwareName,
			@Named("Virtual Node 1 - Name") String virtualNode1Name,
			@Named("Virtual Node 1 - Operating System Name") String virtualNode1OperatingSystemName,
			@Named("Virtual Node 2 - Name") String virtualNode2Name,
			@Named("Virtual Node 2 - Operating System Name") String virtualNode2OperatingSystemName) {

		Node node = this.createNodeWithDeviceAndSystemSoftware(
				virtualizationNodeName, virtualizationNodeDescription,
				device1Name, nameOfTheCommonOperatingSystemOnDevices, null,
				null);
		Device device1 = node.getComposedOfDevices().iterator().next();
		SystemSoftware device1OperatingSystem = device1
				.getAssignedToSystemSoftware().iterator().next();

		Device device2 = node.composeOfNewDevice(device2Name, null);
		SystemSoftware device2OperatingSystem = device2
				.getAssignedToSystemSoftware().iterator().next();

		SystemSoftware virtualizationSoftware1 = device1
				.assignToNewSystemSoftware(virtualizationSoftwareName, null);

		SystemSoftware virtualizationSoftware2 = device2
				.assignToNewSystemSoftware(virtualizationSoftwareName, null);

		// XMS is not supporting that System Software is not assigned to any
		// node. Also, System Software cannot be assigned to 2 System Software
		// instances. The way to model this would be through virtualization
		// Infrastructure Services assigned to virtualization software assigned
		// to both physical devices.
		InfrastructureService virtualizationInfrastructureService1 = virtualizationSoftware1
				.assignToNewInfrastructureService("virtualization service",
						null, null);
		InfrastructureService virtualizationInfrastructureService2 = virtualizationSoftware2
				.assignToNewInfrastructureService("virtualization service",
						null, null);

		// One service simulates the "master". If both can be "master", and are
		// mutually dependent they could have mutual dependency.
		virtualizationInfrastructureService1
				.usesInfrastructureService(virtualizationInfrastructureService2);

		// Also, a Device shouldn't be able to be assigned to System Software.
		// For us, a Device is a "physical" Device. It has specific threats,
		// energy requirements, etc. not available for "virtual" Devices (that
		// are software managed by software).

		// Virtual Nodes A and B are Services simulating a Node provided by the
		// Virtualization Software services.
		InfrastructureService virtualizationInfrastructureServiceForVirtualNodeA = virtualizationSoftware1
				.assignToNewInfrastructureService("virtual Node A", null, null);

		virtualizationInfrastructureServiceForVirtualNodeA
				.usesInfrastructureService(virtualizationInfrastructureService1);
		virtualizationInfrastructureServiceForVirtualNodeA
				.usesInfrastructureService(virtualizationInfrastructureService2);

		InfrastructureService virtualizationInfrastructureServiceForVirtualNodeB = virtualizationSoftware1
				.assignToNewInfrastructureService("virtual Node B", null, null);

		virtualizationInfrastructureServiceForVirtualNodeB
				.usesInfrastructureService(virtualizationInfrastructureService1);
		virtualizationInfrastructureServiceForVirtualNodeB
				.usesInfrastructureService(virtualizationInfrastructureService2);

		// TODO: If an AbstractNode can be assigned to any other AbstractNode
		// (without replacing current ComposedOf relationships), the Bizzdesign
		// pattern could be implemented ...

		return node;

	}
}
