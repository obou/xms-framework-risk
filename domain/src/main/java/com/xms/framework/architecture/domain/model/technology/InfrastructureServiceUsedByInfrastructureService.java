package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;
import org.apache.isis.applib.annotation.Title;

import com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class InfrastructureServiceUsedByInfrastructureService extends
		AbstractMultiTenantUnnamedEntity {

	// {{ SourceInfrastructureService (property)
	private InfrastructureService infrastructureService;

	@MemberOrder(sequence = "1")
	public InfrastructureService getSourceInfrastructureService() {
		return infrastructureService;
	}

	public void setSourceInfrastructureService(
			final InfrastructureService infrastructureService) {
		this.infrastructureService = infrastructureService;
	}

	// }}

	public void modifySourceInfrastructureService(
			final InfrastructureService infrastructureService) {
		InfrastructureService currentSourceInfrastructureService = getSourceInfrastructureService();
		// check for no-op
		if (infrastructureService == null
				|| infrastructureService
						.equals(currentSourceInfrastructureService)) {
			return;
		}
		// associate new
		setSourceInfrastructureService(infrastructureService);
		// additional business logic
		onModifySourceInfrastructureService(currentSourceInfrastructureService,
				infrastructureService);
	}

	public void clearSourceInfrastructureService() {
		InfrastructureService currentSourceInfrastructureService = getSourceInfrastructureService();
		// check for no-op
		if (currentSourceInfrastructureService == null) {
			return;
		}
		// dissociate existing
		setSourceInfrastructureService(null);
		// additional business logic
		onClearSourceInfrastructureService(currentSourceInfrastructureService);
	}

	protected void onModifySourceInfrastructureService(
			final InfrastructureService oldSourceInfrastructureService,
			final InfrastructureService newSourceInfrastructureService) {
	}

	protected void onClearSourceInfrastructureService(
			final InfrastructureService oldSourceInfrastructureService) {
	}

	// }}

	// {{ TargetInfrastructureService (property)
	private InfrastructureService targetInfrastructureService;

	@MemberOrder(sequence = "1")
	public InfrastructureService getTargetInfrastructureService() {
		return targetInfrastructureService;
	}

	public void setTargetInfrastructureService(
			final InfrastructureService targetInfrastructureService) {
		this.targetInfrastructureService = targetInfrastructureService;
	}

	public void modifyTargetInfrastructureService(
			final InfrastructureService infrastructureService) {
		InfrastructureService currentTargetInfrastructureService = getTargetInfrastructureService();
		// check for no-op
		if (infrastructureService == null
				|| infrastructureService
						.equals(currentTargetInfrastructureService)) {
			return;
		}
		// delegate to parent to associate
		infrastructureService.addToUsesInfrastructureServices(this);
		// additional business logic
		onModifyTargetInfrastructureService(currentTargetInfrastructureService,
				infrastructureService);
	}

	public void clearTargetInfrastructureService() {
		InfrastructureService currentTargetInfrastructureService = getTargetInfrastructureService();
		// check for no-op
		if (currentTargetInfrastructureService == null) {
			return;
		}
		// delegate to parent to dissociate
		currentTargetInfrastructureService
				.removeFromUsesInfrastructureServices(this);
		// additional business logic
		onClearTargetInfrastructureService(currentTargetInfrastructureService);
	}

	protected void onModifyTargetInfrastructureService(
			final InfrastructureService oldTargetInfrastructureService,
			final InfrastructureService newTargetInfrastructureService) {
	}

	protected void onClearTargetInfrastructureService(
			final InfrastructureService oldTargetInfrastructureService) {
	}

	// }}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity
	 * #getName()
	 */
	@Override
	@NotPersisted
	@NotPersistent
	@Disabled
	@Title
	public String getName() {
		return getSourceInfrastructureService().getName().concat(" -> ")
				.concat(getTargetInfrastructureService().getName());
	}

}
