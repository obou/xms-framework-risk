package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Dirección Web"),
		@XMSEntityName(locale = "en", name = "Web Address") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class WebAddress extends Address {

}
