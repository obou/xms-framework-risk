package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.ActiveStructureElement;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
@javax.jdo.annotations.Queries({ @javax.jdo.annotations.Query(name = "entity_by_id", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.business.BusinessCollaboration e WHERE e.id == :id") })
public class BusinessCollaboration extends BusinessRole {

	// {{ Aggregates
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAggregates() {
		return getRelationshipRepository().relationshipsAsSourceOfType(this,
				RelationshipType.AGGREGATION);
	}

	/**
	 * @param businessRole
	 */
	// Both Business Roles and Business Actors can be assigned to Business
	// Collaborations.
	protected void addToAggregates(ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.AGGREGATION);
		// Check for no-op
		if (activeStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		getRelationshipFactory().createRelationship(this,
				activeStructureElement, RelationshipType.AGGREGATION);

	}

	/**
	 * @param businessRole
	 */
	// Both Business Roles and Business Actors can be assigned to Business
	// Collaborations.
	protected void removeFromAggregates(
			ActiveStructureElement activeStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = getRelationshipRepository()
				.relationshipBySourceTargetAndType(this,
						activeStructureElement, RelationshipType.AGGREGATION);
		// Check for no-op
		if (activeStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		getDomainObjectContainer().remove(relationship);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "10")
	public void aggregatesBusinessRole(BusinessRole businessRole) {
		addToAggregates(businessRole);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "20")
	public void notAggregatesBusinessRole(BusinessRole businessRole) {
		removeFromAggregates(businessRole);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "10")
	public void aggregatesBusinessActor(BusinessActor businessActor) {
		addToAggregates(businessActor);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "aggregates", sequence = "20")
	public void notAggregatesBusinessActor(BusinessActor businessActor) {
		removeFromAggregates(businessActor);

	}

	// }}

	// {{ AssignedTo

	/**
	 * @param businessInteraction
	 */
	@MemberOrder(name = "assignedTo", sequence = "100")
	public void assignedToBusinessInteraction(
			BusinessInteraction businessInteraction) {
		addAssignedTo(businessInteraction);
	}

	/**
	 * @param businessInteraction
	 */
	@MemberOrder(name = "assignedTo", sequence = "110")
	public void notAssignedToBusinessInteraction(
			BusinessInteraction businessInteraction) {
		removeFromAssignedTo(businessInteraction);

	}

	// }}

}
