package com.xms.framework.document.domain.model;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Enumerated;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Document extends AbstractMultiTenantEntity {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Estado") })
	@Enumerated
	private DocumentStatus status;

	public DocumentStatus getStatus() {
		return status;
	}

	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

}
