package com.xms.framework.party.domain.model.address;

import java.util.Set;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Column;
import javax.persistence.Entity;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "País"),
		@XMSEntityName(locale = "en", name = "Country") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Country extends AbstractMultiTenantEntity {

	@Column(length = 3)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Código ISO"),
			@XMSLocale(locale = "en", caption = "ISO Code") })
	private String ISOCode;

	public String getISOCode() {
		return ISOCode;
	}

	public void setISOCode(String iSOCode) {
		// TODO: Validate ISO 3-digit code for country.
		// See:
		// http://stackoverflow.com/questions/139867/is-there-an-open-source-java-enum-of-iso-3166-1-country-codes

		ISOCode = iSOCode;
	}

	public Set<String> getCountryTranslatedName(String language) {
		return null;

		// TODO: Return country name on the specified language.
		// See:
		// http://stackoverflow.com/questions/139867/is-there-an-open-source-java-enum-of-iso-3166-1-country-codes

	}

}