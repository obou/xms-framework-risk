package com.xms.framework.party.domain.model.organization;

public enum VisualizateRoleType {
	SYS_ADMIN, GRC_ADMIN, GRC_USER, READ_ONLY_USER
}
