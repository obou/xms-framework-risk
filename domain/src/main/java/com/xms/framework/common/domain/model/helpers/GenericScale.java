package com.xms.framework.common.domain.model.helpers;

import java.util.List;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.MappedSuperclass;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * Generic Scale composed by different Levels.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <T>
 *            Class representing the Levels.
 * @param <O>
 *            Default order for visualizing the Scale Levels.
 */
//@MappedSuperclass
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class GenericScale extends AbstractMultiTenantEntity {

	/**
	 * Levels that can be chosen from this Scale.
	 */
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Niveles"),
			@XMSLocale(locale = "en", caption = "Levels") }, show = false)
	//@Transient
	@OrderBy("desc value")
	protected List<GenericScaleLevel> levels;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.risk.criteria.api.domain.Scale#getLevels()
	 */

	//@Transient
	@XMSField(show = false)
	public abstract Set<GenericScaleLevel> getLevels();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.risk.criteria.api.domain.Scale#getScaleLevelForValue
	 * (java.lang.Object)
	 */
	public GenericScaleLevel computeScaleLevelForValue(Double value) {
		if (getLevels() != null) {
			for (GenericScaleLevel currentLevel : getLevels()) {
				if (currentLevel.isValueIncluded(value)) {
					return currentLevel;
				}
			}
		}
		return null;
	}

	public GenericScaleLevel computeScaleLevelForName(String name) {
		GenericScaleLevel foundLevel = null;

		for (GenericScaleLevel currentLevel : getLevels()) {
			if (currentLevel.isNameIncluded(name)) {
				foundLevel = currentLevel;
				break;
			}
		}
		return foundLevel;
	}
}
