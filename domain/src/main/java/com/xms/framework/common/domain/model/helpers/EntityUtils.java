package com.xms.framework.common.domain.model.helpers;

import java.util.Locale;

import org.apache.isis.applib.annotation.NotPersistable;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@NotPersistable
public class EntityUtils {

	/**
	 * @param class1
	 * @param locale
	 * @return
	 */
	public static String getDescription(Class<?> class1, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param likelihoodOfThreatEventOccurrence
	 * @param locale
	 * @return
	 */
	public static String getFieldName(Object object1, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param likelihoodOfThreatEventOccurrence
	 * @param locale
	 * @return
	 */
	public static String getFieldDescription(Object object1, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param likelihoodOfThreatEventOccurrence
	 * @param value
	 * @return
	 */
	public static Boolean getScaleLevelBinIsValueIncluded(Object object1,
			Integer value) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @param class1
	 * @param locale
	 * @return
	 */
	public static String getName(Class<?> class1, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param likelihoodOfThreatEventOccurrence
	 * @param value
	 * @return
	 */
	public static ScaleLevelBin<?, ?> getScaleLevelForValue(Scale<?, ?> scale,
			Integer value) {
		// TODO Auto-generated method stub
		return null;
	}

}
