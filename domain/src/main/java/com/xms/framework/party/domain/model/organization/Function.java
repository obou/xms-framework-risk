package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Función"),
		@XMSEntityName(locale = "en", name = "Function") })
//@Entity(name = "organization_function")
@PersistenceCapable
public class Function extends AbstractMultiTenantEntity {

}
