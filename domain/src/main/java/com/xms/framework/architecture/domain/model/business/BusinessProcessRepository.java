package com.xms.framework.architecture.domain.model.business;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 *
 */
public class BusinessProcessRepository extends
		AbstractMultiTenantEntityRepositoryAndFactory {

}
