package com.xms.framework.architecture.domain.model.business;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.BehaviorElement;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessProcess extends BehaviorElement {

	// {{ Realizes
	/**
	 * @param businessService
	 */
	@MemberOrder(name = "realizes", sequence = "10")
	public void realizesBusinessService(BusinessService businessService) {
		super.addToRealizes(businessService);
	}

	/**
	 * @param businessService
	 */
	@MemberOrder(name = "realizes", sequence = "20")
	public void notRealizesBusinessService(BusinessService businessService) {
		super.removeFromRealizes(businessService);
	}

	// }}

	// {{ Uses

	/**
	 * @param businessService
	 */
	@MemberOrder(name = "uses", sequence = "10")
	public void usesBusinessService(BusinessService businessService) {
		super.addToUses(businessService);
	}

	/**
	 * @param businessService
	 */
	@MemberOrder(name = "uses", sequence = "20")
	public void notUsesBusinessService(BusinessService businessService) {
		super.removeFromUses(businessService);
	}

	// }}

	// {{ AssignedFrom

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "assignedFrom", sequence = "10")
	public void assignedFromBusinessRole(BusinessRole businessRole) {
		super.addToAssignedFrom(businessRole);

	}

	/**
	 * @param businessRole
	 */
	@MemberOrder(name = "assignedFrom", sequence = "20")
	public void notAssignedFromBusinessRole(BusinessRole businessRole) {
		super.removeFromAssignedFrom(businessRole);
	}

	// public String validateAssignedFromBusinessRole(
	// BusinessRole businessRole) {
	// if ((this instanceof BusinessInteraction)
	// && !(businessRole instanceof BusinessCollaboration)) {
	// return
	// "An Business Interaction can only be assigned to Business Collaborations, not to usual Business Components";
	// }
	// return null;
	// }

	// }}

	// {{ Triggers

	/**
	 * @param businessFunction2
	 */
	@MemberOrder(name = "triggers", sequence = "10")
	public void triggersBusinessFunction(BusinessFunction businessFunction) {
		super.addToTriggers(businessFunction);

	}

	/**
	 * @param businessFunction2
	 */
	@MemberOrder(name = "triggers", sequence = "20")
	public void notTriggersBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromTriggers(businessFunction);

	}

	/**
	 * @param businessEvent
	 */
	public void triggersBusinessEvent(BusinessEvent businessEvent) {
		super.addToTriggers(businessEvent);

	}

	@MemberOrder(name = "triggers", sequence = "20")
	public void notTriggersBusinessEvent(BusinessEvent businessEvent) {
		super.removeFromTriggers(businessEvent);

	}

	// }}

	// {{ TriggeredBy

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "triggeredBy", sequence = "10")
	public void triggeredByBusinessFunction(BusinessFunction businessFunction) {
		super.addToTriggeredBy(businessFunction);

	}

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "triggeredBy", sequence = "10")
	public void notTriggeredByBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromTriggeredBy(businessFunction);

	}

	// }}

	// {{ FlowTo

	/**
	 * @param businessFunction2
	 */
	@MemberOrder(name = "flowTo", sequence = "10")
	public void flowToBusinessFunction(BusinessFunction businessFunction) {
		super.addToFlowTo(businessFunction);

	}

	/**
	 * @param businessFunction2
	 */
	@MemberOrder(name = "flowTo", sequence = "20")
	public void notFlowToBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromFlowTo(businessFunction);

	}

	// }}

	// {{ FlowFrom

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "flowFrom", sequence = "10")
	public void flowFromBusinessFunction(BusinessFunction businessFunction) {
		super.addToFlowFrom(businessFunction);

	}

	/**
	 * @param businessFunction
	 */
	@MemberOrder(name = "flowFrom", sequence = "20")
	public void notFlowFromBusinessFunction(BusinessFunction businessFunction) {
		super.removeFromFlowFrom(businessFunction);

	}

	// }}

	// {{ Accesses

	/**
	 * @param businessObject
	 */
	@MemberOrder(name = "accesses", sequence = "10")
	public void accessesBusinessObject(BusinessObject businessObject) {
		super.addToAccesses(businessObject);

	}

	/**
	 * @param businessObject
	 */
	@MemberOrder(name = "accesses", sequence = "20")
	public void notAccessesBusinessObject(BusinessObject businessObject) {
		super.removeFromAccesses(businessObject);

	}

	// }}

}
