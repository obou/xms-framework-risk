package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.NotPersisted;
import org.apache.isis.applib.annotation.Optional;
import org.apache.isis.applib.annotation.Title;
import org.apache.isis.applib.util.TitleBuffer;

import com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class NodeCommunicationPathAddress extends
		AbstractMultiTenantUnnamedEntity {

	private NodeCommunicationPath nodeAssociatedWithCommunicationPath;

	private String address;

	private String mask;

	private NodeFactory nodeFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity
	 * #getName()
	 */
	@Override
	@Optional
	@NotPersisted
	@NotPersistent
	@Title
	public String getName() {
		return new TitleBuffer()
				.append(nodeAssociatedWithCommunicationPath.getName())
				.append(" (", this.getAddress()).concat(" , ", this.getMask())
				.concat(")").toString();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public NodeCommunicationPath getNodeAssociatedWithCommunicationPath() {
		return nodeAssociatedWithCommunicationPath;
	}

	public void setNodeAssociatedWithCommunicationPath(
			NodeCommunicationPath nodeAssociatedWithCommunicationPath) {
		this.nodeAssociatedWithCommunicationPath = nodeAssociatedWithCommunicationPath;
	}

	// {{ addNewInfrastructureInterface (action)
	@MemberOrder(sequence = "1")
	public InfrastructureInterface addNewIpInfrastructureInterface(
			@Named("IP Protocol") IpProtocol ipProtocol,
			@Named("IP Port") int ipPort) {
		return nodeFactory.createIpInfrastructureInterface(this, ipProtocol,
				ipPort);
	}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}
	// }}

}
