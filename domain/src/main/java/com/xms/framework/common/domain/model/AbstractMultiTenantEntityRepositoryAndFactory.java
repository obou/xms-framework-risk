package com.xms.framework.common.domain.model;

import com.xms.framework.domain.api.domain.MultiTenantEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class AbstractMultiTenantEntityRepositoryAndFactory extends
		AbstractMultiTenantObjectRepositoryAndFactory {

	protected <T extends MultiTenantEntity> T newTransientMultiTenantEntity(
			final Class<T> ofType, String id, String name, String description) {

		T entity = newTransientMultiTenantObject(ofType, id);
		entity.setName(name);
		entity.setDescription(description);
		return entity;

	}

	protected <T extends MultiTenantEntity> T newPersistentMultiTenantEntity(
			final Class<T> ofType, String id, String name, String description) {

		T entity = newTransientMultiTenantEntity(ofType, id, name, description);
		persist(entity);
		return entity;
	}

}
