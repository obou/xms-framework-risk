package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

//@Entity
@PersistenceCapable
public class VisualizationRole extends Role {

	@Enumerated
	private VisualizateRoleType visualizateRoleType;

	public VisualizateRoleType getVisualizateRoleType() {
		return visualizateRoleType;
	}

	public void setVisualizateRoleType(VisualizateRoleType visualizateRoleType) {
		this.visualizateRoleType = visualizateRoleType;
	}

	@PrePersist
	@PreUpdate
	public void onSave() {
		this.setName(visualizateRoleType.name());
	}

}
