package com.xms.framework.common.domain.model.helpers;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.xms.framework.annotation.ComponentType;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * Represents a Level on a Scale.
 * 
 * This implementation does not support descriptions on multiple Locales.
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <S>
 *            Scale this Level is related to.
 * @param <V>
 *            Type used to express the Scale Level value.
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
//@MappedSuperclass
public abstract class GenericScaleLevel extends AbstractMultiTenantEntity {

	/**
	 * Scale this Level is associated to.
	 */
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Escala"),
			@XMSLocale(locale = "en", caption = "Scale") }, show = false)
	//@Transient
	protected GenericScale scale;

	// /**
	// * Order index of this Level element on the Scale.
	// * <p>
	// * The Scale will show the Levels ordering ASCENDING by Order.
	// */
	// private Integer uniqueIndex;

	/**
	 * Value associated with this Level.
	 * 
	 * It can represent a "relevance index", an amount, etc.
	 * 
	 * No assumptions should be made about greater value is better or vice
	 * versa.
	 */
	private Double value;

	@XMSField(componentType = ComponentType.COLOR_PICKER)
	private String color;

	//@Transient
	public abstract GenericScale getScale();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xms.framework.risk.criteria.api.domain.ScaleLevel#getValue()
	 */
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.risk.criteria.api.domain.ScaleLevel#isValueIncluded
	 * (java.lang.Object)
	 */
	public Boolean isValueIncluded(Double value) {
		return value.equals(this.value);
	}

	public Boolean isNameIncluded(String name) {
		return name.equalsIgnoreCase(this.getName());
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see java.lang.Object#equals(java.lang.Object)
	// */
	// public boolean equals(Object other) {
	// if (other == this)
	// return true;
	// if (other == null)
	// return false;
	// if (getClass() != other.getClass())
	// return false;
	// GenericScaleLevel level = (GenericScaleLevel) other;
	// return (this.value == level.getValue())
	// && (value != null && value.equals(level.getValue()));
	// }
	//
	// public int hashCode() {
	// return value.hashCode();
	// }

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
