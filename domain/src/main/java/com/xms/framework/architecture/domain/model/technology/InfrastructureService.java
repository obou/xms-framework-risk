package com.xms.framework.architecture.domain.model.technology;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;
import org.apache.isis.applib.annotation.Optional;

import com.xms.framework.architecture.domain.model.Entity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
public class InfrastructureService extends Entity {

	// {{ AssignedToNode (property)
	private AbstractNode assignedToNode;

	@MemberOrder(sequence = "1")
	public AbstractNode getAssignedToNode() {
		return assignedToNode;
	}

	protected void setAssignedToNode(final AbstractNode assignedToNode) {
		this.assignedToNode = assignedToNode;
	}

	public void modifyAssignedToNode(final AbstractNode assignedToNode) {
		AbstractNode currentAssignedToNode = getAssignedToNode();
		// check for no-op
		if (assignedToNode == null
				|| assignedToNode.equals(currentAssignedToNode)) {
			return;
		}
		// delegate to parent to associate
		assignedToNode.addToAssignedToInfrastructureServices(this);
		// additional business logic
		onModifyAssignedToNode(currentAssignedToNode, assignedToNode);
	}

	public void clearAssignedToNode() {
		AbstractNode currentAssignedToNode = getAssignedToNode();
		// check for no-op
		if (currentAssignedToNode == null) {
			return;
		}
		// delegate to parent to dissociate
		currentAssignedToNode.removeFromAssignedToInfrastructureServices(this);
		// additional business logic
		onClearAssignedToNode(currentAssignedToNode);
	}

	protected void onModifyAssignedToNode(final AbstractNode oldAssignedToNode,
			final AbstractNode newAssignedToNode) {
	}

	protected void onClearAssignedToNode(final AbstractNode oldAssignedToNode) {
	}

	// }}

	// {{ RealisedByInfrastructureFunctions (Collection)
	private Set<InfrastructureFunction> realisedByInfrastructureFunctions = new LinkedHashSet<InfrastructureFunction>();

	@MemberOrder(sequence = "1")
	public Set<InfrastructureFunction> getRealisedByInfrastructureFunctions() {
		return realisedByInfrastructureFunctions;
	}

	public void setRealisedByInfrastructureFunctions(
			final Set<InfrastructureFunction> realisedByInfrastructureFunctions) {
		this.realisedByInfrastructureFunctions = realisedByInfrastructureFunctions;
	}

	public void addToRealisedByInfrastructureFunctions(
			final InfrastructureFunction infrastructureFunction) {
		// check for no-op
		if (infrastructureFunction == null
				|| getRealisedByInfrastructureFunctions().contains(
						infrastructureFunction)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureFunction.clearRealisesInfrastructureService();
		// associate arg
		infrastructureFunction.setRealisesInfrastructureService(this);
		getRealisedByInfrastructureFunctions().add(infrastructureFunction);
		// additional business logic
		onAddToRealisedByInfrastructureFunctions(infrastructureFunction);
	}

	/**
	 * @param infrastructureFunction
	 */
	private void onAddToRealisedByInfrastructureFunctions(
			InfrastructureFunction infrastructureFunction) {
		// Do nothing.

	}

	public void removeFromRealisedByInfrastructureFunctions(
			final InfrastructureFunction infrastructureFunction) {
		// check for no-op
		if (infrastructureFunction == null
				|| !getRealisedByInfrastructureFunctions().contains(
						infrastructureFunction)) {
			return;
		}
		// dissociate arg
		infrastructureFunction.setRealisesInfrastructureService(null);
		getRealisedByInfrastructureFunctions().remove(infrastructureFunction);
		// additional business logic
		onRemoveFromRealisedByInfrastructureFunctions(infrastructureFunction);
	}

	/**
	 * @param infrastructureFunction
	 */
	private void onRemoveFromRealisedByInfrastructureFunctions(
			InfrastructureFunction infrastructureFunction) {
		// Do nothing.

	}

	// }}

	// {{ AssignedToInfrastructureInterfaces (Collection)
	private Set<InfrastructureInterface> assignedToInfrastructureInterfaces = new LinkedHashSet<InfrastructureInterface>();
	private NodeFactory nodeFactory;

	@MemberOrder(sequence = "1")
	public Set<InfrastructureInterface> getAssignedToInfrastructureInterfaces() {
		return assignedToInfrastructureInterfaces;
	}

	public void setAssignedToInfrastructureInterfaces(
			final Set<InfrastructureInterface> assignedToInfrastructureInterfaces) {
		this.assignedToInfrastructureInterfaces = assignedToInfrastructureInterfaces;
	}

	public void addToAssignedToInfrastructureInterfaces(
			final InfrastructureInterface infrastructureInterface) {
		// check for no-op
		if (infrastructureInterface == null
				|| getAssignedToInfrastructureInterfaces().contains(
						infrastructureInterface)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureInterface.clearAssignedToInfrastructureService();
		// associate arg
		infrastructureInterface.setAssignedToInfrastructureService(this);
		getAssignedToInfrastructureInterfaces().add(infrastructureInterface);
		// additional business logic
		onAddToAssignedToInfrastructureInterfaces(infrastructureInterface);
	}

	/**
	 * @param infrastructureInterface
	 */
	private void onAddToAssignedToInfrastructureInterfaces(
			InfrastructureInterface infrastructureInterface) {
		// Do nothing.

	}

	public void removeFromAssignedToInfrastructureInterfaces(
			final InfrastructureInterface infrastructureInterface) {
		// check for no-op
		if (infrastructureInterface == null
				|| !getAssignedToInfrastructureInterfaces().contains(
						infrastructureInterface)) {
			return;
		}
		// dissociate arg
		infrastructureInterface.setAssignedToInfrastructureService(null);
		getAssignedToInfrastructureInterfaces().remove(infrastructureInterface);
		// additional business logic
		onRemoveFromAssignedToInfrastructureInterfaces(infrastructureInterface);
	}

	/**
	 * @param infrastructureInterface
	 */
	private void onRemoveFromAssignedToInfrastructureInterfaces(
			InfrastructureInterface infrastructureInterface) {
		// Do nothing.

	}

	public InfrastructureInterface assignToNewInfrastructureInterface(
			@Named("Name") String name,
			@Named("Description") @Optional String description) {
		InfrastructureInterface infrastructureInterface = nodeFactory
				.createInfrastructureInterface(assignedToNode, name,
						description);
		infrastructureInterface.modifyAssignedToInfrastructureService(this);
		return infrastructureInterface;
	}

	/**
	 * @param interfaceName
	 * @param object
	 * @param nodeAddress
	 * @param ipProtocol
	 * @param ipPort
	 */
	public void assignToNewInfrastructureInterfaceOnIp(
			NodeCommunicationPathAddress nodeAddress, IpProtocol ipProtocol,
			Integer ipPort) {

		nodeFactory.createIpInfrastructureInterface(nodeAddress, ipProtocol,
				ipPort);

	}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	// }}

	// {{ UsesInfrastructureServices (Collection)

	@Persistent(mappedBy = "targetInfrastructureService")
	private Set<InfrastructureServiceUsedByInfrastructureService> usesInfrastructureServices = new LinkedHashSet<InfrastructureServiceUsedByInfrastructureService>();

	@MemberOrder(sequence = "1")
	public Set<InfrastructureServiceUsedByInfrastructureService> getUsesInfrastructureServices() {
		return usesInfrastructureServices;
	}

	public void setUsesInfrastructureServices(
			final Set<InfrastructureServiceUsedByInfrastructureService> usesInfrastructureServices) {
		this.usesInfrastructureServices = usesInfrastructureServices;
	}

	public void addToUsesInfrastructureServices(
			final InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService) {
		// check for no-op
		if (infrastructureServiceUsedByInfrastructureService == null
				|| getUsesInfrastructureServices().contains(
						infrastructureServiceUsedByInfrastructureService)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureServiceUsedByInfrastructureService
				.clearTargetInfrastructureService();
		// associate arg
		infrastructureServiceUsedByInfrastructureService
				.setTargetInfrastructureService(this);
		getUsesInfrastructureServices().add(
				infrastructureServiceUsedByInfrastructureService);
		// additional business logic
		onAddToUsesInfrastructureServices(infrastructureServiceUsedByInfrastructureService);
	}

	/**
	 * @param infrastructureServiceUsedByInfrastructureService
	 */
	private void onAddToUsesInfrastructureServices(
			InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService) {
		// Do nothing.

	}

	public void removeFromUsesInfrastructureServices(
			final InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService) {
		// check for no-op
		if (infrastructureServiceUsedByInfrastructureService == null
				|| !getUsesInfrastructureServices().contains(
						infrastructureServiceUsedByInfrastructureService)) {
			return;
		}
		// dissociate arg
		infrastructureServiceUsedByInfrastructureService
				.setTargetInfrastructureService(null);
		getUsesInfrastructureServices().remove(
				infrastructureServiceUsedByInfrastructureService);
		// additional business logic
		onRemoveFromUsesInfrastructureServices(infrastructureServiceUsedByInfrastructureService);
	}

	/**
	 * @param operatingSystemServices
	 */
	public void usesInfrastructureService(
			@Named("Source Infrastructure Service") InfrastructureService infrastructureService) {

		// Verify it's not used yet.
		Boolean found = false;
		for (InfrastructureServiceUsedByInfrastructureService current : this
				.getUsesInfrastructureServices()) {
			found = current.getSourceInfrastructureService().equals(
					infrastructureService)
					&& current.getTargetInfrastructureService().equals(this);
		}

		// Add it if not used.
		if (!found) {
			InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService = nodeFactory
					.createInfrastructureServiceUsedByInfrastructureService(
							infrastructureService, this);
			this.addToUsesInfrastructureServices(infrastructureServiceUsedByInfrastructureService);
		}
	}

	/**
	 * @param infrastructureServiceUsedByInfrastructureService
	 */
	private void onRemoveFromUsesInfrastructureServices(
			InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService) {
		// Do nothing.

	}

	// }}

	// {{ UsedByInfrastructureServices

	// Derived relationship.
	@MemberOrder(sequence = "1")
	public List<InfrastructureServiceUsedByInfrastructureService> getUsedByInfrastructureServices() {
		return nodeFactory
				.getInfrastructureServicesThatUsesInfrastructureService(this);
	}

	/**
	 * @param
	 */
	public InfrastructureServiceUsedByInfrastructureService usedByInfrastructureService(
			@Named("Target Infrastructure Service") InfrastructureService infrastructureService) {

		// Verify it's not used yet.
		InfrastructureServiceUsedByInfrastructureService found = null;
		Iterator<InfrastructureServiceUsedByInfrastructureService> iterator = this
				.getUsesInfrastructureServices().iterator();
		while (iterator.hasNext() && found != null) {
			InfrastructureServiceUsedByInfrastructureService current = iterator
					.next();
			if (current.getTargetInfrastructureService().equals(
					infrastructureService)
					&& current.getSourceInfrastructureService().equals(this)) {
				found = current;
			}
		}

		// Add it if not used.
		if (found == null) {

			InfrastructureServiceUsedByInfrastructureService infrastructureServiceUsedByInfrastructureService = nodeFactory
					.createInfrastructureServiceUsedByInfrastructureService(
							this, infrastructureService);
			infrastructureService
					.addToUsesInfrastructureServices(infrastructureServiceUsedByInfrastructureService);
		}

		return found;
	}

	// }}

	// {{ UsesInfrastructureInterfaces (Collection)
	private Set<InfrastructureInterfaceUsedByInfrastructureService> usesInfrastructureInterfaces = new LinkedHashSet<InfrastructureInterfaceUsedByInfrastructureService>();

	@MemberOrder(sequence = "1")
	public Set<InfrastructureInterfaceUsedByInfrastructureService> getUsesInfrastructureInterfaces() {
		return usesInfrastructureInterfaces;
	}

	public void setUsesInfrastructureInterfaces(
			final Set<InfrastructureInterfaceUsedByInfrastructureService> usesInfrastructureInterfaces) {
		this.usesInfrastructureInterfaces = usesInfrastructureInterfaces;
	}

	public void addToUsesInfrastructureInterfaces(
			final InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService) {
		// check for no-op
		if (infrastructureInterfaceUsedByInfrastructureService == null
				|| getUsesInfrastructureInterfaces().contains(
						infrastructureInterfaceUsedByInfrastructureService)) {
			return;
		}
		// dissociate arg from its current parent (if any).
		infrastructureInterfaceUsedByInfrastructureService
				.clearInfrastructureService();
		// associate arg
		infrastructureInterfaceUsedByInfrastructureService
				.setInfrastructureService(this);
		getUsesInfrastructureInterfaces().add(
				infrastructureInterfaceUsedByInfrastructureService);
		// additional business logic
		onAddToUsesInfrastructureInterfaces(infrastructureInterfaceUsedByInfrastructureService);
	}

	/**
	 * @param infrastructureInterfaceUsedByInfrastructureService
	 */
	private void onAddToUsesInfrastructureInterfaces(
			InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService) {
		// Do nothing.

	}

	public void removeFromUsesInfrastructureInterfaces(
			final InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService) {
		// check for no-op
		if (infrastructureInterfaceUsedByInfrastructureService == null
				|| !getUsesInfrastructureInterfaces().contains(
						infrastructureInterfaceUsedByInfrastructureService)) {
			return;
		}
		// dissociate arg
		infrastructureInterfaceUsedByInfrastructureService
				.setInfrastructureService(null);
		getUsesInfrastructureInterfaces().remove(
				infrastructureInterfaceUsedByInfrastructureService);
		// additional business logic
		onRemoveFromUsesInfrastructureInterfaces(infrastructureInterfaceUsedByInfrastructureService);
	}

	/**
	 * @param infrastructureInterfaceUsedByInfrastructureService
	 */
	private void onRemoveFromUsesInfrastructureInterfaces(
			InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService) {
		// Do nothing.

	}

	/**
	 * @param infrastructureInterface
	 * @return
	 */
	public InfrastructureInterfaceUsedByInfrastructureService usesInfrastructureInterface(
			InfrastructureInterface infrastructureInterface) {

		// Verify it's not used yet.
		InfrastructureInterfaceUsedByInfrastructureService found = null;
		Iterator<InfrastructureInterfaceUsedByInfrastructureService> iterator = this
				.getUsesInfrastructureInterfaces().iterator();
		while (iterator.hasNext() && found != null) {
			InfrastructureInterfaceUsedByInfrastructureService current = iterator
					.next();
			if (current.getInfrastructureInterface().equals(
					infrastructureInterface)
					&& current.getInfrastructureService().equals(this)) {
				found = current;
			}
		}

		// Add it if not used.
		if (found == null) {
			InfrastructureInterfaceUsedByInfrastructureService infrastructureInterfaceUsedByInfrastructureService = nodeFactory
					.createInfrastructureInterfaceUsedByInfrastructureService(
							infrastructureInterface, this);
			this.addToUsesInfrastructureInterfaces(infrastructureInterfaceUsedByInfrastructureService);
		}

		return found;

	}
	// }}

}
