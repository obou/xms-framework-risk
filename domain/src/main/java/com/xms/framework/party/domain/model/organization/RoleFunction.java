package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

//@Entity
@PersistenceCapable
public class RoleFunction extends AbstractMultiTenantEntityRelationship {

	@ManyToOne
	private Role role;

	@ManyToOne
	private Function relatedFunction;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Function getRelatedFunction() {
		return relatedFunction;
	}

	public void setRelatedFunction(Function relatedFunction) {
		this.relatedFunction = relatedFunction;
	}

}
