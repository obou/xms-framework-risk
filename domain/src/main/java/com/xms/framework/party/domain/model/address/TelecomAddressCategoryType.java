package com.xms.framework.party.domain.model.address;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

public enum TelecomAddressCategoryType {
	@XMSField(locales={@XMSLocale(locale="es",caption="Móvil"), @XMSLocale(locale="en",caption="Mobile")})
	MOBILE, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Casa"), @XMSLocale(locale="en",caption="Home")})
	HOME, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Trabajo"), @XMSLocale(locale="en",caption="Work")})
	WORK, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Principal"), @XMSLocale(locale="en",caption="Main")})
	MAIN, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Fax de Casa"), @XMSLocale(locale="en",caption="Home Fax")})
	HOME_FAX, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Fax del Trabajo"), @XMSLocale(locale="en",caption="Work Fax")})
	WORK_FAX, 
	@XMSField(locales={@XMSLocale(locale="es",caption="Otro"), @XMSLocale(locale="en",caption="Other")})
	OTHER

}
