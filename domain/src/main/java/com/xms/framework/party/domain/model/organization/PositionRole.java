package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class PositionRole extends AbstractMultiTenantEntityRelationship {

	@ManyToOne(optional = false)
	private Position position;

	@ManyToOne(optional = false)
	private Role role;

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
