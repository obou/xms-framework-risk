package com.xms.framework.enterprisearchitecture.domain.model.application;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class ApplicationComponent extends AbstractMultiTenantEntity {
	//
	// // {{ applicationFunctions (Collection)
	//
	// @Persistent(mappedBy = "employee")
	// private Set<ApplicationFunction> assignedApplicationFunctions = new
	// LinkedHashSet<ApplicationFunction>();
	//
	// @MemberOrder(sequence = "1")
	// public Set<ApplicationFunction> getAssignedApplicationFunctions() {
	// return assignedApplicationFunctions;
	// }
	//
	// public void setAssignedApplicationFunctions(
	// final Set<ApplicationFunction> assignedApplicationFunctions) {
	// this.assignedApplicationFunctions = assignedApplicationFunctions;
	// }
	//
	// // }}
	//
	/**
	 * @param string
	 */
	public void assignNewApplicationFunction(String applicationFunctionName) {
		// ApplicationFunctions applicationFunctions = new
		// ApplicationFunctions();
		// ApplicationFunction applicationFunction =
		// applicationFunctions.create(
		// applicationFunctionName, null);
		// this.assignedApplicationFunctions.add(applicationFunction);
		// ApplicationFunctions.persist(applicationFunction);
	}

}
