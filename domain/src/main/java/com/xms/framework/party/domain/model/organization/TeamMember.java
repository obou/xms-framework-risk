package com.xms.framework.party.domain.model.organization;

import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;
import com.xms.framework.party.domain.model.Party;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Miembro del Equipo"),
		@XMSEntityName(locale = "en", name = "Team Member") })
//@Entity
@PersistenceCapable
public class TeamMember extends AbstractMultiTenantEntity {

	@ManyToOne(optional = false)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Equipo"),
			@XMSLocale(locale = "en", caption = "Team") })
	private Team team;

	@ManyToOne(optional = false)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Miembro"),
			@XMSLocale(locale = "en", caption = "Member") })
	private Party member;

	@ManyToOne(optional = false)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Miembro Alternativo"),
			@XMSLocale(locale = "en", caption = "Member") })
	private Party alternateMember;

	@OneToMany(mappedBy = "teamMember", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Roles"),
			@XMSLocale(locale = "en", caption = "Roles") })
	private Set<TeamMemberRole> roles;

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Set<TeamMemberRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<TeamMemberRole> roles) {
		this.roles = roles;
	}

	public Party getMember() {
		return member;
	}

	public void setMember(Party member) {
		this.member = member;
	}

	public Party getAlternateMember() {
		return alternateMember;
	}

	public void setAlternateMember(Party alternateMember) {
		this.alternateMember = alternateMember;
	}

}
