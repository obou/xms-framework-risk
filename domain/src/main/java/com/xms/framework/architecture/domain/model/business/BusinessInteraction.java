package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Interaction;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessInteraction extends BusinessFunction implements
		Interaction {

	// {{ AssignedFrom

	/**
	 * @return
	 */
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssignedFrom() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.ASSIGNMENT);
	}

	protected void addToAssignedFrom(BusinessCollaboration businessCollaboration) {
		businessCollaboration.assignedToBusinessInteraction(this);
	}

	protected void removeFromAssignedFrom(
			BusinessCollaboration businessCollaboration) {
		businessCollaboration.notAssignedToBusinessInteraction(this);
	}

	// public String validateAssignedFromBusinessRole(
	// BusinessRole businessRole) {
	// if (!(businessRole instanceof BusinessCollaboration)) {
	// return
	// "An Business Interaction can only be assigned to Business Collaborations, not to usual Business Components";
	// }
	// return null;
	// }

	// }}

}
