package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

//@Entity
@PersistenceCapable
public class TeamMemberRole extends AbstractMultiTenantEntityRelationship {

	@ManyToOne(optional = false)
	private TeamMember teamMember;

	@ManyToOne(optional = false)
	private Role role;

	public TeamMember getTeamMember() {
		return teamMember;
	}

	public void setTeamMember(TeamMember teamMember) {
		this.teamMember = teamMember;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
