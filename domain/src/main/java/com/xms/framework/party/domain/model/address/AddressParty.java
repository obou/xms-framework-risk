package com.xms.framework.party.domain.model.address;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;
import com.xms.framework.party.domain.model.Party;

//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class AddressParty extends AbstractMultiTenantEntityRelationship {

	@ManyToOne
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Persona / Organización"),
			@XMSLocale(locale = "en", caption = "Party") })
	private Party party;

	@ManyToOne
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Dirección"),
			@XMSLocale(locale = "en", caption = "Address") })
	private Address address;

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
