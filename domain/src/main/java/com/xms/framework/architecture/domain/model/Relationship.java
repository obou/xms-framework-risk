package com.xms.framework.architecture.domain.model;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Unique;
import javax.jdo.annotations.Uniques;

import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.annotation.Immutable;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Where;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
@javax.jdo.annotations.Queries({
		@javax.jdo.annotations.Query(name = "relationships_as_source", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE sourceEntityId == :sourceEntityId"),
		@javax.jdo.annotations.Query(name = "relationships_as_source_of_type", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE sourceEntityId == :sourceEntityId && type == :type"),
		@javax.jdo.annotations.Query(name = "relationships_as_target", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE targetEntityId == :targetEntityId"),
		@javax.jdo.annotations.Query(name = "relationships_as_target_of_type", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE targetEntityId == :targetEntityId && type == :type"),
		@javax.jdo.annotations.Query(name = "relationships_of_source_and_target_and_type", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE sourceEntityId == :sourceEntityId && targetEntityId == :targetEntityId && type == :type"),
		@javax.jdo.annotations.Query(name = "relationship_entity", language = "JDOQL", value = "SELECT FROM :class_name WHERE Id == :id"),
		@javax.jdo.annotations.Query(name = "relationships_as_target_of_type", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship WHERE targetEntityId == :targetEntityId && type == :type"),
		@javax.jdo.annotations.Query(name = "entity_by_id", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.Relationship e WHERE e.id == :id") })
@Uniques(value = { @Unique(name = "tenant_source_target_type", members = "tenantid, source, target, type") })
@Immutable
public class Relationship extends AbstractMultiTenantEntity {

	// {{ SourceEntity (property)

	// This is Not directly supported by DataNucleus. More info on JDO
	// documentation regarding
	// relationships with Entities with Inheritance strategy of "SUB_CLASS" and
	// more than 1 subclass. The following Exception is thrown:
	// org.datanucleus.exceptions.NucleusUserException: Relation
	// (com.xms.framework.architecture.domain.model.Relationship.sourceEntity)
	// with multiple related tables (using subclass-table). Not supported
	// private Entity sourceEntity;
	//
	@Hidden(where = Where.ALL_TABLES)
	// TODO: When this field is shown by the Isis Wicket Viewer a long UNION
	// SELECT is generated, searching for the name of the Entity. Perhaps it's
	// not needed to hide the Entity. But it failed before with an exception
	// (see previous comment).
	@MemberOrder(sequence = "10")
	// A Poymorphic Query is executed by the Wicket Viewer to obtain the Entity
	// Title.
	// <T extends Entity> does not show anything on the Viewer. Perhaps if the
	// Viewer obtained an instance of the "real" class (e.g., interpreting <T
	// extends Entity>) ...
	// As it will be shown only for Forms, when showing Lists of Relationships
	// on object forms no "bad" queries (with UNIONS) will be launched. Only 2
	// for the selected Relationship. It could be avoided if Entity would be a
	// JOIN table (@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE),
	// but that would require to insert a record on "Entity" each time any kind
	// of record is inserted. It can be a solution....
	public Entity getSourceEntity() throws ClassNotFoundException {
		return (Entity) relationshipRepository.relationshipEntity(
				Class.forName(getSourceEntityClassName()), getSourceEntityId());
	}

	@Hidden(where = Where.OBJECT_FORMS)
	public <T extends Entity> String getSourceEntityName()
			throws ClassNotFoundException {
		@SuppressWarnings("unchecked")
		T entity = (T) relationshipRepository.relationshipEntity(
				Class.forName(getSourceEntityClassName()), getSourceEntityId());
		return entity.getName();
	}

	@Hidden
	// This way no Polymorphic Query is used, but this link is not properly
	// represented by the Wicket viewer. Every Entity must have a
	public String getSourceEntityLink() throws ClassNotFoundException {
		Entity entity = (Entity) relationshipRepository.relationshipEntity(
				Class.forName(getSourceEntityClassName()), getSourceEntityId());

		return String
				.format("<a href=\"%s\">%s</a>",
						"http://localhost:8080/wicket/wicket/bookmarkable/org.apache.isis.viewer.wicket.ui.pages.entity.EntityPage?objectOid="
								+ entity.getDatabaseId()
								+ "&pageType=ENTITY&pageTitle="
								+ entity.getName(), entity.getName());
	}

	// }}

	// {{ SourceEntityId (property)
	private String sourceEntityId;

	@Hidden
	@MemberOrder(sequence = "1")
	public String getSourceEntityId() {
		return sourceEntityId;
	}

	public void setSourceEntityId(final String sourceEntityId) {
		this.sourceEntityId = sourceEntityId;
	}

	// }}

	// {{ SourceEntityClassName (property)
	private String sourceEntityClassName;

	@Hidden
	@MemberOrder(sequence = "1")
	public String getSourceEntityClassName() {
		return sourceEntityClassName;
	}

	public void setSourceEntityClassName(final String sourceEntityClassName) {
		this.sourceEntityClassName = sourceEntityClassName;
	}

	// }}

	// {{ TargetEntityId (property)
	private String targetEntityId;

	@Hidden
	@MemberOrder(sequence = "1")
	public String getTargetEntityId() {
		return targetEntityId;
	}

	public void setTargetEntityId(final String targetEntityId) {
		this.targetEntityId = targetEntityId;
	}

	@MemberOrder(sequence = "20")
	@Hidden(where = Where.ALL_TABLES)
	public Entity getTargetEntity() throws ClassNotFoundException {
		return (Entity) relationshipRepository.relationshipEntity(
				Class.forName(getTargetEntityClassName()), getTargetEntityId());
	}

	@Hidden(where = Where.OBJECT_FORMS)
	public <T extends Entity> String getTargetEntityName()
			throws ClassNotFoundException {
		@SuppressWarnings("unchecked")
		T entity = (T) relationshipRepository.relationshipEntity(
				Class.forName(getTargetEntityClassName()), getTargetEntityId());
		return entity.getName();
	}

	// }}

	// {{ targetEntityClassName (property)
	private String targetEntityClassName;

	@Hidden
	@MemberOrder(sequence = "1")
	public String getTargetEntityClassName() {
		return targetEntityClassName;
	}

	public void setTargetEntityClassName(final String targetEntityClassName) {
		this.targetEntityClassName = targetEntityClassName;
	}

	// }}

	// {{ Type (property)
	private RelationshipType type;

	@Disabled
	@MemberOrder(sequence = "1")
	public RelationshipType getType() {
		return type;
	}

	public void setType(final RelationshipType type) {
		this.type = type;
	}

	// }}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity
	 * #getName()
	 */
	// @Override
	// @NotPersisted
	// @NotPersistent
	// @Disabled
	// @Title
	// public String getName() {
	// try {
	// return new TitleBuffer().append(getSourceEntityName())
	// .append(" -> ", getTargetEntityName()).toString();
	// } catch (ClassNotFoundException e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}
	// }}

}
