package com.xms.framework.architecture.domain.model;

import org.apache.isis.applib.annotation.Hidden;
import org.apache.isis.applib.util.TitleBuffer;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class RelationshipFactory extends
		AbstractMultiTenantEntityRepositoryAndFactory {

	// Method must be hidden for avoiding creating Relationships between
	// elements that are not allowed by the current Archimate grammar. For
	// "Collaboration" entities there would be multiple inheritance
	// (ApplicationCollaboration is-an ApplicationComponent and a
	// Collaboration). As the easiest maintainable class is
	// ýCollaboration", and the class is on another package, this method must be declared "public".
	@Hidden
	public Relationship createRelationship(Entity sourceEntity,
			Entity targetEntity, RelationshipType relationshipType) {
		Relationship relationship = newTransientMultiTenantEntity(
				Relationship.class,
				null,
				new TitleBuffer().concat(sourceEntity.getName())
						.concat(" -> ", targetEntity.getName()).toString(),
				null);
		relationship.setSourceEntityId(sourceEntity.getId());
		relationship
				.setSourceEntityClassName(sourceEntity.getClass().getName());
		relationship.setTargetEntityId(targetEntity.getId());
		relationship
				.setTargetEntityClassName(targetEntity.getClass().getName());
		relationship.setType(relationshipType);

		persist(relationship);

		return relationship;
	}

	// public Relationship
	// createCompositionRelationship(@Named("Parent Entity - 'composed of' child -")
	// Entity parentEntity,
	// (@Named("Child Entity - 'composes' the parent -") Entity childEntity) {
	//
	// // Archimate:
	// //
	// "Every concept in the language can have composition, aggregation, and specialization relationships with concepts of the same type."
	// if (!(childEntity instanceof parentEntity)) {
	//
	// }
	// }

}
