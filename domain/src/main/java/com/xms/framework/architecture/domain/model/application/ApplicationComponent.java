package com.xms.framework.architecture.domain.model.application;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.ActiveStructureElement;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;
import com.xms.framework.architecture.domain.model.technology_new.InfrastructureInterface;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
@javax.jdo.annotations.Queries({ @javax.jdo.annotations.Query(name = "entity_by_id", language = "JDOQL", value = "SELECT FROM com.xms.framework.architecture.domain.model.application.ApplicationComponent e WHERE e.id == :id") })
public class ApplicationComponent extends ActiveStructureElement {

	// {{ ComposedOf

	@MemberOrder(name = "composedOf", sequence = "10")
	public void composedOfApplicationInterface(
			final ApplicationInterface applicationInterface) {
		super.addToComposedOf(applicationInterface);
	}

	@MemberOrder(name = "composedOf", sequence = "20")
	public void notComposedOfApplicationInterface(
			final ApplicationInterface applicationInterface) {
		super.removeFromComposedOf(applicationInterface);
	}

	// }}

	// {{ Uses

	@MemberOrder(name = "uses", sequence = "10")
	public void usesApplicationInterface(
			final ApplicationInterface applicationInterface) {
		super.usesInterface(applicationInterface);
	}

	@MemberOrder(name = "uses", sequence = "20")
	public void notUsesApplicationInterface(
			final ApplicationInterface applicationInterface) {
		super.notUsesInterface(applicationInterface);
	}

	@MemberOrder(name = "uses", sequence = "30")
	public void usesApplicationService(
			final ApplicationService applicationService) {
		super.usesService(applicationService);
	}

	@MemberOrder(name = "uses", sequence = "40")
	public void notUsesApplicationService(
			final ApplicationService applicationService) {
		super.notUsesService(applicationService);
	}

	@MemberOrder(name = "uses", sequence = "50")
	public void usesInfrastructureInterface(
			final InfrastructureInterface infrastructureInterface) {
		super.addToComposedOf(infrastructureInterface);
	}

	@MemberOrder(name = "uses", sequence = "60")
	public void notUsesInfrastructureInterface(
			final InfrastructureInterface infrastructureInterface) {
		super.removeFromComposedOf(infrastructureInterface);
	}

	// }}

	// {{ AssignedTo

	@MemberOrder(name = "assignedTo", sequence = "10")
	public void assignedToApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addAssignedTo(applicationFunction);
	}

	@MemberOrder(name = "assignedTo", sequence = "20")
	public void notAssignedToApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromAssignedTo(applicationFunction);
	}

	//
	// public String validateAssignedToApplicationFunction(
	// ApplicationFunction applicationFunction) {
	// if ((applicationFunction instanceof ApplicationInteraction)
	// && !(this instanceof ApplicationCollaboration)) {
	// return
	// "An Application Interaction can only be assigned to Application Collaborations, not to usual Application Components";
	// }
	// return null;
	// }

	// }}

	// {{ AggregatedBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAggregatedBy() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.AGGREGATION);
	}

	/**
	 * @param applicationCollaboration
	 */
	@MemberOrder(name = "aggregatedBy", sequence = "10")
	public void aggregatedByApplicationCollaboration(
			ApplicationCollaboration applicationCollaboration) {
		applicationCollaboration.addToAggregates(this);
	}

	/**
	 * @param applicationCollaboration
	 */
	@MemberOrder(name = "aggregatedBy", sequence = "20")
	public void notAggregatedByApplicationCollaboration(
			ApplicationCollaboration applicationCollaboration) {
		applicationCollaboration.removeFromAggregates(this);
	}

	// }}

}
