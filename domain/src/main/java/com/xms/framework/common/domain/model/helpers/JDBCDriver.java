package com.xms.framework.common.domain.model.helpers;

public enum JDBCDriver {

	 ORACLE {//ojdbc6.jar
		@Override
		public String getDriver() {
			return "oracle.jdbc.driver.OracleDriver";
		}
		
		@Override
		public String getConnectionString() {//1521
			return "jdbc:oracle:thin:@<HOST>:<PORT>:<SID>";
		}
	},
	INFORMIX {//informix.jar
		@Override
		public String getDriver() {
			return "com.informix.jdbc.IfxDriver";
		}
		
		@Override
		public String getConnectionString() {//1533
			return "jdbc:informix-sqli://<HOST>:<PORT>/<DB>:INFORMIXSERVER=<SERVER_NAME>";
		}
	},
	MICROSOFT_SQL_SERVER_MICROSOFT_DRIVER {//sqljdbc.jar
		@Override
		public String getDriver() {
			return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		}
		
		@Override
		public String getConnectionString() {//1433
			return "jdbc:sqlserver://<HOST>:<PORT>;databaseName=<DB>";
		}
	},
	MYSQL {//mysql-connector-java.jar
		@Override
		public String getDriver() {
			return "com.mysql.jdbc.Driver";
		}
		
		@Override
		public String getConnectionString() {//3306
			return "jdbc:mysql://<HOST>:<PORT>/<DB>";
		}
	},
	POSTGRESQL {//postgresql.jar
		@Override
		public String getDriver() {
			return "org.postgresql.Driver";
		}
		
		@Override
		public String getConnectionString() {//5432
			return "jdbc:postgresql://<HOST>:<PORT>/<DB>";
		}
	},
	DB2 {//db2jcc.jar
		@Override
		public String getDriver() {
			return "com.ibm.db2.jcc.DB2Driver";
		}
		
		@Override
		public String getConnectionString() {//50000
			return "jdbc:db2://<HOST>:<PORT>/<DB>";
		}
	},
	FIREBIRD {//jaybird.jar
		@Override
		public String getDriver() {
			return "org.firebirdsql.jdbc.FBDriver";
		}
		
		@Override
		public String getConnectionString() {//3050
			return "jdbc:firebirdsql:[//<HOST>[:<PORT>]/]<DB>";
		}
	},
	HYPERSQL {//hsqldb.jar
		@Override
		public String getDriver() {
			return "org.hsqldb.jdbc.JDBCDriver";
		}
		
		@Override
		public String getConnectionString() {
			return "jdbc:hsqldb:hsql://<DB><HOST>/<DB>";
		}
	},
	NOSQL_MONGODB {//mongo-2.10.1.jar
		@Override
		public String getDriver() {
			return "com.mongodb.jdbc.MongoDriver";
		}
		
		@Override
		public String getConnectionString() {//3050
			return "mongodb://<HOST>/<DB>";
		}
	},
	ALIENVAULT_LOGGER {//mongo-2.10.1.jar
		@Override
		public String getDriver() {
			return "com.mongodb.jdbc.MongoDriver";
		}
		
		@Override
		public String getConnectionString() {//3050
			return "mongodb://<HOST>/<DB>";
		}
	},
	NOSQL_NEO4J {//neo4j-jdbc-1.8.jar
		@Override
		public String getDriver() {
			return "org.neo4j.jdbc.Driver";
		}
		
		@Override
		public String getConnectionString() {//3050
			return "jdbc:neo4j://<HOST>:<PORT>/";
		}
	},;
	
	
	public abstract String getDriver();
	
	public abstract String getConnectionString();
}