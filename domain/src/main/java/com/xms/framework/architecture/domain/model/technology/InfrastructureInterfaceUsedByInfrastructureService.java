package com.xms.framework.architecture.domain.model.technology;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.Disabled;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;
import org.apache.isis.applib.annotation.Title;

import com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class InfrastructureInterfaceUsedByInfrastructureService extends
		AbstractMultiTenantUnnamedEntity {

	// {{ InfrastructureInterface (property)
	private InfrastructureInterface infrastructureInterface;

	@MemberOrder(sequence = "1")
	public InfrastructureInterface getInfrastructureInterface() {
		return infrastructureInterface;
	}

	public void setInfrastructureInterface(
			final InfrastructureInterface infrastructureInterface) {
		this.infrastructureInterface = infrastructureInterface;
	}

	public void modifyInfrastructureInterface(
			final InfrastructureInterface infrastructureInterface) {
		InfrastructureInterface currentInfrastructureInterface = getInfrastructureInterface();
		// check for no-op
		if (infrastructureInterface == null
				|| infrastructureInterface
						.equals(currentInfrastructureInterface)) {
			return;
		}
		// associate new
		setInfrastructureInterface(infrastructureInterface);
		// additional business logic
		onModifyInfrastructureInterface(currentInfrastructureInterface,
				infrastructureInterface);
	}

	public void clearInfrastructureInterface() {
		InfrastructureInterface currentInfrastructureInterface = getInfrastructureInterface();
		// check for no-op
		if (currentInfrastructureInterface == null) {
			return;
		}
		// dissociate existing
		setInfrastructureInterface(null);
		// additional business logic
		onClearInfrastructureInterface(currentInfrastructureInterface);
	}

	protected void onModifyInfrastructureInterface(
			final InfrastructureInterface oldInfrastructureInterface,
			final InfrastructureInterface newInfrastructureInterface) {
	}

	protected void onClearInfrastructureInterface(
			final InfrastructureInterface oldInfrastructureInterface) {
	}

	// }}

	// {{ InfrastructureService (property)
	private InfrastructureService infrastructureService;

	@MemberOrder(sequence = "1")
	public InfrastructureService getInfrastructureService() {
		return infrastructureService;
	}

	public void setInfrastructureService(
			final InfrastructureService infrastructureService) {
		this.infrastructureService = infrastructureService;
	}

	public void modifyInfrastructureService(
			final InfrastructureService infrastructureService) {
		InfrastructureService currentInfrastructureService = getInfrastructureService();
		// check for no-op
		if (infrastructureService == null
				|| infrastructureService.equals(currentInfrastructureService)) {
			return;
		}
		// associate new
		setInfrastructureService(infrastructureService);
		// additional business logic
		onModifyInfrastructureService(currentInfrastructureService,
				infrastructureService);
	}

	public void clearInfrastructureService() {
		InfrastructureService currentInfrastructureService = getInfrastructureService();
		// check for no-op
		if (currentInfrastructureService == null) {
			return;
		}
		// dissociate existing
		setInfrastructureService(null);
		// additional business logic
		onClearInfrastructureService(currentInfrastructureService);
	}

	protected void onModifyInfrastructureService(
			final InfrastructureService oldInfrastructureService,
			final InfrastructureService newInfrastructureService) {
	}

	protected void onClearInfrastructureService(
			final InfrastructureService oldInfrastructureService) {
	}

	// }}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity
	 * #getName()
	 */
	@Override
	@NotPersisted
	@NotPersistent
	@Disabled
	@Title
	public String getName() {
		return getInfrastructureInterface().getName().concat(" -> ")
				.concat(getInfrastructureService().getName());
	}

}
