package com.xms.framework.party.domain.model.organization;

import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.party.domain.model.Organization;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Unidad de Negocio"),
		@XMSEntityName(locale = "en", name = "Business Unit") })
//@Entity
@PersistenceCapable
public class BusinessUnit extends Organization {

	@ManyToOne
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Unidad de Negocio Padre"),
			@XMSLocale(locale = "en", caption = "Parent Business Unit") })
	private BusinessUnit parentBusinessUnit;

	@OneToMany(mappedBy = "parentBusinessUnit", cascade = CascadeType.REMOVE)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Unidades de Negocio Hijas"),
			@XMSLocale(locale = "en", caption = "Child Business Units") })
	private Set<BusinessUnit> childBusinessUnits;

	public BusinessUnit getParentBusinessUnit() {
		return parentBusinessUnit;
	}

	public void setParentBusinessUnit(BusinessUnit parentBusinessUnit) {
		this.parentBusinessUnit = parentBusinessUnit;
	}

	public Set<BusinessUnit> getChildBusinessUnits() {
		return childBusinessUnits;
	}

	public void setChildBusinessUnits(Set<BusinessUnit> childBusinessUnits) {
		this.childBusinessUnits = childBusinessUnits;
	}

}
