package com.xms.framework.project.api.domain;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Project extends AbstractMultiTenantEntity {

	@OneToOne
	private Task rootTask;

	public Task getRootTask() {
		return rootTask;
	}

	public void setRootTask(Task rootTask) {
		this.rootTask = rootTask;
	}

}
