package com.xms.framework.performance.domain.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
//@Entity
@PersistenceCapable
public class ObjectiveIndicator extends AbstractMultiTenantEntityRelationship {

	@ManyToOne(optional = false)
	private Objective objective;

	// @ManyToOne(optional = false)
	// private Indicator relatedIndicator;

	public Objective getObjective() {
		return objective;
	}

	public void setObjective(Objective objective) {
		this.objective = objective;
	}

	// public Indicator getRelatedIndicator() {
	// return relatedIndicator;
	// }
	//
	// public void setRelatedIndicator(Indicator relatedIndicator) {
	// this.relatedIndicator = relatedIndicator;
	// }

}
