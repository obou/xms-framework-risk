package com.xms.framework.common.domain.model.helpers;

/**
 * Scale Level definition that provides both a numerical value and a Bin.
 * 
 * 
 * <b>NIST SP 800-30 rev1</b>: The bins (e.g., 0-15, 16-35, 35-70, 71-85,
 * 86-100) or scales (e.g., 1-10) translate easily into qualitative terms that
 * support risk communications for decision makers (e.g., a score of 95 can be
 * interpreted as very high), while also allowing relative comparisons between
 * values in different bins or even within the same bin (e.g., the difference
 * between risks scored 70 and 71 respectively is relatively insignificant,
 * while the difference between risks scored 35 and 70 is relatively
 * significant).
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 * 
 * @param <S>
 *            Scale this Level is related to.
 * @param <V>
 *            Type used to express the Scale Level value.
 * @param <L>
 *            Type used to express the Bin limits.
 */
public interface ScaleLevelBin<S extends Scale<?, ?>, V extends Number> extends ScaleLevel<S, V> {

	/**
	 * Lower value of the bin represented by this Scale Level.
	 */
	public V getLowerLimit();

	/**
	 * Upper value of the bin represented by this Scale Level.
	 */
	public V getUpperLimit();

}
