package com.xms.framework.party.domain.model.organization;

import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

public enum CompetencyType {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Habilidad"),
			@XMSLocale(locale = "en", caption = "Skill") })
	SKILL, @XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Educación"),
			@XMSLocale(locale = "en", caption = "Education") })
	EDUCATION, @XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Experiencia"),
			@XMSLocale(locale = "en", caption = "Experience") })
	EXPERIENCE, @XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Formación"),
			@XMSLocale(locale = "en", caption = "Training") })
	TRAINING
}
