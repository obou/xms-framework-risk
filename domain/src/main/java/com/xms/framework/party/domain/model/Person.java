package com.xms.framework.party.domain.model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Persona"),
		@XMSEntityName(locale = "en", name = "Person") })
//@Entity
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Person extends Party {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Nombre"),
			@XMSLocale(locale = "en", caption = "First Name") })
	private String firstName;

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Segundo Nombre"),
			@XMSLocale(locale = "en", caption = "Middle Name") })
	private String middleName;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Apellidos"),
			@XMSLocale(locale = "en", caption = "Last Name") })
	private String lastName;

	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Nombre de Pila"),
			@XMSLocale(locale = "en", caption = "Preferred Name") })
	private String preferredName;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Prefijo"),
			@XMSLocale(locale = "en", caption = "Prefix") })
	private String prefix;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Sufijo"),
			@XMSLocale(locale = "en", caption = "Suffix") })
	private String suffix;

	@Override
	public void setName(String name) {
		super.setName(name);
		// If comma is found, the name will be decomposed
		// following the format "LastName, FirstName".
		if (name.contains(",")) {

			String[] names = name.split(", ");

			this.setFirstName(names[1]);
			this.setLastName(names[0]);

		} else {

			// The format expected will be "FirstName LastName LastName".
			Integer spaceIndex = name.indexOf(" ");

			if (spaceIndex == -1) {
				this.setFirstName(name);
				this.setLastName("");
			} else {
				this.setFirstName(name.substring(0, spaceIndex));
				this.setLastName(name.substring(spaceIndex + 1, name.length()));
			}
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

}
