package com.xms.framework.architecture.domain.model.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.xms.framework.architecture.domain.model.application.ApplicationCollaboration;
import com.xms.framework.architecture.domain.model.application.ApplicationComponent;
import com.xms.framework.architecture.domain.model.application.ApplicationFunction;
import com.xms.framework.architecture.domain.model.application.ApplicationInteraction;
import com.xms.framework.architecture.domain.model.application.ApplicationInterface;
import com.xms.framework.architecture.domain.model.application.ApplicationService;
import com.xms.framework.architecture.domain.model.application.DataObject;
import com.xms.framework.architecture.domain.model.application.tests.ApplicationLayerTest;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
public class EnterpriseArchitectureDomainModelTests extends
		ApplicationLayerTest {

	@Test
	public void testActiveStructureElementComposedOfInterface() {

		// Direct relationship.
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("active structure element", null);
		ApplicationInterface applicationInterface = getApplicationInterfaceFactory()
				.createApplicationInterface("interface", null);

		assertTrue(applicationComponent.getComposedOf().size() == 0);
		assertTrue(applicationInterface.getComposes().size() == 0);
		applicationComponent
				.composedOfApplicationInterface(applicationInterface);
		assertTrue(applicationComponent.getComposedOf().size() == 1);
		assertTrue(applicationInterface.getComposes().size() == 1);

		// Direct Remove.
		applicationComponent
				.notComposedOfApplicationInterface(applicationInterface);
		assertTrue(applicationComponent.getComposedOf().size() == 0);
		assertTrue(applicationInterface.getComposes().size() == 0);

		// Inverse relationship.
		applicationInterface.composesApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getComposedOf().size() == 1);
		assertTrue(applicationInterface.getComposes().size() == 1);

		// Inverse Remove.
		applicationInterface
				.notComposesApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getComposedOf().size() == 0);
		assertTrue(applicationInterface.getComposes().size() == 0);

	}

	@Test
	public void testInterfaceUsedByActiveStructureElement() {

		// Direct relationship.
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("active structure element", null);
		ApplicationInterface applicationInterface = getApplicationInterfaceFactory()
				.createApplicationInterface("interface", null);

		assertTrue(applicationComponent.getUses().size() == 0);
		assertTrue(applicationInterface.getUsedBy().size() == 0);
		applicationInterface.usedByApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getUses().size() == 1);
		assertTrue(applicationInterface.getUsedBy().size() == 1);

		// Direct remove.
		applicationInterface
				.notUsedByApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getUses().size() == 0);
		assertTrue(applicationInterface.getUsedBy().size() == 0);

		// Inverse relationship.
		applicationComponent.usesApplicationInterface(applicationInterface);
		assertTrue(applicationComponent.getUses().size() == 1);
		assertTrue(applicationInterface.getUsedBy().size() == 1);

		// Inverse remove.
		applicationComponent.notUsesApplicationInterface(applicationInterface);
		assertTrue(applicationComponent.getUses().size() == 0);
		assertTrue(applicationInterface.getUsedBy().size() == 0);

	}

	@Test
	public void testInterfaceAssignedToService() {

		// Direct relationship.
		ApplicationService applicationService = getApplicationServiceFactory()
				.createApplicationService("service", null);
		ApplicationInterface applicationInterface = getApplicationInterfaceFactory()
				.createApplicationInterface("interface", null);

		assertTrue(applicationService.getAssignedFrom().size() == 0);
		assertTrue(applicationInterface.getAssignedTo().size() == 0);
		applicationInterface.assignedToApplicationService(applicationService);
		assertTrue(applicationService.getAssignedFrom().size() == 1);
		assertTrue(applicationInterface.getAssignedTo().size() == 1);

		// Direct remove.
		applicationInterface
				.notAssignedToApplicationService(applicationService);
		assertTrue(applicationService.getAssignedFrom().size() == 0);
		assertTrue(applicationInterface.getAssignedTo().size() == 0);

		// Inverse relationship.
		applicationService
				.assignedFromApplicationInterface(applicationInterface);
		assertTrue(applicationService.getAssignedFrom().size() == 1);
		assertTrue(applicationInterface.getAssignedTo().size() == 1);

		// Inverse remove.
		applicationService
				.notAssignedFromApplicationInterface(applicationInterface);
		assertTrue(applicationService.getAssignedFrom().size() == 0);
		assertTrue(applicationInterface.getAssignedTo().size() == 0);

	}

	@Test
	public void testServiceUsedByActiveStructureElement() {

		// Direct relationship.
		ApplicationService applicationService = getApplicationServiceFactory()
				.createApplicationService("service", null);
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("active structure element", null);

		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationComponent.getUses().size() == 0);
		applicationService.usedByApplicationComponent(applicationComponent);
		assertTrue(applicationService.getUsedBy().size() == 1);
		assertTrue(applicationComponent.getUses().size() == 1);

		// Direct remove.
		applicationService.notUsedByApplicationComponent(applicationComponent);
		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationComponent.getUses().size() == 0);

		// Inverse relationship.
		applicationComponent.usesApplicationService(applicationService);
		assertTrue(applicationService.getUsedBy().size() == 1);
		assertTrue(applicationComponent.getUses().size() == 1);

		// Inverse remove.
		applicationComponent.notUsesApplicationService(applicationService);
		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationComponent.getUses().size() == 0);

	}

	@Test
	public void testBehaviorElementRealizesService() {

		// Direct relationship.
		ApplicationService applicationService = getApplicationServiceFactory()
				.createApplicationService("service", null);
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("behavior", null);

		assertTrue(applicationService.getRealizedBy().size() == 0);
		assertTrue(applicationFunction.getRealizes().size() == 0);
		applicationFunction.realizesApplicationService(applicationService);
		assertTrue(applicationService.getRealizedBy().size() == 1);
		assertTrue(applicationFunction.getRealizes().size() == 1);

		// Direct remove.
		applicationFunction.notRealizesApplicationService(applicationService);
		assertTrue(applicationService.getRealizedBy().size() == 0);
		assertTrue(applicationFunction.getRealizes().size() == 0);

		// Inverse relationship.
		applicationService.realizedByApplicationFunction(applicationFunction);
		assertTrue(applicationService.getRealizedBy().size() == 1);
		assertTrue(applicationFunction.getRealizes().size() == 1);

		// Inverse remove.
		applicationService
				.notRealizedByApplicationFunction(applicationFunction);
		assertTrue(applicationService.getRealizedBy().size() == 0);
		assertTrue(applicationFunction.getRealizes().size() == 0);

	}

	@Test
	public void testBehaviorElementAccessesDataObject() {

		// Direct relationship.
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("behavior", null);
		DataObject dataObject = getDataObjectFactory().createDataObject(
				"data object", null);

		assertTrue(applicationFunction.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);
		applicationFunction.accessesDataObject(dataObject);
		assertTrue(applicationFunction.getAccesses().size() == 1);
		assertTrue(dataObject.getAccessedBy().size() == 1);

		// Direct remove.
		applicationFunction.notAccessesDataObject(dataObject);
		assertTrue(applicationFunction.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);

		// Inverse relationship.
		dataObject.accessedByApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getAccesses().size() == 1);
		assertTrue(dataObject.getAccessedBy().size() == 1);

		// Inverse remove.
		dataObject.notAccessedByApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);

	}

	@Test
	public void testBehaviorElementTriggersBehaviorElement() {

		// Direct relationship.
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("behavior", null);
		ApplicationFunction applicationFunction2 = getApplicationFunctionFactory()
				.createApplicationFunction("behavior2", null);

		assertTrue(applicationFunction.getTriggers().size() == 0);
		assertTrue(applicationFunction2.getTriggeredBy().size() == 0);
		applicationFunction.triggersApplicationFunction(applicationFunction2);
		assertTrue(applicationFunction.getTriggers().size() == 1);
		assertTrue(applicationFunction2.getTriggeredBy().size() == 1);

		// Direct remove.
		applicationFunction
				.notTriggersApplicationFunction(applicationFunction2);
		assertTrue(applicationFunction.getTriggers().size() == 0);
		assertTrue(applicationFunction2.getTriggeredBy().size() == 0);

		// Inverse relationship.
		applicationFunction2
				.triggeredByApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getTriggers().size() == 1);
		assertTrue(applicationFunction2.getTriggeredBy().size() == 1);

		// Inverse remove.
		applicationFunction2
				.notTriggeredByApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getTriggers().size() == 0);
		assertTrue(applicationFunction2.getTriggeredBy().size() == 0);

	}

	@Test
	public void testBehaviorElementFlowToBehaviorElement() {

		// Direct relationship.
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("behavior", null);
		ApplicationFunction applicationFunction2 = getApplicationFunctionFactory()
				.createApplicationFunction("behavior2", null);

		assertTrue(applicationFunction.getFlowTo().size() == 0);
		assertTrue(applicationFunction2.getFlowFrom().size() == 0);
		applicationFunction.flowToApplicationFunction(applicationFunction2);
		assertTrue(applicationFunction.getFlowTo().size() == 1);
		assertTrue(applicationFunction2.getFlowFrom().size() == 1);

		// Direct remove.
		applicationFunction.notFlowToApplicationFunction(applicationFunction2);
		assertTrue(applicationFunction.getFlowTo().size() == 0);
		assertTrue(applicationFunction2.getFlowFrom().size() == 0);

		// Inverse relationship.
		applicationFunction2.flowFromApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getFlowTo().size() == 1);
		assertTrue(applicationFunction2.getFlowFrom().size() == 1);

		// Inverse remove.
		applicationFunction2
				.notFlowFromApplicationFunction(applicationFunction);
		assertTrue(applicationFunction.getFlowTo().size() == 0);
		assertTrue(applicationFunction2.getFlowFrom().size() == 0);

	}

	@Test
	public void testServiceUsedByBehaviorElement() {

		// Direct relationship.
		ApplicationService applicationService = getApplicationServiceFactory()
				.createApplicationService("service", null);
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("behavior element", null);

		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationFunction.getUses().size() == 0);
		applicationService.usedByApplicationFunction(applicationFunction);
		assertTrue(applicationService.getUsedBy().size() == 1);
		assertTrue(applicationFunction.getUses().size() == 1);

		// Direct remove.
		applicationService.notUsedByApplicationFunction(applicationFunction);
		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationFunction.getUses().size() == 0);

		// Inverse relationship.
		applicationFunction.usesApplicationService(applicationService);
		assertTrue(applicationService.getUsedBy().size() == 1);
		assertTrue(applicationFunction.getUses().size() == 1);

		// Inverse remove.
		applicationFunction.notUsesApplicationService(applicationService);
		assertTrue(applicationService.getUsedBy().size() == 0);
		assertTrue(applicationFunction.getUses().size() == 0);

	}

	@Test
	public void testServiceAccessesDataObject() {
		// Direct relationship.
		ApplicationService applicationService = getApplicationServiceFactory()
				.createApplicationService("service", null);
		DataObject dataObject = getDataObjectFactory().createDataObject(
				"data object", null);

		assertTrue(applicationService.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);
		applicationService.accessesDataObject(dataObject);
		assertTrue(applicationService.getAccesses().size() == 1);
		assertTrue(dataObject.getAccessedBy().size() == 1);

		// Direct remove.
		applicationService.notAccessesDataObject(dataObject);
		assertTrue(applicationService.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);

		// Inverse relationship.
		dataObject.accessedByApplicationService(applicationService);
		assertTrue(applicationService.getAccesses().size() == 1);
		assertTrue(dataObject.getAccessedBy().size() == 1);

		// Inverse remove.
		dataObject.notAccessedByApplicationService(applicationService);
		assertTrue(applicationService.getAccesses().size() == 0);
		assertTrue(dataObject.getAccessedBy().size() == 0);
	}

	@Test
	public void testActiveStructureElementAssignedToBehaviorElement() {

		// Direct relationship.
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("active structure element", null);
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("function", null);

		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);
		applicationComponent.assignedToApplicationFunction(applicationFunction);
		assertTrue(applicationComponent.getAssignedTo().size() == 1);
		assertTrue(applicationFunction.getAssignedFrom().size() == 1);

		// Direct Remove.
		applicationComponent
				.notAssignedToApplicationFunction(applicationFunction);
		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);

		// Inverse relationship.
		applicationFunction
				.assignedFromApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getAssignedTo().size() == 1);
		assertTrue(applicationFunction.getAssignedFrom().size() == 1);

		// Inverse Remove.
		applicationFunction
				.notAssignedFromApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);
	}

	@Test
	public void testCollaborationAggregatesActiveStructureElement() {

		ApplicationCollaboration applicationCollaboration = getApplicationCollaborationFactory()
				.createApplicationCollaboration("collabotation", null);
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("component1", null);

		// Direct relationship.
		assertTrue(applicationCollaboration.getAggregates().size() == 0);
		assertTrue(applicationComponent.getAggregatedBy().size() == 0);
		applicationCollaboration
				.aggregatesApplicationComponent(applicationComponent);
		assertTrue(applicationCollaboration.getAggregates().size() == 1);
		assertTrue(applicationComponent.getAggregatedBy().size() == 1);

		// Direct remove.
		applicationCollaboration
				.notAggregatesApplicationComponent(applicationComponent);
		assertTrue(applicationCollaboration.getAggregates().size() == 0);
		assertTrue(applicationComponent.getAggregatedBy().size() == 0);

		// Inverse relationship.
		applicationComponent
				.aggregatedByApplicationCollaboration(applicationCollaboration);
		assertTrue(applicationCollaboration.getAggregates().size() == 1);
		assertTrue(applicationComponent.getAggregatedBy().size() == 1);

		// Inverse remove.
		applicationComponent
				.notAggregatedByApplicationCollaboration(applicationCollaboration);
		assertTrue(applicationCollaboration.getAggregates().size() == 0);
		assertTrue(applicationComponent.getAggregatedBy().size() == 0);

	}

	@Test
	public void ActiveStructureElementAssignedToBehaviorElement() {

		// Direct relationship.
		ApplicationComponent applicationComponent = getApplicationComponentFactory()
				.createApplicationComponent("active structure element", null);
		ApplicationFunction applicationFunction = getApplicationFunctionFactory()
				.createApplicationFunction("interface", null);

		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);
		applicationComponent.assignedToApplicationFunction(applicationFunction);
		assertTrue(applicationComponent.getAssignedTo().size() == 1);
		assertTrue(applicationFunction.getAssignedFrom().size() == 1);

		// Direct Remove.
		applicationComponent
				.notAssignedToApplicationFunction(applicationFunction);
		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);

		// Inverse relationship.
		applicationFunction
				.assignedFromApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getAssignedTo().size() == 1);
		assertTrue(applicationFunction.getAssignedFrom().size() == 1);

		// Inverse Remove.
		applicationFunction
				.notAssignedFromApplicationComponent(applicationComponent);
		assertTrue(applicationComponent.getAssignedTo().size() == 0);
		assertTrue(applicationFunction.getAssignedFrom().size() == 0);

	}

	@Test
	public void testInteractionAssignedFromCollaboration() {

		ApplicationCollaboration applicationCollaboration = getApplicationCollaborationFactory()
				.createApplicationCollaboration("collabotation", null);
		ApplicationInteraction applicationInteraction = getApplicationInteractionFactory()
				.createApplicationInteraction("interaction", null);

		// Direct relationship.
		assertTrue(applicationCollaboration.getAssignedTo().size() == 0);
		assertTrue(applicationInteraction.getAssignedFrom().size() == 0);
		applicationCollaboration
				.assignedToApplicationInteraction(applicationInteraction);
		assertTrue(applicationCollaboration.getAssignedTo().size() == 1);
		assertTrue(applicationInteraction.getAssignedFrom().size() == 1);

		// Direct remove.
		applicationCollaboration
				.notAssignedToApplicationInteraction(applicationInteraction);
		assertTrue(applicationCollaboration.getAssignedTo().size() == 0);
		assertTrue(applicationInteraction.getAssignedFrom().size() == 0);

		// Inverse relationship.
		applicationInteraction
				.assignedFromApplicationComponent(applicationCollaboration);
		assertTrue(applicationCollaboration.getAssignedTo().size() == 1);
		assertTrue(applicationInteraction.getAssignedFrom().size() == 1);

		// Inverse remove.
		applicationInteraction
				.notAssignedFromApplicationComponent(applicationCollaboration);
		assertTrue(applicationCollaboration.getAssignedTo().size() == 0);
		assertTrue(applicationInteraction.getAssignedFrom().size() == 0);

	}

	@Test(expected = Exception.class)
	public void testApplicationComponentAssignedToApplicationInteractionThrowsException() {

		ApplicationComponent applicationComponent = wrapped(getApplicationComponentFactory()
				.createApplicationComponent("component", null));
		ApplicationInteraction applicationInteraction = getApplicationInteractionFactory()
				.createApplicationInteraction("interaction", null);

		// Direct relationship.
		applicationComponent
				.assignedToApplicationFunction(applicationInteraction);

		getDomainObjectContainer().validate(applicationComponent);

		// Inverse relationship.

	}

	@Test
	public void testInteractionAssignedFromNotACollaborationActiveStructureElementThrowsExceptionInverse() {

		ApplicationComponent applicationComponent = wrapped(getApplicationComponentFactory()
				.createApplicationComponent("component", null));
		ApplicationInteraction applicationInteraction = getApplicationInteractionFactory()
				.createApplicationInteraction("interaction", null);

		// Inverse relationship.
		applicationInteraction
				.assignedFromApplicationComponent(applicationComponent);

		validateCommonAssertions(applicationInteraction);
		validateCommonAssertions(applicationComponent);

	}

	@Test
	@org.junit.Ignore
	public void testEntityNameUpdatedUpdatesRelationshipEntityName() {

		fail("pending");

	}
}
