package com.xms.framework.architecture.domain.model.business;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.ActiveStructureElement;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class BusinessActor extends ActiveStructureElement {

	// {{ Uses

	@NotPersisted
	@NotPersistent
	@MemberOrder(sequence = "1")
	public List<Relationship> getUses() {

		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.USED_BY);
	}

	protected void usesBusinessInterface(
			final BusinessInterface businessInterface) {

		// Is the inverse relationship.
		businessInterface.usedByBusinessActor(this);

	}

	protected void notUsesBusinessInterface(
			final BusinessInterface businessInterface) {

		// Is the inverse relationship.
		businessInterface.notUsedByBusinessActor(this);

	}

	protected void usesBusinessService(final BusinessService businessService) {

		// Is the inverse relationship.
		businessService.usedByBusinessActor(this);

	}

	protected void notUsesBusinessService(final BusinessService businessService) {

		// Is the inverse relationship.
		businessService.notUsedByBusinessActor(this);

	}

	// }}

}
