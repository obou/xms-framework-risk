package com.xms.framework.architecture.domain.model.application;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

import com.xms.framework.architecture.domain.model.Interaction;
import com.xms.framework.architecture.domain.model.Relationship;
import com.xms.framework.architecture.domain.model.RelationshipType;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class ApplicationInteraction extends ApplicationFunction implements
		Interaction {

	// {{ AssignedFrom

	/**
	 * @return
	 */
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssignedFrom() {
		return getRelationshipRepository().relationshipsAsTargetOfType(this,
				RelationshipType.ASSIGNMENT);
	}

	protected void addToAssignedFrom(
			ApplicationCollaboration applicationCollaboration) {
		applicationCollaboration.assignedToApplicationInteraction(this);
	}

	protected void removeFromAssignedFrom(
			ApplicationCollaboration applicationCollaboration) {
		applicationCollaboration.notAssignedToApplicationInteraction(this);
	}

	// public String validateAssignedFromApplicationComponent(
	// ApplicationComponent applicationComponent) {
	// if (!(applicationComponent instanceof ApplicationCollaboration)) {
	// return
	// "An Application Interaction can only be assigned to Application Collaborations, not to usual Application Components";
	// }
	// return null;
	// }

	// }}

}
