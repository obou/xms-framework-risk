package com.xms.framework.party.domain.model.organization;

import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Equipo"),
		@XMSEntityName(locale = "en", name = "Team") })
//@Entity
@PersistenceCapable
public class Team extends AbstractMultiTenantEntity {

	@OneToMany(mappedBy = "team", cascade = CascadeType.REMOVE)
	@XMSField(locales = {
			@XMSLocale(locale = "es", caption = "Miembros del Equipo"),
			@XMSLocale(locale = "en", caption = "Team Members") })
	private Set<TeamMember> teamMembers;

	public Set<TeamMember> getTeamMembers() {
		return teamMembers;
	}

	public void setTeamMembers(Set<TeamMember> teamMembers) {
		this.teamMembers = teamMembers;
	}

}
