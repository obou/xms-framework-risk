package com.xms.framework.party.domain.model.address;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Dirección"),
		@XMSEntityName(locale = "en", name = "Address") })
//@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
@PersistenceCapable
@javax.jdo.annotations.Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class Address extends AbstractMultiTenantEntity {

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Uso"),
			@XMSLocale(locale = "en", caption = "Use") })
	private String use;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Válido desde"),
			@XMSLocale(locale = "en", caption = "Valid from") }, required = true)
	@Column(nullable = false)
	private Date validFrom = new Date();

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Válido hasta"),
			@XMSLocale(locale = "en", caption = "Valid to") }, required = true)
	@Column(nullable = false)
	private Date validTo = new GregorianCalendar(2099, 12, 31).getTime();

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

}
