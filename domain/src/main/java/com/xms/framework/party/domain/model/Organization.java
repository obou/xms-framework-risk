package com.xms.framework.party.domain.model;

import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;

@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Organización"),
		@XMSEntityName(locale = "en", name = "Organization") })
//@Entity
@PersistenceCapable
public class Organization extends Party {

	public Organization() {
	}

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Organización Matriz") })
	@ManyToOne
	private Organization parentOrganization;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Miembros (Personas que forman parte de la misma)") })
	@OneToMany(mappedBy = "organization")
	private Set<OrganizationMember> members;

	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Organizaciones Hijas") })
	@OneToMany(mappedBy = "parentOrganization")
	private Set<Organization> childOrganizations;

	public Organization getParentOrganization() {
		return parentOrganization;
	}

	public void setParentOrganization(Organization parentOrganization) {
		this.parentOrganization = parentOrganization;
	}

	public Set<Organization> getChildOrganizations() {
		return childOrganizations;
	}

	public void setChildOrganizations(Set<Organization> childOrganizations) {
		this.childOrganizations = childOrganizations;
	}

	public Set<OrganizationMember> getMembers() {
		return members;
	}

	public void setMembers(Set<OrganizationMember> members) {
		this.members = members;
	}

}
