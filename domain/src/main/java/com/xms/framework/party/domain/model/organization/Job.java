package com.xms.framework.party.domain.model.organization;

import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.xms.framework.annotation.XMSEntityName;
import com.xms.framework.annotation.XMSEntityNames;
import com.xms.framework.annotation.XMSField;
import com.xms.framework.annotation.XMSLocale;
import com.xms.framework.common.domain.model.AbstractMultiTenantEntity;
import com.xms.framework.party.domain.model.Organization;

/**
 * Job models the nature of work done. Job describes a Position . Job defines
 * what work that particular position has to do.
 * 
 * 
 * @author <p>
 *         Copyright © 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@XMSEntityNames({ @XMSEntityName(locale = "es", name = "Puesto de Trabajo"),
		@XMSEntityName(locale = "en", name = "Job") })
//@Entity
@PersistenceCapable
public class Job extends AbstractMultiTenantEntity {

	/**
	 * Jobs are specific to a Organization.
	 * <p>
	 * Roles can be shared between Organizations.
	 */
	@ManyToOne(optional = false)
	private Organization organization;

	@Column(nullable = false)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Título"),
			@XMSLocale(locale = "en", caption = "Title") })
	private String title;

	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Roles"),
			@XMSLocale(locale = "en", caption = "Roles") })
	private Set<JobRole> rolesAssigned;

	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE)
	@XMSField(locales = { @XMSLocale(locale = "es", caption = "Roles"),
			@XMSLocale(locale = "en", caption = "Roles") })
	private Set<Position> positions;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<JobRole> getRolesAssigned() {
		return rolesAssigned;
	}

	public void setRolesAssigned(Set<JobRole> rolesAssigned) {
		this.rolesAssigned = rolesAssigned;
	}

	public Set<Position> getPositions() {
		return positions;
	}

	public void setPositions(Set<Position> positions) {
		this.positions = positions;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}
