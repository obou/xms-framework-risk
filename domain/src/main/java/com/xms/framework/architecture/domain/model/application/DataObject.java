package com.xms.framework.architecture.domain.model.application;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.MemberOrder;

import com.xms.framework.architecture.domain.model.PassiveStructureElement;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class DataObject extends PassiveStructureElement {

	// {{ AccessedBy

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "accessedBy", sequence = "10")
	public void accessedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.addToAccessedByBehaviorElement(applicationFunction);
	}

	/**
	 * @param applicationFunction
	 */
	@MemberOrder(name = "accessedBy", sequence = "20")
	public void notAccessedByApplicationFunction(
			ApplicationFunction applicationFunction) {
		super.removeFromAccessedByBehaviorElement(applicationFunction);
	}

	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "accessedBy", sequence = "30")
	public void accessedByApplicationService(
			ApplicationService applicationService) {
		super.addToAccessedByService(applicationService);
	}

	/**
	 * @param applicationService
	 */
	@MemberOrder(name = "accessedBy", sequence = "40")
	public void notAccessedByApplicationService(
			ApplicationService applicationService) {
		super.removeFromAccessedByService(applicationService);

	}

	// }}

}
