package com.xms.framework.architecture.domain.model.technology.tests;

import static org.junit.Assert.assertTrue;

import org.apache.isis.viewer.junit.Service;
import org.apache.isis.viewer.junit.Services;
import org.junit.Before;
import org.junit.Test;

import com.xms.framework.architecture.domain.model.technology.Artifact;
import com.xms.framework.architecture.domain.model.technology.CommunicationPathFactory;
import com.xms.framework.architecture.domain.model.technology.CommunicationPathRepository;
import com.xms.framework.architecture.domain.model.technology.Device;
import com.xms.framework.architecture.domain.model.technology.InfrastructureFunction;
import com.xms.framework.architecture.domain.model.technology.InfrastructureInterface;
import com.xms.framework.architecture.domain.model.technology.InfrastructureService;
import com.xms.framework.architecture.domain.model.technology.IpProtocol;
import com.xms.framework.architecture.domain.model.technology.Node;
import com.xms.framework.architecture.domain.model.technology.NodeCommunicationPathAddress;
import com.xms.framework.architecture.domain.model.technology.NodeFactory;
import com.xms.framework.architecture.domain.model.technology.NodeRepository;
import com.xms.framework.architecture.domain.model.technology.SystemSoftware;
import com.xms.framework.frameworks.apache.isis.test.AbstractDomainTest;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Services(value = { @Service(NodeFactory.class),
		@Service(NodeRepository.class),
		@Service(CommunicationPathFactory.class),
		@Service(CommunicationPathRepository.class) })
public class MasteringArchimateTests extends AbstractDomainTest {

	// {{ Services

	private NodeFactory nodeFactory;
	private NodeRepository nodeRepository;
	private CommunicationPathFactory communicationPathFactory;
	private CommunicationPathRepository communicationPathRepository;

	public void setcommunicationPathFactory(
			CommunicationPathFactory communicationPathFactory) {
		this.communicationPathFactory = communicationPathFactory;
	}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	public void setCommunicationPathFactory(
			CommunicationPathFactory communicationPathFactory) {
		this.communicationPathFactory = communicationPathFactory;
	}

	public void setCommunicationPathRepository(
			CommunicationPathRepository communicationPathRepository) {
		this.communicationPathRepository = communicationPathRepository;
	}

	public void setNodeRepository(NodeRepository nodeRepository) {
		this.nodeRepository = nodeRepository;
	}

	@Before
	public void wrapInjectedServices() throws Exception {
		nodeFactory = wrapped(nodeFactory);
		nodeRepository = wrapped(nodeRepository);
		communicationPathFactory = wrapped(communicationPathFactory);
		communicationPathRepository = wrapped(communicationPathRepository);
	}

	// }}

	@Test
	public void testNode_CreateMasteringArchimateView78() {

		Node node = nodeFactory.createNodeWithDeviceAndSystemSoftware(
				"View 78 Node", null, "x86 computer", "Windows Server 2003",
				null, null);
		NodeCommunicationPathAddress nodeCommunicationPathAddress = node
				.addIpAddress("192.168.0.1", "255.255.255.0");

		// Internal Infrastructure Function implementing the Infrastructure
		// Service is automatically created with the same name as the Service.
		InfrastructureService windowsServerInfrastructureService = node
				.assignToNewInfrastructureService("Windows Server", null,
						"Windows Interface (e.g. API, CLI)");
		InfrastructureFunction windowServerInfrastructureFunction = windowsServerInfrastructureService
				.getRealisedByInfrastructureFunctions().iterator().next();
		windowServerInfrastructureFunction.accessesNewArtifact(
				"[winsrv2/rdbms1] SQL Server 2005 DB", null);

		Device device = node.getComposedOfDevices().iterator().next();
		SystemSoftware sqlServerSystemSoftware = device
				.assignToNewSystemSoftware("SQL Server 2005", null);

		// Internal Infrastructure Function implementing the Infrastructure
		// Service is automatically created with the same name as the Service.
		InfrastructureService sqlServerInfrastructureService = sqlServerSystemSoftware
				.assignToNewInfrastructureServiceOnIp("SQL Server 2005", null,
						nodeCommunicationPathAddress, IpProtocol.TCP, 1433);

		// Relationship between services.
		windowsServerInfrastructureService
				.usedByInfrastructureService(sqlServerInfrastructureService);

		// Relationship between interface and service.
		InfrastructureInterface windowsServerInfrastructureInterface = windowsServerInfrastructureService
				.getAssignedToInfrastructureInterfaces().iterator().next();
		sqlServerInfrastructureService.getAssignedToInfrastructureInterfaces()
				.iterator().next();

		sqlServerInfrastructureService
				.usesInfrastructureInterface(windowsServerInfrastructureInterface);

		// An InfrastructureInterface used by a Node will not be directly
		// modeled. It must be used by a Service assigned to that Node.

		assertTrue(true);

	}

	@Test
	public void testNode_CreateMasteringArchimateView57_p46() {

		Node node = nodeFactory.createNodeWithDeviceAndSystemSoftware(
				"View 78 Node", null, "x86 computer", "Windows Server 2003",
				null, null);

		Device device = node.getComposedOfDevices().iterator().next();
		SystemSoftware sqlServerSystemSoftware = device
				.assignToNewSystemSoftware("SQL Server 2005", null);

		sqlServerSystemSoftware.assignToNewInfrastructureService(
				"SQL Server 2005", null, null);
		Artifact sqlServerDatabase = node.assignToNewArtifact(
				"SQL Server 2005 DB", null);

		// Artifact assigned to Device or System Software.
		sqlServerDatabase.modifyAssignedToNode(sqlServerSystemSoftware);
		sqlServerDatabase.modifyAssignedToNode(device);

		assertTrue(true);

	}

	/**
	 * For more info see: <a
	 * href="http://www.bizzdesign.com/blog/archimate-core-patterns/"
	 * >http://www.bizzdesign.com/blog/archimate-core-patterns/</a>
	 */
	@Test
	public void testNode_CreateHardwareVirtualization() {

		Node node = nodeFactory.createNodeWithHardwareVirtualization(
				"Archimate Core Pattern", "device1", "device2",
				"operating system A", "virtualization software",
				"virtual node a", "operating system b", "virtual node b",
				"operating system c", null);

	}
}
