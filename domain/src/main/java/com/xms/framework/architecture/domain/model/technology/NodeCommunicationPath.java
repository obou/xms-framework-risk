package com.xms.framework.architecture.domain.model.technology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.annotation.Title;
import org.apache.isis.applib.util.TitleBuffer;

import com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.NEW_TABLE)
public class NodeCommunicationPath extends
		AbstractMultiTenantUnnamedEntity {

	private CommunicationPath communicationPath;

	private Node node;

	// {{ Address (Collection)
	private Set<NodeCommunicationPathAddress> addresses = new LinkedHashSet<NodeCommunicationPathAddress>();

	// }}

	public void addToAddresses(
			final NodeCommunicationPathAddress address) {
		// check for no-op
		if (address == null || getAddresses().contains(address)) {
			return;
		}
		// associate new
		getAddresses().add(address);
		// additional business logic
		onAddToAddresses(address);
	}

	public void removeFromAddresses(
			final NodeCommunicationPathAddress address) {
		// check for no-op
		if (address == null || !getAddresses().contains(address)) {
			return;
		}
		// dissociate existing
		getAddresses().remove(address);
		// additional business logic
		onRemoveFromAddresses(address);
	}

	protected void onAddToAddresses(
			final NodeCommunicationPathAddress address) {
	}

	protected void onRemoveFromAddresses(
			final NodeCommunicationPathAddress address) {
	}

	public CommunicationPath getCommunicationPath() {
		return communicationPath;
	}

	public void setCommunicationPath(CommunicationPath communicationPath) {
		this.communicationPath = communicationPath;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	/**
	 * 
	 */
	public void clearNode() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xms.framework.common.domain.model.AbstractMultiTenantUnnamedEntity
	 * #getName()
	 */
	@Override
	@Title
	public String getName() {
		return new TitleBuffer().append(this.getNode().getName())
				.append(" -> ", this.getCommunicationPath().getName())
				.toString();

	}

	public Set<NodeCommunicationPathAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(
			Set<NodeCommunicationPathAddress> addresses) {
		this.addresses = addresses;
	}

}
