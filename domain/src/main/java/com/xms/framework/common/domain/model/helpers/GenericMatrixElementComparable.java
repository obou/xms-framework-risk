package com.xms.framework.common.domain.model.helpers;

public class GenericMatrixElementComparable<K extends Comparable<K>, J extends Comparable<J>>
		implements Comparable<GenericMatrixElementComparable<K, J>> {

	private K key1;
	private J key2;

	public GenericMatrixElementComparable(K key1, J key2) {
		this.key1 = key1;
		this.key2 = key2;
	}

	public K getFirstKey() {
		return this.key1;
	}

	public J getSecondKey() {
		return this.key2;
	}

	@Override
	public int hashCode() {
		return key1.hashCode() * 3 + key2.hashCode() * 5;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (!(other instanceof GenericMatrixElementComparable))
			return false;
		GenericMatrixElementComparable<?, ?> that = (GenericMatrixElementComparable<?, ?>) other;
		return (this.key1 == null ? that.key1 == null : this.key1
				.equals(that.key1))
				&& (this.key2 == null ? that.key2 == null : this.key2
						.equals(that.key2));
	}

	// need for Comparable interface
	@Override
	public int compareTo(GenericMatrixElementComparable<K, J> aThat) {
		// NOTE: check for nulls
		return (this.key1.toString() + this.key2.toString())
				.compareTo(aThat.key1.toString() + aThat.key2.toString());
	}
}
