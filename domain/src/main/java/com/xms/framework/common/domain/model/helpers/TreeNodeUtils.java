package com.xms.framework.common.domain.model.helpers;


/**
 * Utils for working with Enums.
 * <p>
 * Some of them are used, for example, when an Enum implements de Taxonomy
 * interface.
 * <p>
 * See also: - http://whiteboxcomputing.com/java/enum_factory/ -
 * http://www.canoo.com/blog/2010/09/24/beautiful-java-enums/
 * 
 * 
 * @author <p>
 *         Copyright &copy; 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 * 
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework&reg; that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 * 
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved. It is acknowledged that there may be other brand, company,
 *         and product names used in the XMS Framework&reg; that may be covered
 *         by trademark protection and advises the reader to verify them
 *         independently.
 * 
 */
public class TreeNodeUtils {
	//TODO: Uncomment
//	@SuppressWarnings("unchecked")
//	
//	public static <T> List<TreeNode<T>> getAllElements(Object enumElement) {
//
//		return (List<TreeNode<T>>) Arrays.asList(enumElement.getClass()
//				.getEnumConstants());
//	}
//
//	public static <T> List<TreeNode<T>> getRootElements(T enumElement) {
//
//		List<TreeNode<T>> elements = getAllElements(enumElement);
//
//		List<TreeNode<T>> rootsList = new ArrayList<TreeNode<T>>();
//
//		for (TreeNode<T> currentElement : elements) {
//			if (currentElement.getParent() == null) {
//				rootsList.add(currentElement);
//			}
//		}
//		return rootsList;
//	}
//
//	public static <T> List<TreeNode<T>> getChild(T enumElement) {
//
//		List<TreeNode<T>> elements = getAllElements(enumElement);
//
//		List<TreeNode<T>> childList = new ArrayList<TreeNode<T>>();
//
//		for (TreeNode<T> currentElement : elements) {
//			if (currentElement.getParent() == enumElement) {
//				childList.add(currentElement);
//			}
//		}
//		return childList;
//	}

}
