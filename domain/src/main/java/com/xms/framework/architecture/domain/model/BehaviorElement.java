package com.xms.framework.architecture.domain.model;

import java.util.List;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.NotPersisted;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class BehaviorElement extends Entity {

	// {{ AssignedFrom
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getAssignedFrom() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.ASSIGNMENT);
	}

	public void addToAssignedFrom(
			final ActiveStructureElement activeStructureElement) {

		// It's the inverse Relationship.
		activeStructureElement.addAssignedTo(this);

	}

	public void removeFromAssignedFrom(
			final ActiveStructureElement activeStructureElement) {

		// Is the inverse relationship.
		activeStructureElement.removeFromAssignedTo(this);

	}

	// }}

	// {{ Realizes
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getRealizes() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.REALIZATION);
	}

	public void addToRealizes(final Service service) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, service,
						RelationshipType.REALIZATION);
		// Check for no-op
		if (service == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, service,
				RelationshipType.REALIZATION);
	}

	public void removeFromRealizes(final Service service) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, service,
						RelationshipType.REALIZATION);
		// Check for no-op
		if (service == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ Uses
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getUses() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.USED_BY);
	}

	public void addToUses(final Service service) {

		// It's the inverse Relationship.
		service.addToUsedByBehaviorElement(this);

	}

	public void removeFromUses(final Service service) {

		// Is the inverse relationship.
		service.removeFromUsedByBehaviorElement(this);

	}

	// }}

	// {{ Accesses
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getAccesses() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.ACCESS);
	}

	protected void addToAccesses(
			final PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, passiveStructureElement,
				RelationshipType.ACCESS);
	}

	protected void removeFromAccesses(
			final PassiveStructureElement passiveStructureElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this,
						passiveStructureElement, RelationshipType.ACCESS);
		// Check for no-op
		if (passiveStructureElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ Triggers
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getTriggers() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.TRIGGERING);
	}

	protected void addToTriggers(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.TRIGGERING);
		// Check for no-op
		if (behaviorElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, behaviorElement,
				RelationshipType.TRIGGERING);
	}

	protected void removeFromTriggers(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.TRIGGERING);
		// Check for no-op
		if (behaviorElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ TriggeredBy
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getTriggeredBy() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.TRIGGERING);
	}

	protected void addToTriggeredBy(final BehaviorElement behaviorElement) {

		// It's the inverse Relationship.
		behaviorElement.addToTriggers(this);

	}

	protected void removeFromTriggeredBy(final BehaviorElement behaviorElement) {

		// Is the inverse relationship.
		behaviorElement.removeFromTriggers(this);

	}

	// }}

	// {{ FlowFrom
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "10")
	public List<Relationship> getFlowFrom() {
		return relationshipRepository.relationshipsAsTargetOfType(this,
				RelationshipType.FLOW);
	}

	protected void addToFlowFrom(final BehaviorElement behaviorElement) {

		// It's the inverse Relationship.
		behaviorElement.addToFlowTo(this);

	}

	protected void removeFromFlowFrom(final BehaviorElement behaviorElement) {

		// Is the inverse relationship.
		behaviorElement.removeFromFlowTo(this);

	}

	// }}

	// {{ FlowTo
	@NotPersistent
	@NotPersisted
	@MemberOrder(sequence = "20")
	public List<Relationship> getFlowTo() {
		return relationshipRepository.relationshipsAsSourceOfType(this,
				RelationshipType.FLOW);
	}

	protected void addToFlowTo(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.FLOW);
		// Check for no-op
		if (behaviorElement == null || relationship != null) {
			return;
		}
		// Create new
		relationshipFactory.createRelationship(this, behaviorElement,
				RelationshipType.FLOW);
	}

	protected void removeFromFlowTo(final BehaviorElement behaviorElement) {
		// Obtain the Relationship.
		Relationship relationship = relationshipRepository
				.relationshipBySourceTargetAndType(this, behaviorElement,
						RelationshipType.FLOW);
		// Check for no-op
		if (behaviorElement == null || relationship == null) {
			return;
		}
		// Remove existing
		domainObjectContainer.remove(relationship);
	}

	// }}

	// {{ injected: RelationshipRepository
	private RelationshipRepository relationshipRepository;

	public void setRelationshipRepository(
			final RelationshipRepository relationshipRepository) {
		this.relationshipRepository = relationshipRepository;
	}

	protected RelationshipRepository getRelationshipRepository() {
		return relationshipRepository;
	}

	// }}

	// {{ injected: RelationshipFactory
	private RelationshipFactory relationshipFactory;

	public void setRelationshipFactory(
			final RelationshipFactory relationshipFactory) {
		this.relationshipFactory = relationshipFactory;
	}

	// }}

	// {{ injected: DomainObjectContainer
	private DomainObjectContainer domainObjectContainer;

	public void setDomainObjectContainer(
			final DomainObjectContainer domainObjectContainer) {
		this.domainObjectContainer = domainObjectContainer;
	}
	// }}

}
