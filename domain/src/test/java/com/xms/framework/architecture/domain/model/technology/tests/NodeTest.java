package com.xms.framework.architecture.domain.model.technology.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.isis.viewer.junit.Service;
import org.apache.isis.viewer.junit.Services;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.xms.framework.api.exception.XMSRuntimeException;
import com.xms.framework.architecture.domain.model.technology.Artifact;
import com.xms.framework.architecture.domain.model.technology.CommunicationPath;
import com.xms.framework.architecture.domain.model.technology.CommunicationPathFactory;
import com.xms.framework.architecture.domain.model.technology.CommunicationPathRepository;
import com.xms.framework.architecture.domain.model.technology.Device;
import com.xms.framework.architecture.domain.model.technology.InfrastructureInterface;
import com.xms.framework.architecture.domain.model.technology.InfrastructureService;
import com.xms.framework.architecture.domain.model.technology.IpAddress;
import com.xms.framework.architecture.domain.model.technology.IpProtocol;
import com.xms.framework.architecture.domain.model.technology.Node;
import com.xms.framework.architecture.domain.model.technology.NodeCommunicationPath;
import com.xms.framework.architecture.domain.model.technology.NodeCommunicationPathAddress;
import com.xms.framework.architecture.domain.model.technology.NodeFactory;
import com.xms.framework.architecture.domain.model.technology.NodeRepository;
import com.xms.framework.architecture.domain.model.technology.SystemSoftware;
import com.xms.framework.domain.api.domain.DomainObject;
import com.xms.framework.frameworks.apache.isis.test.AbstractDomainTest;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>..all()()
 *         rights reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Services(value = { @Service(NodeFactory.class),
		@Service(NodeRepository.class),
		@Service(CommunicationPathFactory.class),
		@Service(CommunicationPathRepository.class) })
public class NodeTest extends AbstractDomainTest {

	// {{ Services

	private NodeFactory nodeFactory;
	private NodeRepository nodeRepository;
	private CommunicationPathFactory communicationPathFactory;
	private CommunicationPathRepository communicationPathRepository;

	public void setcommunicationPathFactory(
			CommunicationPathFactory communicationPathFactory) {
		this.communicationPathFactory = communicationPathFactory;
	}

	public void setNodeFactory(NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	public void setCommunicationPathFactory(
			CommunicationPathFactory communicationPathFactory) {
		this.communicationPathFactory = communicationPathFactory;
	}

	public void setCommunicationPathRepository(
			CommunicationPathRepository communicationPathRepository) {
		this.communicationPathRepository = communicationPathRepository;
	}

	public void setNodeRepository(NodeRepository nodeRepository) {
		this.nodeRepository = nodeRepository;
	}

	@Before
	public void wrapInjectedServices() throws Exception {
		nodeFactory = wrapped(nodeFactory);
		nodeRepository = wrapped(nodeRepository);
		communicationPathFactory = wrapped(communicationPathFactory);
		communicationPathRepository = wrapped(communicationPathRepository);
	}

	// }}

	// {{ Archimate

	// See tests on project "uk.ac.bolton.archimate.tests".
	// IArchimateModel model;

	// @Before
	// public void initializeArchimateModel() throws Exception {

	// model = IArchimateFactory.eINSTANCE.createArchimateModel();

	// }

	// }}

	@Test
	public void testInfrastructureInterface_Create() {

		Node node = nodeFactory.createNode("node", "description");
		InfrastructureInterface infrastructureInterface = node
				.composeOfNewInfrastructureInterface("Windows API", null);
		validateCommonAssertions(infrastructureInterface);

	}

	@Test
	public void testInfrastructureInterface_CreateOnIp() {

		Node node = nodeFactory.createNode("node", "description");
		NodeCommunicationPathAddress nodeCommunicationPathAddress = node
				.addIpAddress("127.0.0.1", "255.255.255.0");

		InfrastructureInterface infrastructureInterface = node
				.composeOfNewIpInfrastructureInterface(
						nodeCommunicationPathAddress, IpProtocol.TCP, 2548);
		validateCommonAssertions(infrastructureInterface);

	}

	@Test
	public void testInfrastructureInterface_AssignAndChangeInfrastructureService() {

		Node node = nodeFactory.createNode("node", "description");
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());
		NodeCommunicationPathAddress nodeAddress = node.addIpAddress(
				"127.0.0.1", "255.255.255.0");
		InfrastructureInterface infrastructureInterface = node
				.composeOfNewIpInfrastructureInterface(nodeAddress,
						IpProtocol.TCP, 80);

		InfrastructureService infrastructureService = node
				.assignToNewInfrastructureService("database service", null,
						null);

		assertTrue(infrastructureInterface.getAssignedToInfrastructureService() == null);
		assertTrue(infrastructureService
				.getAssignedToInfrastructureInterfaces().size() == 0);
		infrastructureInterface
				.modifyAssignedToInfrastructureService(infrastructureService);
		assertTrue(infrastructureInterface.getAssignedToInfrastructureService()
				.equals(infrastructureService));
		assertTrue(infrastructureService
				.getAssignedToInfrastructureInterfaces().size() == 1);

	}

	@Test
	public void testInfrastructureService_UsedByInfrastructureService() {

		Node node = nodeFactory.createNode("node", "description");
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		InfrastructureService operatingSystemServices = node
				.assignToNewInfrastructureService("operating system service",
						null, null);

		InfrastructureService databaseServices = node
				.assignToNewInfrastructureService("database service", null,
						null);

		assertTrue(databaseServices.getUsesInfrastructureServices().size() == 0);
		assertTrue(operatingSystemServices.getUsedByInfrastructureServices()
				.size() == 0);
		databaseServices.usesInfrastructureService(operatingSystemServices);
		assertTrue(databaseServices.getUsesInfrastructureServices().size() == 1);
		assertTrue(operatingSystemServices.getUsedByInfrastructureServices()
				.size() == 1);

		databaseServices.getUsesInfrastructureServices().iterator().next()
				.clearTargetInfrastructureService();
		assertTrue(databaseServices.getUsesInfrastructureServices().size() == 0);
		assertTrue(operatingSystemServices.getUsedByInfrastructureServices()
				.size() == 1);

	}

	@Test
	public void testInfrastructureInterface_UsedByInfrastructureService() {

		Node node = nodeFactory.createNode("node", "description");
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		InfrastructureService operatingSystemServices = node
				.assignToNewInfrastructureService("database service", null,
						null);

		InfrastructureInterface infrastructureInterface = node
				.composeOfNewInfrastructureInterface("operating system API",
						null);

		assertTrue(operatingSystemServices.getUsesInfrastructureInterfaces()
				.size() == 0);
		assertTrue(infrastructureInterface.getUsedByInfrastructureServices()
				.size() == 0);
		operatingSystemServices
				.usesInfrastructureInterface(infrastructureInterface);
		assertTrue(operatingSystemServices.getUsesInfrastructureInterfaces()
				.size() == 1);
		assertTrue(infrastructureInterface.getUsedByInfrastructureServices()
				.size() == 1);

	}

	@Test
	public void testCommunicationPath_IpCreatedWithProperId() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		CommunicationPath ipCommunicationPath = communicationPathFactory
				.ipCommunicationPath();
		assertTrue(ipCommunicationPath.getId().equals(
				communicationPathFactory.currentTenantId().concat("::IP")));

		validateCommonAssertions(ipCommunicationPath);
	}

	@Test
	public void testNodeAssociatedWithCommunicationPath_IsCreatedOnlyOnce() {

		Node node = nodeFactory.createNode("node", "description");
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

	}

	@Test
	public void testCommunicationPath_IpIsCreatedOnlyOnce() {

		assertTrue(communicationPathRepository.all().size() == 0);

		// The Ip Communication Path is created on first .all()().
		communicationPathFactory.ipCommunicationPath();
		assertTrue(communicationPathRepository.all().size() == 1);

		// .all()()ed twice a new Communication Path it's not created.
		communicationPathFactory.ipCommunicationPath();
		assertTrue(communicationPathRepository.all().size() == 1);

	}

	@Test
	public void testCommunicationPath_AssociateWithnode() {

		Node node = nodeFactory.createNode("node", null);
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		communicationPathFactory.ipCommunicationPath().associateWithNode(node);
		assertTrue(communicationPathFactory.ipCommunicationPath()
				.getAssociatedWithNodes().size() == 1);

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

	}

	@Test
	public void testCommunicationPath_AssociateWithNodeOnAddress() {

		Node node = nodeFactory.createNode("node", null);
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());

		communicationPathFactory.ipCommunicationPath()
				.associateWithNodeOnAddress(node, "ip", "mask");
		assertTrue(communicationPathFactory.ipCommunicationPath()
				.getAssociatedWithNodes().size() == 1);

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

	}

	@Test
	public void testNode_associateWithCommunicationPath() {

		Node node = nodeFactory.createNode("node", null);
		node.associateWithCommunicationPath(communicationPathFactory
				.ipCommunicationPath());
		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

		assertTrue(communicationPathFactory.ipCommunicationPath()
				.getAssociatedWithNodes().size() == 1);

	}

	@Test(expected = XMSRuntimeException.class)
	public void testDomainObject_BusinessIdCannotBeChanged() {

		Node node = new Node();
		node.setId(UUID.randomUUID().toString().toUpperCase());

		// Here an exception must be raised.
		node.setId(UUID.randomUUID().toString().toUpperCase());
	}

	@Test
	public void testDomainObject_CreatedWithDefaultValues() {
		Node node = nodeFactory.createNode("node", "description");
		assertTrue(node.getDateCreated() != null);
	}

	@Test
	public void testDomainObject_EqualsAndHashCode() {

		// hash() and equalsTo() must be based only on the Business Id. It must
		// be assigned when the Business Object is created. The problem with ORM
		// (JPA, JDO, etc.) generated Ids. is that they are generated when
		// persisting. If the Domain Object is inserted on a Hash-based
		// Collection it will overwrite previously inserted Domain Objects, as
		// both had a "null" Id prior before persisting.

		// Tests that current implementation works well with both hash based and
		// non-hash based Collections.

		// See:
		// http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
		// See: https://community.jboss.org/wiki/EqualsandHashCode?_sscc=t

		Node node1 = nodeFactory.createNode("node1", "d");

		// In order to verify it does not depend on the Name (only on the
		// Business Id.) it will have the same name.
		Node node2 = nodeFactory.createNode("node1", "d");

		// TreeNode uses the Comparable interface (compareTo() method).
		Set<DomainObject> set = new TreeSet<DomainObject>();
		set.add(node1);
		set.add(node2);
		assertTrue(set.size() == 2);

		set = new TreeSet<DomainObject>();
		set.add(node1);
		set.add(node1);
		assertTrue(set.size() == 1);

		// LinkedHashSet uses the hash() and equalsTo() method.

		set = new LinkedHashSet<DomainObject>();
		set.add(node1);
		set.add(node2);
		assertTrue(set.size() == 2);

		set = new LinkedHashSet<DomainObject>();
		set.add(node1);
		set.add(node1);
		assertTrue(set.size() == 1);

	}

	@Test
	public void testNode_AssignTwoIps_Different() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		Node node = wrapped(nodeFactory.createNode("demo.gesconsultor.com",
				"GESCONSULTOR Demo System"));
		node.addIpAddress("192.168.0.1", "255.255.255.1");
		node.addIpAddress("192.168.0.2", "255.255.255.1");

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);
		assertTrue(node.getAssociatedWithCommunicationPaths().iterator().next()
				.getAddresses().size() == 2);
	}

	@Test
	public void testNode_AssignTwoIps_SameOnlyInsertedOnce() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		Node node = wrapped(nodeFactory.createNode("demo.gesconsultor.com",
				"GESCONSULTOR Demo System"));
		node.addIpAddress("192.168.0.1", "255.255.255.1");
		node.addIpAddress("192.168.0.1", "255.255.255.1");

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);
	}

	@Test
	public void testNode_Create() {
		Node node = nodeFactory.createNode("demo.gesconsultor.com",
				"GESCONSULTOR Demo System");

		super.validateCommonAssertions(node);
	}

	@Test
	public void testNode_CreatedDefault() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		Node node = wrapped(nodeFactory.createNode("node", "description"));
		assertFalse(node.getId().equals(null));
	}

	@Test
	public void testNode_CreateWithDeviceAndSystemSoftware() {

		Node node = nodeFactory.createNodeWithDeviceAndSystemSoftware(
				"demo.gesconsultor.com", "Demo System", "svr02-x86",
				"Ubuntu Linux", "127.0.0.1", "255.255.255.0");

		validateCommonAssertions(node);
		Device deviceCreated = node.getComposedOfDevices().iterator().next();
		validateCommonAssertions(deviceCreated);
		assertTrue(deviceCreated.getName() == "ASSETID");

		SystemSoftware systemSoftwareCreated = deviceCreated
				.getAssignedToSystemSoftware().iterator().next();
		validateCommonAssertions(systemSoftwareCreated);
		assertTrue(systemSoftwareCreated.getName() == "Ubuntu Linux");
		IpAddress ipAddress = node.getIpAddresses().iterator().next();
		assertTrue(ipAddress.getIpAddress() == "127.0.0.1");
		assertTrue(ipAddress.getIpMask() == "255.255.255.0");

	}

	@Test
	public void testDomainEntity_CreateWithNullDescription() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		Node node = wrapped(nodeFactory.createNode("node", null));
		assertTrue(node.getDescription() == null);
	}

	@Test
	public void testDomainEntity_CreateWithEmptyString() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		Node node = wrapped(nodeFactory.createNode("node", ""));
		assertTrue(node.getDescription() == "");
	}

	@Test
	public void testNode_CreateNodeOnIp() {
		// Node must be wrapped for the Apache Isis validators to be launched.
		// Node definition with 2 Ips.
		Node node = nodeFactory.createNodeOnIp("demo.gesconsultor.com",
				"GESCONSULTOR Demo System", "192.168.0.179", "255.255.255.0");

		assertTrue(node.getAssociatedWithCommunicationPaths().size() == 1);

		super.validateCommonAssertions(node);

	}

	@Test
	public void testDevice_CreateAndChangeNode() {
		Node node1 = nodeFactory.createNode("node 1", null);
		Node node2 = nodeFactory.createNode("node 2", null);
		// Node composed By 2 Devices.

		Device applianceDevice = node1.composeOfNewDevice("DELL 420", "");

		assertTrue(node1.getComposedOfDevices().size() == 1);
		assertTrue(applianceDevice.getPartOfNode().equals(node1));

		applianceDevice.modifyPartOfNode(node2);

		assertTrue(node1.getComposedOfDevices().size() == 0);
		assertTrue(node2.getComposedOfDevices().size() == 1);
		assertTrue(applianceDevice.getPartOfNode().equals(node2));

	}

	@Test
	public void testSystemSoftware_CreateAndChangeDevice() {
		Node node1 = nodeFactory.createNode("node 1", null);
		Device applianceDevice1 = node1.composeOfNewDevice("DELL 420", "");
		Device applianceDevice2 = node1.composeOfNewDevice("DELL 420", "");

		SystemSoftware windows = applianceDevice1.assignToNewSystemSoftware(
				"Windows", null);

		assertTrue(applianceDevice1.getAssignedToSystemSoftware().size() == 1);
		assertTrue(windows.getAssignedToDevice().equals(applianceDevice1));

		windows.modifyAssignedToDevice(applianceDevice2);

		assertTrue(applianceDevice1.getAssignedToSystemSoftware().size() == 0);
		assertTrue(applianceDevice2.getAssignedToSystemSoftware().size() == 1);
		assertTrue(windows.getAssignedToDevice().equals(applianceDevice2));

	}

	@Test
	public void testNode_composeOfExistingDevice() {

		Node node1 = nodeFactory.createNode("node 1", null);
		Device applianceDevice1 = node1.composeOfNewDevice("DELL 420", "");
		assertTrue(node1.getComposedOfDevices().size() == 1);
		node1.composeOfExistingDevice(applianceDevice1);
		assertTrue(node1.getComposedOfDevices().size() == 1);

		Node node2 = nodeFactory.createNode("node 2", null);
		node2.composeOfExistingDevice(applianceDevice1);

		assertTrue(node1.getComposedOfDevices().size() == 0);
		assertTrue(node2.getComposedOfDevices().size() == 1);
		assertTrue(applianceDevice1.getPartOfNode().equals(node2));
	}

	@Test
	public void testSystemSoftware_InfrastructureServicesCreateNewAndExisting() {

		// Two alternative Nodes can be used for the Infrastructure Service
		// "Oracle database".

		Node node1 = nodeFactory.createNode("node 1", null);
		Device applianceDevice1 = node1.composeOfNewDevice("DELL 420", "");
		SystemSoftware oracle1 = applianceDevice1.assignToNewSystemSoftware(
				"Oracle DBMS", null);

		// assertTrue(oracle1.getInfrastructureServices().size() == 0);
		// InfrastructureService oracleInfrastructureService = oracle1
		// .addNewInfrastructureService("database", null);
		//
		// assertTrue(oracle1.getInfrastructureServices().size() == 1);
		//
		// Node node2 = nodeFactory.createNode("node 1", null);
		// Device applianceDevice2 = node1.createDevice("DELL 420", "");
		// SystemSoftware oracle2 = applianceDevice1.assignNewSystemSoftware(
		// "Oracle DBMS", null);
		//
		// assertTrue(oracle2.getInfrastructureServices().size() == 0);
		// InfrastructureService oracleInfrastructureService = oracle2
		// .addNewInfrastructureService("database", null);
		//
		// assertTrue(oracle2.getInfrastructureServices().size() == 1);

	}

	@Test
	public void testDevice_AssignExistingSystemSoftware() {

		Node node1 = nodeFactory.createNode("node 1", null);
		Device applianceDevice1 = node1.composeOfNewDevice("DELL 420", "");
		SystemSoftware software = applianceDevice1.assignToNewSystemSoftware(
				"software", null);

		Device applianceDevice2 = node1.composeOfNewDevice("DELL 420 - 2", "");
		applianceDevice2.assignToExistingSystemSoftware(software);

		assertTrue(applianceDevice1.getAssignedToSystemSoftware().size() == 0);
		assertTrue(applianceDevice2.getAssignedToSystemSoftware().size() == 1);
		assertTrue(software.getAssignedToDevice().equals(applianceDevice2));
	}

	@Test
	public void testAbstractNode_assignedToInfrastructureService() {

		// Test assignment to Node.
		Node node1 = nodeFactory.createNode("node 1", null);

		assertTrue(node1.getAssignedToInfrastructureServices().size() == 0);
		InfrastructureService messagingService = node1
				.assignToNewInfrastructureService("messaging service", null,
						null);
		assertTrue(node1.getAssignedToInfrastructureServices().size() == 1);
		assertTrue(messagingService.getAssignedToNode().equals(node1));

		// Test Infrastructure Function creation.
		assertTrue(messagingService.getRealisedByInfrastructureFunctions()
				.iterator().next().getRealisesInfrastructureService()
				.equals(messagingService));

		// Test assignment to a Device.
		Device device1 = node1.composeOfNewDevice("DELL R420", null);
		messagingService.modifyAssignedToNode(device1);
		assertTrue(device1.getAssignedToInfrastructureServices().size() == 1);
		assertTrue(node1.getAssignedToInfrastructureServices().size() == 0);

		// Test assignment to System Software.
		SystemSoftware messagingMiddleware = device1.assignToNewSystemSoftware(
				"Messaging Middleware", null);
		messagingService.modifyAssignedToNode(messagingMiddleware);
		assertTrue(messagingMiddleware.getAssignedToInfrastructureServices()
				.size() == 1);
		assertTrue(device1.getAssignedToInfrastructureServices().size() == 0);

	}

	@Test
	public void testAbstractNode_AssignedToArtifact() {

		// Assign to a Node.
		Node node = nodeFactory.createNode("demo.gesconsultor.com", null);

		assert (node.getAssignedToArtifacts().size() == 0);
		Artifact wordDocument = node.assignToNewArtifact("letter.doc", null);
		assert (node.getAssignedToArtifacts().size() == 1);

		// Assign to a Device.
		Device device = node.composeOfNewDevice("device1", null);
		assert (device.getAssignedToArtifacts().size() == 0);
		wordDocument.modifyAssignedToNode(device);
		assert (node.getAssignedToArtifacts().size() == 0);
		assert (device.getAssignedToArtifacts().size() == 1);

		// Assign to System Software.
		SystemSoftware systemSoftware = device.assignToNewSystemSoftware(
				"Word", null);
		assert (systemSoftware.getAssignedToArtifacts().size() == 0);
		wordDocument.modifyAssignedToNode(systemSoftware);
		assert (systemSoftware.getAssignedToArtifacts().size() == 1);
		assert (device.getAssignedToArtifacts().size() == 0);

	}

	@Test
	public void testNode_assignedDevices() {
		Node node = wrapped(nodeFactory.createNode("demo.gesconsultor.com",
				null));
		// Node composed By 2 Devices.

		Device applianceDevice = node.composeOfNewDevice("DELL 420", "");
		Device keyboardDevice = node.composeOfNewDevice("DELL Keyboard", "");

		assertTrue(node.getComposedOfDevices().size() == 2);

		// 2 Services by Device.
		// InfrastructureService deviceInfrastructureService =
		// infrastructureServiceFactory
		// .createInfrastructureService(applianceDevice, "CPU1",
		// "description", InfrastructureServiceMonitoredType.CPU,
		// true);

		// Device composed by 2 System Software.

		// 2 Services by System Software.

		// InfrastructureService infrastructureService =
		// infrastructureServiceFactory
		// .createInfrastructureServiceOnIp(systemSoftware, "file share",
		// "description", IpProtocol.TCP, 2943, true);

		// System Software Services accesses 2 Artifacts.

		// System Software realised by 2 Artifacts.

	}

	@Test
	public void testNode_NodeAssociatedWithCommunicationPath_Create() {

		Node node = nodeFactory.createNode("node", "description");
		NodeCommunicationPath communicationPathAssociatedWithNode = node
				.associateWithCommunicationPath(communicationPathFactory
						.ipCommunicationPath());

		assertFalse(communicationPathAssociatedWithNode.getId().equals(null));

		/***** COMMON ASSERTIONS *****/

		super.validateCommonAssertions(communicationPathAssociatedWithNode);
	}

	// This test is ignored due to Apache Isis not saving dateUpdated() and
	// userUpdated() inside a "saved" lifecycle method. If using the Auditing
	// Service it's not needed.
	@Test()
	@Ignore
	public void testDomainObject_updateValuesAssigned()
			throws InterruptedException {

		Node node = nodeFactory.createNode("node", "description");
		assertTrue(node.getDateUpdated().equals(node.getDateCreated()));

		node.setName("node2");
		assertTrue(node.getDateUpdated().after(node.getDateCreated()));

	}

	@Test
	public void testDomainObject_creationValuesAssigned() {

		Node node = nodeFactory.createNode("node", "description");
		assertTrue(node.getDateCreated() != null);
		assertTrue(node.getCreatedByUser() != null);

	}

	@Test
	public void testEntity_DescriptionCanBeNull() {

	}

}
