package com.xms.framework.document.domain.model;

public enum DocumentStatus {

	DRAFT, APPROVED, RETIRED
}
