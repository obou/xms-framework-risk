package com.xms.framework.enterprisearchitecture.domain.model.application;

import org.apache.isis.applib.annotation.ActionSemantics;
import org.apache.isis.applib.annotation.ActionSemantics.Of;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Named;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRepositoryAndFactory;

/**
 * 
 * 
 * 
 * @author <p>
 *         Copyright � 2011-2012, <a
 *         href="http://www.gesconsultor.com">GESCONSULTOR.COM</a>. All rights
 *         reserved.
 *         </p>
 *         <p>
 *         It is acknowledged that there may be other brand, company, and
 *         product names used in the XMS Framework that may be covered by
 *         trademark protection and advises the reader to verify them
 *         independently.
 *         </p>
 * 
 */
@Named("Application Components")
public class ApplicationComponentFactory extends
		AbstractMultiTenantEntityRepositoryAndFactory {

	@ActionSemantics(Of.NON_IDEMPOTENT)
	@MemberOrder(sequence = "10")
	public ApplicationComponent create(@Named("Name") String name,
			@Named("Description") String description) {

		// TODO: Obtain the Tenant's identifier and pass it as a String. It's on
		// a different Bounded Context, so only the id should be referenced;
		// never a direct reference to it.
		return newPersistentMultiTenantEntity(ApplicationComponent.class,
				nextIdentity(), name, description);

		// return newTransientInstance(ApplicationComponent.class);

	}

}
