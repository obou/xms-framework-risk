package com.xms.framework.party.domain.model.organization;

import javax.jdo.annotations.PersistenceCapable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.xms.framework.common.domain.model.AbstractMultiTenantEntityRelationship;

//@Entity
@PersistenceCapable
public class RoleCompetency extends AbstractMultiTenantEntityRelationship {

	@ManyToOne
	private Role role;

	@ManyToOne
	private Competency competency;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Competency getCompetence() {
		return competency;
	}

	public void setCompetence(Competency competence) {
		this.competency = competence;
	}

}
